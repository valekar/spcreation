package com.srini.config;

import org.springframework.security.core.context.SecurityContextHolder;

public class LoggedInUserSingleton {



        private MyUserPrincipal myUserPrincipal;

        final static LoggedInUserSingleton loggedInUser = new LoggedInUserSingleton();

        //@Autowired
        private LoggedInUserSingleton(){
            this.myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();;
        }


        public static LoggedInUserSingleton getInstance(){
            return loggedInUser;
        }

        public MyUserPrincipal getMyUserPrincipal(){
            return  this.myUserPrincipal;
        }


        public String getUserName(){
            return this.myUserPrincipal.getUser().getUsername();
        }
        public String getUserEmail(){
            return this.myUserPrincipal.getUser().getEmail();
        }

        public Long getUserId(){
            return this.myUserPrincipal.getUser().getId();
        }


}
