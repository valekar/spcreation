package com.srini.config;

import com.srini.model.Privilege;
import com.srini.model.User;
import com.srini.util.Utility;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class MyUserPrincipal implements UserDetails {

    private User user;

    //private ThreadService ser;

    public MyUserPrincipal(User user) {
        this.user = user;
        //this.ser = new ThreadService();
        //this.id = this.user.getId();
    }



    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //ser.getColumnNames("privileges");
        List<String> privilegeColumns = Utility.getClassColumns(Privilege.class);
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        //for (Privilege privilege : user.getPrivileges()) {
        Privilege privilege = user.getPrivileges();
            for(String pColumn : privilegeColumns){
                try{
                    Method method = new PropertyDescriptor(pColumn,Privilege.class).getReadMethod();
                    if(!method.getName().endsWith("Id")){
                        String authority = pColumn.concat("-").concat(method.invoke(privilege).toString());
                        //System.out.println(authority);
                        authorities.add(new SimpleGrantedAuthority(authority));
                    }
                }
                catch (Exception ex){}
            }
       // }
        return authorities;
    }

    public User getUser() {
        return user;
    }
}