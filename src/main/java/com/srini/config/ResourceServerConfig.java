package com.srini.config;

import com.srini.filter.JsonToUrlEncodedAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	// needs to be a separate config in order to be used in unit test with custom slices

    private static final String RESOURCE_ID = "resource";
    @Autowired
    JsonToUrlEncodedAuthenticationFilter jsonFilter;
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .addFilterBefore(jsonFilter, BasicAuthenticationFilter.class)
                .requestMatchers().antMatchers("/api/threads", "/api/threads/**")
                .and()
                .requestMatchers().antMatchers("/api/users", "/api/users/**")
                .and()
                .requestMatchers().antMatchers("/api", "/api/**")
                .and()
                .authorizeRequests().anyRequest().access("#oauth2.hasScope('write')");
        // @formatter:on
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(RESOURCE_ID);
    }
}