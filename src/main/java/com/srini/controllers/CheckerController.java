package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Checking;
import com.srini.model.Inward;
import com.srini.model.Tag;
import com.srini.service.ICheckingService;
import com.srini.viewmodel.CheckingVM;
import com.srini.viewmodel.SuccessResponse;
import com.srini.viewmodel.TagVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ParseConversionEvent;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'checking_tab', 'write')")
public class CheckerController extends BaseController {

    @Autowired
    ICheckingService iCheckingService;

    @RequestMapping(value = "/checkings/inwards/produced/tags/",method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<Set<Inward>>>> getInwardWithProducedTags(){
        Set<Inward> inwards = iCheckingService.getInwardsWithProducedTags();
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CHECKER_INWARD_GET,inwards));
    }

    @RequestMapping(value = "/checkings/produced/tags/",method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<Set<TagVM>>>> getProducedTags(){
        Set<TagVM> tagsVM = iCheckingService.getTagsWithProducedStatus();
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CHECKER_TAG_GET,tagsVM));
    }

    @RequestMapping(value = "/checkings/",method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<CheckingVM>>> addCheckingPlan(@RequestBody CheckingVM checkingVM)
        throws ParseException,DataIntegrityViolationException
    {
        CheckingVM VM = iCheckingService.add(checkingVM);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CHECKING_CREATED,VM));
    }

    @RequestMapping(value = "/checkings/",method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<CheckingVM>>>> getAllPlans(){
        List<CheckingVM> VMList = iCheckingService.getAll();
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CHECKING_CREATED,VMList));
    }

    @RequestMapping(value = "/checkings/{id}",method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<CheckingVM>>> getCheckingPlan(@PathVariable(value = "id") Long id){
        CheckingVM VM = iCheckingService.getById(id);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CHECKING_GET,VM));
    }
}
