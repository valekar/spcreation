package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.http.HttpStatus;

public class BaseController<T> {


        public SuccessResponse successResponse(String type,String object){

            SuccessResponse successResponse = null;
            if(type == CONSTANTS.ADD){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.SUCCESS_HEADER+" "+ CONSTANTS.SUCCESS_HEADER_ADD,
                        CONSTANTS.ADDED_MESSAGE + object + CONSTANTS.SUCCESSFULLY_MESSAGE,HttpStatus.OK);
            }
            else if(type == CONSTANTS.EDIT){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.SUCCESS_HEADER+" "+ CONSTANTS.SUCCESS_HEADER_EDIT,
                        CONSTANTS.EDIT_MESSAGE + object + CONSTANTS.SUCCESSFULLY_MESSAGE,HttpStatus.OK);
            }
            else if(type == CONSTANTS.DELETE){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.SUCCESS_HEADER+" "+ CONSTANTS.SUCCESS_HEADER_DELETE,
                        CONSTANTS.DELETE_MESSAGE + object + CONSTANTS.SUCCESSFULLY_MESSAGE,HttpStatus.OK);
            }
            else if(type == CONSTANTS.SENT_EMAIL){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.SUCCESS_HEADER+" "+ CONSTANTS.SENT,
                        CONSTANTS.EMAIL_DESCRIPTION ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.VALID_TOKEN){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.VALID_TOKEN,
                        CONSTANTS.VALID_TOKEN_DESC ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.PASSWORD_CHANGED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PASSWORD_CHANGED,
                        CONSTANTS.PASSWORD_CHANGED_DESC ,HttpStatus.OK);
            }
            else if (type == CONSTANTS.ADMIN_ADDED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ADMIN_AUTH_CHANGED,
                        CONSTANTS.ADMIN_AUTH_CHANGED_DESC ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.CREATE_USER){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.NEW_USER_CREATED,
                        CONSTANTS.NEW_USER_CREATE_DESC ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.DELETE_USER){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.DELETED_USER,
                        CONSTANTS.DELETED_USER_DESC ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.EMPLOYEE_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.EMPLOYEE_DELETED_HEADER,
                        CONSTANTS.EMPLOYEE_DELETED_DESC ,HttpStatus.OK);
            }
            else if(type == CONSTANTS.SAM_SPC_RESET_COUNTER_SUCCESS){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.SUCCESS_HEADER,
                        CONSTANTS.SAM_SPC_RESET_COUNTER_SUCCESS ,HttpStatus.OK);
            }


           return  successResponse;
        }


        public SuccessResponse successResponseWithObject(String type, T obj){
            SuccessResponse successResponse = null;
            if (type == CONSTANTS.USER_UPDATE) {
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.USER_UPDATE,
                        CONSTANTS.USER_UPDATE_DESC, HttpStatus.OK, obj);
            }
            else if(type == CONSTANTS.CREATE_USER_DEFAULT_PASSWORD){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.NEW_USER_CREATED,
                        CONSTANTS.NEW_USER_CREATE_DEFAULT_PASSWORD_DESC ,HttpStatus.OK,obj);
            }
            //employee
            else if(type == CONSTANTS.EMPLOYEE_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.EMPLOYEE_GET,
                        CONSTANTS.EMPLOYEE_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.EMPLOYEE_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.EMPLOYEE_UPDATED,
                        CONSTANTS.EMPLOYEE_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.EMPLOYEE_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.EMPLOYEE_CREATED,
                        CONSTANTS.EMPLOYEE_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CUSTOMER_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CUSTOMER_GET,
                        CONSTANTS.CUSTOMER_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CUSTOMER_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CUSTOMER_CREATED,
                        CONSTANTS.CUSTOMER_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CUSTOMER_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CUSTOMER_UPDATED,
                        CONSTANTS.CUSTOMER_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CUSTOMER_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CUSTOMER_DELETED,
                        CONSTANTS.CUSTOMER_DELETED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.MACHINE_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.MACHINE_GET,
                        CONSTANTS.MACHINE_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.MACHINE_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.MACHINE_CREATED,
                        CONSTANTS.MACHINE_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.MACHINE_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.MACHINE_UPDATED,
                        CONSTANTS.MACHINE_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.MACHINE_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.MACHINE_DELETED,
                        CONSTANTS.MACHINE_DELETED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.RATE_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.RATE_GET,
                        CONSTANTS.RATE_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.RATE_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.RATE_CREATED,
                        CONSTANTS.RATE_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.RATE_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.RATE_UPDATED,
                        CONSTANTS.RATE_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.RATE_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.RATE_DELETED,
                        CONSTANTS.RATE_DELETED_DESC ,HttpStatus.OK,obj);
            }
            //paper handling
            else if(type == CONSTANTS.PAPER_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PAPER_GET,
                        CONSTANTS.PAPER_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.PAPER_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PAPER_CREATED,
                        CONSTANTS.PAPER_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.PAPER_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PAPER_UPDATED,
                        CONSTANTS.PAPER_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.PAPER_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PAPER_DELETED,
                        CONSTANTS.PAPER_DELETED_DESC ,HttpStatus.OK,obj);
            }

            //Thread handling
            else if(type == CONSTANTS.THREAD_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.THREAD_GET,
                        CONSTANTS.THREAD_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.THREAD_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.THREAD_CREATED,
                        CONSTANTS.THREAD_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.THREAD_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.THREAD_UPDATED,
                        CONSTANTS.THREAD_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.THREAD_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.THREAD_DELETED,
                        CONSTANTS.THREAD_DELETED_DESC ,HttpStatus.OK,obj);
            }

            //department
            else if(type == CONSTANTS.DEPARTMENT_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.DEPARTMENT_GET,
                        CONSTANTS.DEPARTMENT_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.DEPARTMENT_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.DEPARTMENT_CREATED,
                        CONSTANTS.DEPARTMENT_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.DEPARTMENT_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.DEPARTMENT_UPDATED,
                        CONSTANTS.DEPARTMENT_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.DEPARTMENT_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.DEPARTMENT_DELETED,
                        CONSTANTS.DEPARTMENT_DELETED_DESC ,HttpStatus.OK,obj);
            }

            //applique
            else if(type == CONSTANTS.APPLIQUE_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.APPLIQUE_GET,
                        CONSTANTS.APPLIQUE_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.APPLIQUE_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.APPLIQUE_CREATED,
                        CONSTANTS.APPLIQUE_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.APPLIQUE_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.APPLIQUE_UPDATED,
                        CONSTANTS.APPLIQUE_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.APPLIQUE_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.APPLIQUE_DELETED,
                        CONSTANTS.APPLIQUE_DELETED_DESC ,HttpStatus.OK,obj);
            }


            //Order
            else if(type == CONSTANTS.ORDER_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_GET,
                        CONSTANTS.ORDER_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_CREATED,
                        CONSTANTS.ORDER_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_UPDATED,
                        CONSTANTS.ORDER_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_DELETED,
                        CONSTANTS.ORDER_DELETED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_COUNT){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_COUNT,
                        CONSTANTS.ORDER_COUNT_DESC ,HttpStatus.OK,obj);
            }
            // order approve/reject/pending/confirm
            else if(type == CONSTANTS.ORDER_APPROVE){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_APPROVE,
                        CONSTANTS.ORDER_APPROVE_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_REJECT){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_REJECT,
                        CONSTANTS.ORDER_REJECT_DESC ,HttpStatus.OK,obj);
            }

            else if(type == CONSTANTS.ORDER_PENDING){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_PENDING,
                        CONSTANTS.ORDER_PENDING_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.ORDER_CONFIRMED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.ORDER_CONFIRMED,
                        CONSTANTS.ORDER_CONFIRMED_DESC ,HttpStatus.OK,obj);
            }

            //inward handling
            else if(type == CONSTANTS.INWARD_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.INWARD_GET,
                        CONSTANTS.INWARD_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.INWARD_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.INWARD_CREATED,
                        CONSTANTS.INWARD_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.INWARD_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.INWARD_UPDATED,
                        CONSTANTS.INWARD_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.INWARD_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.INWARD_DELETED,
                        CONSTANTS.INWARD_DELETED_DESC ,HttpStatus.OK,obj);
            }

            //tag handling
            else if(type == CONSTANTS.TAG_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.TAG_GET,
                        CONSTANTS.TAG_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.TAG_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.TAG_CREATED,
                        CONSTANTS.TAG_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.TAG_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.TAG_UPDATED,
                        CONSTANTS.TAG_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.TAG_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.TAG_DELETED,
                        CONSTANTS.TAG_DELETED_DESC ,HttpStatus.OK,obj);
            }

            //constant handling
            else if(type == CONSTANTS.CONSTANT_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CONSTANT_GET,
                        CONSTANTS.CONSTANT_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CONSTANT_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CONSTANT_CREATED,
                        CONSTANTS.CONSTANT_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CONSTANT_UPDATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CONSTANT_UPDATED,
                        CONSTANTS.CONSTANT_UPDATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CONSTANT_DELETED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CONSTANT_DELETED,
                        CONSTANTS.CONSTANT_DELETED_DESC ,HttpStatus.OK,obj);
            }
            //production planning
            else if(type == CONSTANTS.PRODUCTION_PLANNING){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.PRODUCTION_PLANNING_SET,
                        CONSTANTS.PRODUCTION_PLANNING_SET_DESC ,HttpStatus.OK,obj);
            }

            //inward tags with produced
            else if(type == CONSTANTS.CHECKER_INWARD_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CHECKER_INWARD_GET,
                        CONSTANTS.CHECKER_INWARD_GET_DESC ,HttpStatus.OK,obj);
            }
            //checker produced status tags
            else if(type == CONSTANTS.CHECKER_TAG_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CHECKER_TAG_GET,
                        CONSTANTS.CHECKER_TAG_GET_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CHECKING_CREATED){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CHECKING_CREATED,
                        CONSTANTS.CHECKING_CREATED_DESC ,HttpStatus.OK,obj);
            }
            else if(type == CONSTANTS.CHECKING_GET){
                successResponse = new SuccessResponse(HttpStatus.OK.value(),
                        CONSTANTS.CHECKING_GET,
                        CONSTANTS.CHECKING_GET_DESC ,HttpStatus.OK,obj);
            }
            return successResponse;
        }
}
