package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.exception.CustomException;
import com.srini.exception.UserNotFoundException;
import com.srini.model.User;
import com.srini.service.AdminService;
import com.srini.service.UserService;
import com.srini.viewmodel.ModifiedUserVM;
import com.srini.viewmodel.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@PreAuthorize("hasPermission('', 'admin_tab', 'write')")
@RequestMapping(value = "/api")
public class AdminController extends BaseController{

    final private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserService userService;
    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/admin/users/",method = RequestMethod.POST)
    public Callable<ResponseEntity<SuccessResponse>> createNewUser(@RequestBody ModifiedUserVM user)
            throws DataIntegrityViolationException,CustomException{
        ModifiedUserVM mUser = adminService.saveMUser(user);

        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CREATE_USER_DEFAULT_PASSWORD,mUser));
    }

    @RequestMapping(value = "/admin/users/",method = RequestMethod.GET)
    public Callable<ResponseEntity<List<ModifiedUserVM>>> getAllUsers() throws Exception{
        List<ModifiedUserVM> userList = adminService.getAllUsers();
        return () -> ResponseEntity.ok(userList);
    }

    @RequestMapping(value = "/admin/users/{id}",method=RequestMethod.DELETE)
    public Callable<ResponseEntity<SuccessResponse>> deleteUser(@PathVariable(value = "id") Long id) throws CustomException,UserNotFoundException {
        int status = adminService.deleteUser(id);
        if(status == 1){
            return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.DELETE_USER,""));
        }
        else{
            throw new UserNotFoundException();
        }
    }

    @RequestMapping(value = "/admin/users/{id}",method = RequestMethod.PUT)
    public Callable<ResponseEntity<SuccessResponse>> updateUser
            (@RequestBody ModifiedUserVM user, @PathVariable(value = "id") Long id) throws NullPointerException{
        ModifiedUserVM modifiedUserVM = adminService.updateUser(user,id);
        //BaseController<ModifiedUserVM> baseController = new BaseController<ModifiedUserVM>();
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.USER_UPDATE, modifiedUserVM));
    }

    @RequestMapping(value = "/admin/reset/counter",method = RequestMethod.DELETE)
    public Callable<ResponseEntity<SuccessResponse>> resetCounter() throws DataIntegrityViolationException{
        int status = adminService.resetCounter();
        if(status == 1){
            return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.SAM_SPC_RESET_COUNTER_SUCCESS,""));
        }
        else {
            throw new DataIntegrityViolationException(CONSTANTS.SAM_SPC_DELETE_TABLE_ERROR);
        }
    }

}
