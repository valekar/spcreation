package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Inward;
import com.srini.model.Production;
import com.srini.service.IProductionService;
import com.srini.viewmodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'production_tab', 'write')")
public class ProductionController extends BaseController{

    @Autowired
    IProductionService iProductionService;

    @RequestMapping(value = "/productions/inwards/order/{id}/confirm/", method = RequestMethod.PUT)
    public Callable<ResponseEntity<SuccessResponse<InwardVM>>> confirmOrder(
            @PathVariable("id") Long id) throws NullPointerException{
        InwardVM VM= iProductionService.confirmOrderWithInward(id);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_CONFIRMED,VM));
    }

    @RequestMapping(value = "/productions/inwards/", method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<List<InwardVM>>>> getOrderAppOrConfirmedInwards() throws NullPointerException{
        List<InwardVM> vmList= iProductionService.orderApprovedOrConfirmedInwards();
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.INWARD_GET,vmList));
    }

    @RequestMapping(value = "/productions/inwards/orders/confirmed/", method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<List<InwardVM>>>> getOrderConfirmedInwards() throws NullPointerException{
        List<InwardVM> vmList= iProductionService.confirmedOrderInwards();
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.INWARD_GET,vmList));
    }



    @RequestMapping(value = "/productions/set/planning" , method = RequestMethod.POST)
    public Callable<ResponseEntity<SuccessResponse<Production>>> setProductionPlanner(@RequestBody PlanningForm planningForm)
        throws ParseException,DataIntegrityViolationException
    {
        PlanningForm pf = planningForm;
        Production production = iProductionService.setPlanner(pf);
        //Production production1 = iProductionService.getById(production.getId());
        return ()->ResponseEntity.ok(successResponseWithObject(CONSTANTS.PRODUCTION_PLANNING,production));
    }

    @RequestMapping(value = "/productions/",method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<List<ProductionVM>>>> getPlans(){
        List<ProductionVM> vmList = iProductionService.getAll();
        return ()->ResponseEntity.ok(successResponseWithObject(CONSTANTS.PRODUCTION_PLANNING,vmList));
    }

    @RequestMapping(value = "/productions/{id}",method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<ProductionVM>>> getProdById(@PathVariable(value="id") Long id){
        ProductionVM vm = iProductionService.getProdWithInProductionTags(id);
        return ()->ResponseEntity.ok(successResponseWithObject(CONSTANTS.PRODUCTION_PLANNING,vm));
    }

}
