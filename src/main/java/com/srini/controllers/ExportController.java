package com.srini.controllers;

import com.srini.service.ICheckingService;
import com.srini.service.IProductionService;
import com.srini.viewmodel.CheckingVM;
import com.srini.viewmodel.ProductionVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ExportController  {

    @Autowired
    IProductionService iProductionService;

    @Autowired
    ICheckingService iCheckingService;


    @PreAuthorize("hasPermission('', 'production_tab', 'write')")
    @RequestMapping(value = "/api/production/planning/{id}/download", method = RequestMethod.GET)
    public String downloadProductionPlan(Model model, @PathVariable(value = "id") Long id) {

        ProductionVM vm = iProductionService.getProdWithInProductionTags(id);
        model.addAttribute("productionVM", vm);
        return "download Excel";
    }

    @PreAuthorize("hasPermission('', 'checking_tab', 'write')")
    @RequestMapping(value = "/api/checking/planning/{id}/download", method = RequestMethod.GET)
    public String downloadCheckingPlan(Model model, @PathVariable(value = "id") Long id) {
        CheckingVM vm = iCheckingService.getById(id);
        model.addAttribute("checkingVM", vm);
        return "download Excel";
    }
}
