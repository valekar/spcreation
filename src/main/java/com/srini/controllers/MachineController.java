package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Machine;
import com.srini.service.IMachineService;
import com.srini.viewmodel.MachineVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(value = "/api")

@PreAuthorize("hasPermission('', 'store_tab', 'write')")
public class MachineController extends BaseController{

    @Autowired
    IMachineService machineService;

    @RequestMapping(value = "/machines/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<MachineVM>>> getMachineById(@PathVariable(value = "id") Long id) {
        MachineVM machineVM = machineService.getMachinesById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.MACHINE_GET, machineVM));
    }

    @RequestMapping(value = "/machines/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<MachineVM>>>> getAllMachines() {
        List<MachineVM> machineVMList = machineService.getAllMachines();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.MACHINE_GET, machineVMList));
    }

    @RequestMapping(value = "/machines/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<MachineVM>>>
    updateMachine(@RequestBody Machine machine, @PathVariable(value = "id") Long id) throws NullPointerException {
        MachineVM machineVM = machineService.updateMachine(machine, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.MACHINE_UPDATED, machineVM));
    }

    @RequestMapping(value = "/machines/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<MachineVM>>> addMachine(@RequestBody Machine customer) {
        MachineVM machineVM = machineService.addMachine(customer);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.MACHINE_CREATED, machineVM));
    }

    @RequestMapping(value = "/machines/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> deleteMachine(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = machineService.deleteMachine(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.MACHINE_DELETED, null));
    }

}
