package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Rate;
import com.srini.service.IRateService;
import com.srini.viewmodel.RateVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'company_tab', 'write')")
public class RateController extends  BaseController{

    @Autowired
    IRateService rateService;

    @RequestMapping(value = "/rates/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<RateVM>>> getRateById(@PathVariable(value = "id") Long id) {
        RateVM VM = rateService.getRatesById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.RATE_GET, VM));
    }

    @RequestMapping(value = "/rates/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<RateVM>>>> getAllRates() {
        List<RateVM> VMList = rateService.getAllRates();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.RATE_GET, VMList));
    }

    @RequestMapping(value = "/rates/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<RateVM>>>
            updateRate(@RequestBody Rate rate, @PathVariable(value = "id") Long id) throws NullPointerException {
        RateVM VM = rateService.updateRate(rate, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.RATE_UPDATED, VM));
    }

    @RequestMapping(value = "/rates/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<RateVM>>> addRate(@RequestBody Rate rate) {
        RateVM VM = rateService.addRate(rate);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.RATE_CREATED, VM));
    }

    @RequestMapping(value = "/rates/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> deleteRate(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = rateService.deleteRate(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.RATE_DELETED, null));
    }

}
