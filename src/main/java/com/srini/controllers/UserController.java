package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.PasswordResetToken;
import com.srini.service.ResetPasswordService;
import com.srini.service.UserService;
import com.srini.viewmodel.ChangePasswordToken;
import com.srini.viewmodel.ResetPasswordVM;
import com.srini.viewmodel.SuccessResponse;
import com.srini.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class UserController extends BaseController{


    @Autowired
    UserService userService;

    @Autowired
    ResetPasswordService resetPasswordService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/users/register", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Callable<ResponseEntity<SuccessResponse>> saveUser(@RequestBody User user)  {
        User u = userService.save(user);
        //System.out.println(u);
        return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.ADD,CONSTANTS.USER));
        //return new ResponseEntity<SuccessResponse>(super.getSuccessAdded(CONSTANTS.ADD,CONSTANTS.USER), HttpStatus.OK);
    }

    @RequestMapping(value = "/users/reset-password", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Callable<ResponseEntity<SuccessResponse>> resetPassword(@RequestBody ResetPasswordVM resetPassword) throws Exception{
        PasswordResetToken p =  resetPasswordService.save(resetPassword.getEmail());
        return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.SENT_EMAIL, ""));
        //return new ResponseEntity<SuccessResponse>(super.getSuccessAdded(CONSTANTS.ADD,CONSTANTS.USER), HttpStatus.OK);
    }

    @RequestMapping(value = "/users/change-password/validate")
    public Callable<ResponseEntity<SuccessResponse>> validateToken(@RequestBody ChangePasswordToken changePasswordToken) throws InvalidTokenException{
        boolean valid = resetPasswordService.validateToken(changePasswordToken.getToken());
        if(valid)
            return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.VALID_TOKEN, ""));
        else
            throw new InvalidTokenException(changePasswordToken.getToken());
    }

    @RequestMapping(value = "/users/change-password")
    public Callable<ResponseEntity<SuccessResponse>> changePassword(@RequestBody ChangePasswordToken changePasswordToken) throws InvalidTokenException{
        boolean valid = resetPasswordService.validateToken(changePasswordToken.getToken());
        if(valid){
            int u = userService.updatePassword(changePasswordToken.getPassword(),changePasswordToken.getToken());
            return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.PASSWORD_CHANGED, ""));
        }

        else
            throw new InvalidTokenException(changePasswordToken.getToken());
    }

    @RequestMapping(value = "/users/{id}")
    public @ResponseBody String getUser(@PathVariable("id") String id){
        return "asd";
        //return new ResponseEntity<SuccessResponse>(super.getSuccessAdded(), HttpStatus.OK);
    }

    @PostAuthorize("hasPermission('','inward_tab','write')")
    @RequestMapping(value="/test", method = RequestMethod.GET)

    public Callable<ResponseEntity<String>> testAuthorize(){
        logger.error("Testing logging");
        return () -> ResponseEntity.ok("Successfully tested!");
    }





}
