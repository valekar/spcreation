package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Constant;
import com.srini.service.IConstantService;
import com.srini.viewmodel.ConstantVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RequestMapping("/api")
@RestController

@PreAuthorize("hasPermission('', 'admin_tab', 'write')")
public class ConstantController extends BaseController {

    @Autowired
    IConstantService constantService;

    @RequestMapping(value = "/constants/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<ConstantVM>>> getById(@PathVariable(value = "id") Long id) {
        ConstantVM VM = constantService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CONSTANT_GET, VM));
    }

    @RequestMapping(value = "/constants/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<ConstantVM>>>> getAll() {
        List<ConstantVM> VMList = constantService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CONSTANT_GET, VMList));
    }

    @RequestMapping(value = "/constants/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<ConstantVM>>>
    update(@RequestBody Constant fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        ConstantVM VM = constantService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CONSTANT_UPDATED, VM));
    }

    @RequestMapping(value = "/constants/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<ConstantVM>>> add(@RequestBody Constant fromBodyModel) {
        ConstantVM VM = constantService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CONSTANT_CREATED, VM));
    }

    @RequestMapping(value = "/constants/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = constantService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CONSTANT_DELETED, null));
    }
}
