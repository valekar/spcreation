package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Order;
import com.srini.service.IOrderService;
import com.srini.viewmodel.OrderVM;
import com.srini.viewmodel.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;



@RestController
@RequestMapping(value = "/api")

@PreAuthorize("hasPermission('', 'order_tab', 'write')")
public class OrderController extends BaseController {

    @Autowired
    IOrderService iOrderService;

    @Autowired
    Environment env;

    private final Logger log = LoggerFactory.getLogger(OrderController.class);

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<OrderVM>>> getById(@PathVariable(value = "id") Long id) throws NullPointerException,IOException{
        OrderVM VM = iOrderService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_GET, VM));
    }

    @RequestMapping(value = "/orders", params = {"page","size"},method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<OrderVM>>>> getAll(@RequestParam("page") int page,@RequestParam("size") int size)
            throws IOException,NullPointerException{
        List<OrderVM> VMList = iOrderService.getAll(page,size);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_GET, VMList));
    }

    /*@RequestMapping(value = "/orders/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<OrderVM>>>
    update(@RequestBody Order fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        OrderVM VM = iOrderService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_UPDATED, VM));
    }*/

    /*@RequestMapping(value = "/orders/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<OrderVM>>> add(@RequestBody Order fromBodyModel) {
        OrderVM VM = iOrderService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_CREATED, VM));
    }*/

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.PUT,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    Callable<ResponseEntity<SuccessResponse<OrderVM>>>
    update(@RequestPart("file") MultipartFile file,
           @RequestPart("order") Order fromBodyModel, @PathVariable(value = "id") Long id)
            throws NullPointerException,MultipartException, IOException {
        OrderVM VM = iOrderService.update(fromBodyModel, file ,id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_UPDATED, VM));
    }

    @RequestMapping(value = "/orders/", method = RequestMethod.POST,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    Callable<ResponseEntity<SuccessResponse<OrderVM>>> add(@RequestPart("file") MultipartFile file,
                                                           @RequestPart("order") Order fromBodyModel) throws MultipartException, IOException {
        OrderVM VM = iOrderService.add(fromBodyModel,file);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_CREATED, VM));
    }

    //this is built for pagination
    @RequestMapping(value = "/orders/count/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<OrderVM>>> ordersCount() throws Exception{
        long count = iOrderService.getOrderCount();
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_COUNT, count));
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws IOException,DataIntegrityViolationException {
        int status = iOrderService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.ORDER_DELETED, null));
    }

}
