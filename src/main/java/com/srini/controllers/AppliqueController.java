package com.srini.controllers;


import com.srini.constant.CONSTANTS;
import com.srini.model.Applique;
import com.srini.service.IAppliqueService;
import com.srini.viewmodel.AppliqueVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(value = "/api")

@PreAuthorize("hasPermission('', 'store_tab', 'write')")
public class AppliqueController extends BaseController {

    @Autowired
    IAppliqueService appliqueService;

    @RequestMapping(value = "/appliques/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<AppliqueVM>>> getById(@PathVariable(value = "id") Long id) {
        AppliqueVM VM = appliqueService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.APPLIQUE_GET, VM));
    }

    @RequestMapping(value = "/appliques/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<AppliqueVM>>>> getAll() {
        List<AppliqueVM> VMList = appliqueService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.APPLIQUE_GET, VMList));
    }

    @RequestMapping(value = "/appliques/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<AppliqueVM>>>
    update(@RequestBody Applique fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        AppliqueVM VM = appliqueService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.APPLIQUE_UPDATED, VM));
    }

    @RequestMapping(value = "/appliques/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<AppliqueVM>>> add(@RequestBody Applique fromBodyModel) {
        AppliqueVM VM = appliqueService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.APPLIQUE_CREATED, VM));
    }

    @RequestMapping(value = "/appliques/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = appliqueService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.APPLIQUE_DELETED, null));
    }
}
