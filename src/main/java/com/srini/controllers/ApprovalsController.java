package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Order;
import com.srini.service.IOrderService;
import com.srini.viewmodel.OrderVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'admin_tab', 'write')")
public class ApprovalsController extends BaseController {

    @Autowired
    private IOrderService iOrderService;

    @RequestMapping( value = "/approvals/orders/",method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<List<OrderVM>>>> getAllOrdersForApproval() throws IOException{
        List<OrderVM> VMList = iOrderService.getAll(0,0);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_GET,VMList));
    }

    @RequestMapping(value = "/approvals/order/approve/{id}", method = RequestMethod.PUT)
    public Callable<ResponseEntity<SuccessResponse<OrderVM>>> approveOrder(
            @PathVariable("id") Long id) throws IOException,NullPointerException{
        OrderVM VM =iOrderService.approveOrder(id);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_APPROVE,VM));
    }

    @RequestMapping(value = "/approvals/order/reject/{id}", method = RequestMethod.PUT)
    public Callable<ResponseEntity<SuccessResponse<OrderVM>>> rejectOrder(
            @PathVariable("id") Long id) throws IOException,NullPointerException{
        OrderVM VM =iOrderService.rejectOrder(id);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_REJECT,VM));
    }

    @RequestMapping(value = "/approvals/order/pending/{id}", method = RequestMethod.PUT)
    public Callable<ResponseEntity<SuccessResponse<OrderVM>>> pendingOrder(
            @PathVariable("id") Long id) throws IOException,NullPointerException{
        OrderVM VM =iOrderService.pendingOrder(id);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_PENDING,VM));
    }
}
