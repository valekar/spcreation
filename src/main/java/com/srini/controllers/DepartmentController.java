package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.BaseEntity;
import com.srini.model.Department;
import com.srini.service.IDepartmentService;
import com.srini.viewmodel.DepartmentVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'company_tab', 'write')")
public class DepartmentController extends BaseController{

    @Autowired
    IDepartmentService departmentService;

   
    @RequestMapping(value = "/departments/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<DepartmentVM>>> getById(@PathVariable(value = "id") Long id) {
        DepartmentVM VM = departmentService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.DEPARTMENT_GET, VM));
    }

    @RequestMapping(value = "/departments/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<DepartmentVM>>>> getAll() {
        List<DepartmentVM> VMList = departmentService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.DEPARTMENT_GET, VMList));
    }

    @RequestMapping(value = "/departments/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<DepartmentVM>>>
        update(@RequestBody Department fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        DepartmentVM VM = departmentService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.DEPARTMENT_UPDATED, VM));
    }

    @RequestMapping(value = "/departments/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<DepartmentVM>>> add(@RequestBody Department fromBodyModel) {
        DepartmentVM VM = departmentService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.DEPARTMENT_CREATED, VM));
    }

    @RequestMapping(value = "/departments/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = departmentService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.DEPARTMENT_DELETED, null));
    }
}
