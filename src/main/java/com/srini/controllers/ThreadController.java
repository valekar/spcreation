package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Thread;
import com.srini.service.IThreadService;
import com.srini.viewmodel.SuccessResponse;
import com.srini.viewmodel.ThreadVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(value = "/api")

@PreAuthorize("hasPermission('', 'store_tab', 'write')")
public class ThreadController extends BaseController {

    @Autowired
    IThreadService threadService;

    @RequestMapping(value = "/threads/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<ThreadVM>>> getById(@PathVariable(value = "id") Long id) {
        ThreadVM VM = threadService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.THREAD_GET, VM));
    }

    @RequestMapping(value = "/threads/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<ThreadVM>>>> getAll() {
        List<ThreadVM> VMList = threadService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.THREAD_GET, VMList));
    }

    @RequestMapping(value = "/threads/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<ThreadVM>>>
    update(@RequestBody Thread fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        ThreadVM VM = threadService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.THREAD_UPDATED, VM));
    }

    @RequestMapping(value = "/threads/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<ThreadVM>>> add(@RequestBody Thread fromBodyModel) {
        ThreadVM VM = threadService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.THREAD_CREATED, VM));
    }

    @RequestMapping(value = "/threads/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = threadService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.THREAD_DELETED, null));
    }
}
