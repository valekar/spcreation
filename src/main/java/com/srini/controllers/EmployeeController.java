package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Employee;
import com.srini.service.EmployeeService;
import com.srini.viewmodel.EmployeeVM;
import com.srini.viewmodel.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(value = "/api")

@PreAuthorize("hasPermission('', 'company_tab', 'write')")
public class EmployeeController extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<EmployeeVM>> getEmployeeById(@PathVariable(value = "id") Long id) {
        EmployeeVM emp = employeeService.getEmployeeById(id);
        return () -> ResponseEntity.ok(emp);
    }

    @RequestMapping(value = "/employees",params = {"type"},method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<EmployeeVM>>> getEmployeesByDepartmentType(
            @RequestParam(name = "type") String employeesType) {
        List<EmployeeVM> employeeVMList = employeeService.getEmployeesByDepartmentType(employeesType);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.EMPLOYEE_GET,employeeVMList));
    }

    @RequestMapping(value = "/employees/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<EmployeeVM>>> getAllEmployees() {
        List<EmployeeVM> employeeVMList = employeeService.getAllEmployees();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.EMPLOYEE_GET,employeeVMList));
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<EmployeeVM>>> updateEmployee(@RequestBody Employee employee, @PathVariable(value = "id") Long id) throws NullPointerException {
        EmployeeVM empl = employeeService.updateEmployee(employee, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.EMPLOYEE_UPDATED, empl));
    }


    @RequestMapping(value = "/employees/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<EmployeeVM>>> addUser(@RequestBody Employee employee) {
        EmployeeVM employee1 = employeeService.addEmployee(employee);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.EMPLOYEE_CREATED, employee1));
    }


    @RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> deleteUser(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = employeeService.deleteEmployee(id);
        return () -> ResponseEntity.ok(super.successResponse(CONSTANTS.EMPLOYEE_DELETED, ""));
    }
}
