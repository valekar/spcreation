package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Customer;
import com.srini.service.ICustomerService;
import com.srini.viewmodel.CustomerVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'company_tab', 'write')")
public class CustomerController extends BaseController {

    @Autowired
    ICustomerService customerService;

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<CustomerVM>>> getCustomerById(@PathVariable(value = "id") Long id) {
        CustomerVM customer = customerService.getCustomerById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CUSTOMER_GET, customer));
    }

    @RequestMapping(value = "/customers/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<CustomerVM>>>> getAllCustomers() {
        List<CustomerVM> customerVMList = customerService.getAllCustomers();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.CUSTOMER_GET, customerVMList));
    }

    @RequestMapping(value = "/customers/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<CustomerVM>>> updateCustomer(@RequestBody Customer customer, @PathVariable(value = "id") Long id) throws NullPointerException {
        CustomerVM customerVM = customerService.updateCustomer(customer, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CUSTOMER_UPDATED, customerVM));
    }


    @RequestMapping(value = "/customers/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<CustomerVM>>> addCustomer(@RequestBody Customer customer) {
        CustomerVM customerVM = customerService.addCustomer(customer);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CUSTOMER_CREATED, customerVM));
    }


    @RequestMapping(value = "/customers/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> deleteCustomer(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = customerService.deleteCustomer(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.CUSTOMER_DELETED, null));
    }

}
