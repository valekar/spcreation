package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Tag;
import com.srini.service.ITagService;
import com.srini.viewmodel.SuccessResponse;
import com.srini.viewmodel.TagVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

public class TagController extends BaseController {

    @Autowired
    ITagService tagService;

    @RequestMapping(value = "/tags/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<TagVM>>> getById(@PathVariable(value = "id") Long id) {
        TagVM VM = tagService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.TAG_GET, VM));
    }

     @RequestMapping(value = "/inwards/{id}/tags/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<TagVM>>>> getAllByInwardId(@PathVariable(value ="id") Long id) {
        List<TagVM> VMList = tagService.getAllByInwardId(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.TAG_GET, VMList));
    }

    //this may not be used at all!
    @RequestMapping(value = "/tags/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<TagVM>>>> getAll() {
        List<TagVM> VMList = tagService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.TAG_GET, VMList));
    }

    @RequestMapping(value = "/tags/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<TagVM>>>
    update(@RequestBody Tag fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        TagVM VM = tagService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.TAG_UPDATED, VM));
    }

    @RequestMapping(value = "/tags/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<TagVM>>> add(@RequestBody Tag fromBodyModel){
        TagVM VM = tagService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.TAG_CREATED, VM));
    }

    @RequestMapping(value = "/tags/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = tagService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.TAG_DELETED, null));
    }
}
