package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Paper;
import com.srini.service.IPaperService;
import com.srini.viewmodel.PaperVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'store_tab', 'write')")
@CrossOrigin(origins="*")
public class PaperController extends BaseController{

    @Autowired
    IPaperService paperService;

    @RequestMapping(value = "/papers/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<PaperVM>>> getById(@PathVariable(value = "id") Long id) {
        PaperVM VM = paperService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.PAPER_GET, VM));
    }

    @RequestMapping(value = "/papers/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<PaperVM>>>> getAll() {
        List<PaperVM> VMList = paperService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.PAPER_GET, VMList));
    }

    @RequestMapping(value = "/papers/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<PaperVM>>>
    update(@RequestBody Paper fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException {
        PaperVM VM = paperService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.PAPER_UPDATED, VM));
    }

    @RequestMapping(value = "/papers/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<PaperVM>>> add(@RequestBody Paper fromBodyModel) {
        PaperVM VM = paperService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.PAPER_CREATED, VM));
    }

    @RequestMapping(value = "/papers/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = paperService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.PAPER_DELETED, null));
    }
}
