package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.model.Inward;
import com.srini.service.IInwardService;
import com.srini.viewmodel.InwardVM;
import com.srini.viewmodel.OrderVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.Callable;

@RequestMapping(value = "/api")
@RestController

@PreAuthorize("hasPermission('', 'inward_tab', 'write')")
public class InwardController extends BaseController {

    @Autowired
    IInwardService inwardService;

    @RequestMapping(value = "/inwards/{id}", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<InwardVM>>> getById(@PathVariable(value = "id") Long id) {
        InwardVM VM = inwardService.getById(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.INWARD_GET, VM));
    }

    @RequestMapping(value = "/inwards/", method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<InwardVM>>>> getAll() {
        List<InwardVM> VMList = inwardService.getAll();
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.INWARD_GET, VMList));
    }

    @RequestMapping(value = "/inwards/{id}", method = RequestMethod.PUT)
    Callable<ResponseEntity<SuccessResponse<InwardVM>>>
    update(@RequestBody Inward fromBodyModel, @PathVariable(value = "id") Long id) throws NullPointerException,ParseException {
        InwardVM VM = inwardService.update(fromBodyModel, id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.INWARD_UPDATED, VM));
    }

    @RequestMapping(value = "/inwards/", method = RequestMethod.POST)
    Callable<ResponseEntity<SuccessResponse<InwardVM>>> add(@RequestBody Inward fromBodyModel) throws ParseException{
        InwardVM VM = inwardService.add(fromBodyModel);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.INWARD_CREATED, VM));
    }

    @RequestMapping(value = "/inwards/{id}", method = RequestMethod.DELETE)
    Callable<ResponseEntity<SuccessResponse>> delete(@PathVariable(value = "id") Long id) throws DataIntegrityViolationException {
        int status = inwardService.delete(id);
        return () -> ResponseEntity.ok(super.successResponseWithObject(CONSTANTS.INWARD_DELETED, null));
    }

    @RequestMapping(value = "/inwards/approved/orders",params = {"page","size"},method = RequestMethod.GET)
    public Callable<ResponseEntity<SuccessResponse<List<OrderVM>>>> getApprovedOrders(
            @RequestParam("page") Long page, @RequestParam("size") Long size
    ) throws NullPointerException,IOException{
        List<OrderVM> vmList= inwardService.getOnlyApprovedOrders(page,size);
        return ()-> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_GET,vmList));
    }
}
