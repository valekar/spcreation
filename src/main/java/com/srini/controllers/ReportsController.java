package com.srini.controllers;

import com.srini.constant.CONSTANTS;
import com.srini.service.IReportsService;
import com.srini.viewmodel.OutwardReportVM;
import com.srini.viewmodel.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/api")

@PreAuthorize("hasPermission('', 'reports_tab', 'write')")
public class ReportsController extends BaseController {

    @Autowired
    IReportsService reportsService;

    @RequestMapping(value = "/reports/inwards/customer/{id}",method = RequestMethod.GET)
    Callable<ResponseEntity<SuccessResponse<List<OutwardReportVM>>>> getOrdersByCustomerId(@PathVariable(value = "id") Long id){
        List<OutwardReportVM> VMList = reportsService.getInwardsByCustomerId(id);
        return () -> ResponseEntity.ok(successResponseWithObject(CONSTANTS.ORDER_GET, VMList));
    }
}
