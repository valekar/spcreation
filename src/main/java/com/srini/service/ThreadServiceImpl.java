package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Thread;
import com.srini.repository.ThreadRepository;
import com.srini.viewmodel.ThreadVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ThreadServiceImpl extends BaseServiceImpl implements IThreadService {

    @Autowired
    ThreadRepository threadRepository;

    @Transactional
    @Override
    public ThreadVM add(Thread fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        Thread savedModel = threadRepository.save(fromBodyModel);
        ThreadVM VM = new ThreadVM(savedModel);
        return VM;
    }

    @Override
    public ThreadVM update(Thread fromBodyModel, Long id) {
        try {
            Thread retrievedModel = threadRepository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setType(fromBodyModel.getType());
            retrievedModel.setBrandName(fromBodyModel.getBrandName());
            retrievedModel.setColor(fromBodyModel.getColor());
            //retrievedModel.setShape(fromBodyModel.getShape());
            Thread updatedModel = threadRepository.save(retrievedModel);
            return new ThreadVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.THREAD_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Thread retrievedModel = threadRepository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.THREAD_DELETE_ERROR);
        }
        int status = threadRepository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.THREAD_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<ThreadVM> getAll() {
        List<Thread> modelList = threadRepository.findAll(sortByIdDesc());
        List<ThreadVM> VMList = new ArrayList<>();
        for (Thread model : modelList) {
            VMList.add(new ThreadVM(model));
        }
        return VMList;
    }

    @Override
    public ThreadVM getById(Long id) {
        try {
            Thread retrievedModel = threadRepository.findOne(id);
            return new ThreadVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.THREAD_DOES_NOT_EXISTS);
        }
    }
}
