package com.srini.service;

import com.srini.model.Applique;
import com.srini.viewmodel.AppliqueVM;

import javax.transaction.Transactional;
import java.util.List;

public interface IAppliqueService {


    AppliqueVM add(Applique fromBodyModel);

    AppliqueVM update(Applique fromBodyModel, Long id);

    int delete(Long id);
    List<AppliqueVM> getAll();
    AppliqueVM getById(Long id);
}
