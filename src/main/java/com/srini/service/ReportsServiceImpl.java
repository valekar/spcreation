package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Inward;
import com.srini.model.Order;
import com.srini.model.Tag;
import com.srini.repository.InwardRepository;
import com.srini.repository.OrderRepository;
import com.srini.util.Utility;
import com.srini.viewmodel.OutwardReportVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ReportsServiceImpl extends BaseServiceImpl implements IReportsService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    InwardRepository inwardRepository;

    @Override
    public List<OutwardReportVM> getInwardsByCustomerId(Long id) {
        Set<Order> orders = orderRepository.findOrdersByCustomerId(id);
        List<OutwardReportVM> outwardReportVMList = new ArrayList<>();
        if (orders.size() > 0) {
            for (Order order : orders) {
                if (!order.getStatus().equals(CONSTANTS.ORDER_PENDING)) {
                    Inward inward = inwardRepository.findInwardByOrderId(order.getId());
                    if (inward != null) {
                        OutwardReportVM outwardReportVM = new OutwardReportVM();
                        outwardReportVM.setLay(Utility.getSimpleDateFormatter(inward.getLay()));
                        outwardReportVM.setPart(inward.getOrder().getPart());
                        int totalTags = 0;
                        int totalClothes = 0;
                        int totalCheckingMistakes = 0;
                        int totalProdMistakes = 0;
                        int totalProduced = 0;
                        int totalChecked = 0;
                        int toBeProdBalanceClothes = 0;
                        int toBeCheckBalanceClothes = 0;
                        int totalBundles = 0;

                        for (Tag tag : inward.getTags()) {
                            totalTags += 1;
                            totalClothes += tag.getQuantity();
                            totalBundles += tag.getBundle();
                            if (tag.getCheckingMistakes() != null) {
                                totalCheckingMistakes += tag.getCheckingMistakes();
                            }
                            if (tag.getMistakes() != null) {
                                totalProdMistakes += tag.getMistakes();
                            }
                            if (tag.getQuantityCompleted() != null) {
                                totalProduced += tag.getQuantityCompleted();
                            }
                            if (tag.getCheckingQuantityCompleted() != null) {
                                totalChecked += tag.getCheckingQuantityCompleted();
                            }
                            if (tag.getCheckingQuantityCompleted() != null) {
                                toBeCheckBalanceClothes += tag.getQuantity() - tag.getCheckingQuantityCompleted();
                            }
                            if (tag.getQuantityCompleted() != null) {
                                toBeProdBalanceClothes += tag.getQuantity() - tag.getQuantityCompleted();
                            }
                        }
                        outwardReportVM.setSPC(inward.getOrder().getSPC());
                        outwardReportVM.setToBeCheckBalanceClothes(toBeCheckBalanceClothes);
                        outwardReportVM.setToBeProdBalanceClothes(toBeProdBalanceClothes);
                        outwardReportVM.setTotalBundles(totalBundles);
                        outwardReportVM.setTotalTags(totalTags);
                        outwardReportVM.setTotalClothes(totalClothes);
                        outwardReportVM.setTotalCheckingMistakes(totalCheckingMistakes);
                        outwardReportVM.setTotalProdMistakes(totalProdMistakes);
                        outwardReportVM.setTotalProduced(totalProduced);
                        outwardReportVM.setTotalChecked(totalChecked);
                        outwardReportVM.setOrderStatus(inward.getOrder().getStatus());
                        outwardReportVM.setUpdated_by(inward.getUpdated_by());
                        outwardReportVM.setFormatted_updated_at(Utility.getSimpleDateFormatter(inward.getUpdated_at()));

                        outwardReportVMList.add(outwardReportVM);
                    }
                }
            }
        }
        return outwardReportVMList;
    }
}
