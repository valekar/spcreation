package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.*;
import com.srini.repository.CheckEmployeeRepository;
import com.srini.repository.CheckTagRepository;
import com.srini.repository.CheckingRepository;
import com.srini.repository.TagRepository;
import com.srini.util.Utility;
import com.srini.viewmodel.CheckEmployeeVM;
import com.srini.viewmodel.CheckTagVM;
import com.srini.viewmodel.CheckingVM;
import com.srini.viewmodel.TagVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CheckingServiceImpl extends BaseServiceImpl implements ICheckingService {

    @Autowired
    TagRepository tagRepository;

    @Autowired
    CheckingRepository repository;

    @Autowired
    CheckTagRepository checkTagRepository;

    @Autowired
    CheckEmployeeRepository checkEmployeeRepository;

    @Transactional(rollbackOn = DataIntegrityViolationException.class)
    @Override
    public CheckingVM add(CheckingVM fromBodyModel) throws ParseException,DataIntegrityViolationException {
        try {
            Checking checking = new Checking();
            checking.setPlanningDate(Utility.getTimestampFromString(fromBodyModel.getPlanningDate()));
            checking.setShift(fromBodyModel.getShift());
            Set<CheckEmployee> checkEmployeeSet = new HashSet<>();
            for (CheckEmployeeVM employeeVM : fromBodyModel.getEmployees()) {
                CheckEmployee checkEmployee = new CheckEmployee();
                checkEmployee.setChecker(employeeVM.getChecker());
                checkEmployee.setHelper(employeeVM.getHelper());
                checkEmployee.setAssignedQuantity(employeeVM.getAssignedQuantity());
                Set<CheckTag> savedTags = new HashSet<>();
                for (CheckTagVM checkTagVM : employeeVM.getAssignedTags()) {
                    Tag tag = checkTagVM.getTag();
                    tag.setStatus(CONSTANTS.TAG_STATUS_IN_CHECKING);
                    tag.setInward(tag.getInward());
                    CheckTag checkTag = new CheckTag();
                    checkTag.setTag(tagRepository.save(tag));
                    checkTag.setAddAuditFields();
                    savedTags.add(checkTagRepository.save(checkTag));
                }
                checkEmployee.setAssignedTags(savedTags);
                checkEmployee.setAddAuditFields();
                checkEmployeeSet.add(checkEmployeeRepository.save(checkEmployee));
            }

            checking.setEmployees(checkEmployeeSet);
            checking.setAddAuditFields();
            CheckingVM vm = new CheckingVM(repository.save(checking));
            return vm;
        }
        catch (Exception ex){
            throw new DataIntegrityViolationException(CONSTANTS.CHECKING_SAVE_ERROR);
        }
    }

    @Override
    public CheckingVM update(Checking fromBodyModel, Long id) {
        return null;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public List<CheckingVM> getAll() {
        List<Checking> checkings = repository.findAll(sortByIdDesc());
        List<CheckingVM> checkingVMList = new ArrayList<>();
        for(Checking checking:checkings){
            checkingVMList.add(new CheckingVM(checking));
        }
        return checkingVMList;
    }

    @Override
    public CheckingVM getById(Long id) {
        Checking checking = repository.findOne(id);
        return new CheckingVM(checking);
    }

    @Override
    public Set<Inward> getInwardsWithProducedTags() {
        Set<Tag> tags = tagRepository.getInwardByStatus(CONSTANTS.TAG_STATUS_IN_PRODUCED);
        Set<Inward> inwards = new HashSet<>();
        for (Tag tag : tags) {
            if (tag.getInward() != null) {
                inwards.add(tag.getInward());
            }
        }
        return inwards;
    }

    @Override
    public Set<TagVM> getTagsWithProducedStatus() {
        Set<Tag> tags = tagRepository.getInwardByStatus(CONSTANTS.TAG_STATUS_IN_PRODUCED);
        Set<TagVM> activeTagsVM = new HashSet<>();
        for (Tag tag : tags) {
            if (tag.getInward() != null) {
                activeTagsVM.add(new TagVM(tag));
            }
        }
        return activeTagsVM;
    }

}
