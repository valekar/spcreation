package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Paper;
import com.srini.repository.PaperRepository;
import com.srini.viewmodel.PaperVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaperServiceImpl extends BaseServiceImpl implements IPaperService {

    @Autowired
    PaperRepository paperRepository;

    @Transactional
    @Override
    public PaperVM add(Paper fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        Paper savedModel = paperRepository.save(fromBodyModel);
        PaperVM VM = new PaperVM(savedModel);
        return VM;
    }

    @Override
    public PaperVM update(Paper fromBodyModel, Long id) {
        try {
            Paper retrievedModel = paperRepository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setType(fromBodyModel.getType());
            retrievedModel.setQuality(fromBodyModel.getQuality());
            retrievedModel.setSize(fromBodyModel.getSize());
            Paper updatedModel = paperRepository.save(retrievedModel);
            return new PaperVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.PAPER_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Paper retrievedModel = paperRepository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.PAPER_DELETE_ERROR);
        }
        int status = paperRepository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.PAPER_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<PaperVM> getAll() {
        List<Paper> modelList = paperRepository.findAll(sortByIdDesc());
        List<PaperVM> VMList = new ArrayList<>();
        for (Paper model : modelList) {
            VMList.add(new PaperVM(model));
        }
        return VMList;
    }

    @Override
    public PaperVM getById(Long id) {
        try {
            Paper retrievedModel = paperRepository.findOne(id);
            return new PaperVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.PAPER_DOES_NOT_EXISTS);
        }
    }
}
