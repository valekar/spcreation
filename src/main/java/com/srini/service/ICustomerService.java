package com.srini.service;

import com.srini.model.Customer;
import com.srini.viewmodel.CustomerVM;

import java.util.List;

public interface ICustomerService {

    CustomerVM addCustomer(Customer customer);
    CustomerVM updateCustomer(Customer customer, Long id);
    int deleteCustomer(Long id);
    List<CustomerVM> getAllCustomers();
    CustomerVM getCustomerById(Long id);
}
