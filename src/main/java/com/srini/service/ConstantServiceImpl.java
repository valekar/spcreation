package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Constant;
import com.srini.repository.ConstantRepository;
import com.srini.viewmodel.ConstantVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ConstantServiceImpl  extends BaseServiceImpl implements IConstantService{
    @Autowired
    ConstantRepository repository;

    @Override
    public ConstantVM add(Constant fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        fromBodyModel.setValue(fromBodyModel.getValue().toUpperCase());
        fromBodyModel.setKey(fromBodyModel.getValue().toUpperCase());
        Constant savedModel = repository.save(fromBodyModel);
        ConstantVM VM = new ConstantVM(savedModel);
        return VM;
    }

    @Override
    public ConstantVM update(Constant fromBodyModel, Long id) {
        try {
            Constant retrievedModel = repository.findOne(id);
            fromBodyModel.setValue(fromBodyModel.getValue().toUpperCase());
            fromBodyModel.setKey(fromBodyModel.getValue().toUpperCase());
            retrievedModel.setEditAuditFields();
            retrievedModel.setKey(fromBodyModel.getKey());
            retrievedModel.setValue(fromBodyModel.getValue());
            Constant updatedModel = repository.save(retrievedModel);
            return new ConstantVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.CONSTANT_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Constant retrievedModel = repository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.CONSTANT_DELETE_ERROR);
        }
        int status = repository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.CONSTANT_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<ConstantVM> getAll() {
        List<Constant> modelList = repository.findAll(sortByIdDesc());
        List<ConstantVM> VMList = new ArrayList<>();
        for (Constant model : modelList) {
            VMList.add(new ConstantVM(model));
        }
        return VMList;
    }

    @Override
    public ConstantVM getById(Long id) {
        try {
            Constant retrievedModel = repository.findOne(id);
            return new ConstantVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.CONSTANT_DOES_NOT_EXISTS);
        }
    }
}
