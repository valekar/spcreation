package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Applique;
import com.srini.repository.AppliqueRepository;
import com.srini.viewmodel.AppliqueVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppliqueServiceImpl extends BaseServiceImpl implements IAppliqueService {


    @Autowired
    private AppliqueRepository appliqueRepository;

    @Transactional
    @Override
    public AppliqueVM add(Applique fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        Applique savedModel = appliqueRepository.save(fromBodyModel);
        AppliqueVM VM = new AppliqueVM(savedModel);
        return VM;
    }

    @Transactional
    @Override
    public AppliqueVM update(Applique fromBodyModel, Long id) {
        try {
            Applique retrievedModel = appliqueRepository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setColor(fromBodyModel.getColor());
            retrievedModel.setNumber(fromBodyModel.getNumber());
            retrievedModel.setType(fromBodyModel.getType());
            Applique updatedModel = appliqueRepository.save(retrievedModel);
            return new AppliqueVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.APPLIQUE_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Applique retrievedModel = appliqueRepository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.APPLIQUE_DELETE_ERROR);
        }
        int status = appliqueRepository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.APPLIQUE_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<AppliqueVM> getAll() {
        List<Applique> modelList = appliqueRepository.findAll(sortByIdDesc());
        List<AppliqueVM> VMList = new ArrayList<>();
        for (Applique model : modelList) {
            VMList.add(new AppliqueVM(model));
        }
        return VMList;
    }

    @Override
    public AppliqueVM getById(Long id) {
        try {
            Applique retrievedModel = appliqueRepository.findOne(id);
            return new AppliqueVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.APPLIQUE_DOES_NOT_EXISTS);
        }
    }
}
