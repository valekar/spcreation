package com.srini.service;

import com.srini.component.LoggedInUser;
import com.srini.config.LoggedInUserSingleton;
import com.srini.constant.CONSTANTS;
import com.srini.model.Department;
import com.srini.model.Employee;
import com.srini.repository.EmployeeRepository;
import com.srini.util.Utility;
import com.srini.viewmodel.EmployeeVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.rmi.CORBA.Util;
import javax.transaction.Transactional;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Transactional
    @Override
    public EmployeeVM addEmployee(Employee employee) {
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();

        employee.setCreated_at(Utility.getTime());
        employee.setUpdated_at(Utility.getTime());
        employee.setCreated_by(loggedInUser.getUserName());
        employee.setCreated_user_id(loggedInUser.getUserId());

        employee.setUpdated_by(loggedInUser.getUserName());
        employee.setUpdated_user_id(loggedInUser.getUserId());
        Employee emp = employeeRepository.save(employee);
        //emp.setUpdated_at(emp.getCreated_at().);
        EmployeeVM employeeVM = new EmployeeVM(emp);
        return employeeVM;
    }

    @Transactional
    @Override
    public EmployeeVM updateEmployee(Employee employee,Long id) throws NullPointerException{
        try{
            LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
            Employee emp = employeeRepository.getOne(id);
            emp.setUpdated_at(Utility.getTime());
            emp.setAddress(employee.getAddress());
            emp.setAge(employee.getAge());
            emp.setBlood_group(employee.getBlood_group());
            emp.setName(employee.getName());
            emp.setSex(employee.getSex());

            //set departments
            emp.setDepartments(employee.getDepartments());

            emp.setUpdated_by(loggedInUser.getUserName());
            emp.setUpdated_user_id(loggedInUser.getUserId());
            Employee updatedEmp = employeeRepository.save(emp);
            EmployeeVM employeeVM = new EmployeeVM(employee);
            return employeeVM;
        }
        catch (NullPointerException ex){
            throw new NullPointerException(CONSTANTS.EMPLOYEE_DOES_NOT_EXIST);
        }
    }

    @Transactional
    @Override
    public int deleteEmployee(Long id) throws  DataIntegrityViolationException{
        Employee emp = employeeRepository.findOne(id);
        if(emp==null){
            throw new DataIntegrityViolationException(CONSTANTS.EMPLOYEE_DELETE_ERROR);
        }
        int status = employeeRepository.delete(id,false);
        //int status = employeeRepository.deleteById(id);

        if(status == 0){
            throw new DataIntegrityViolationException(CONSTANTS.EMPLOYEE_DELETE_ERROR);
        }
        else{
            return status;
        }
    }
    @Override
    public List<EmployeeVM> getAllEmployees() {
        List<Employee> list = employeeRepository.findAll(sortByIdDesc());
        List<EmployeeVM> employeeVMList = new ArrayList<>();
        for(Employee employee : list){
            employeeVMList.add(new EmployeeVM(employee));
        }
        return employeeVMList;
    }

    @Override
    public EmployeeVM getEmployeeById(Long id) {
        EmployeeVM employeeVM = new EmployeeVM(employeeRepository.findOne(id));
        return employeeVM;
    }

    @Override
    public EmployeeVM getEmployeeByName(String name) {
        return new EmployeeVM(employeeRepository.findByName(name));
    }

    @Override
    public List<EmployeeVM> getEmployeesByDepartmentType(String employeesType) {
        List<Employee> employeeList = employeeRepository.findAll(sortByIdDesc());
        List<EmployeeVM> employeeVMList = new ArrayList<>();

        for(Employee  employee: employeeList){
            boolean employeeOfDepartment = false;
            for(Department department : employee.getDepartments()){
                if(department.getName().toUpperCase().equals(employeesType.toUpperCase())){
                    employeeOfDepartment = true;
                    break;
                }
            }
            employeeVMList.add(new EmployeeVM(employee));
        }
        return employeeVMList;
    }

    private Sort sortByIdDesc() {
        return new Sort(Sort.Direction.DESC, "id");
    }
}
