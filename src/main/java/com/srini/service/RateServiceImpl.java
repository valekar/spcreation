package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Rate;
import com.srini.repository.RateRepository;
import com.srini.viewmodel.RateVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class RateServiceImpl extends BaseServiceImpl implements IRateService {

    @Autowired
    RateRepository rateRepository;

    @Transactional
    @Override
    public RateVM addRate(Rate rate) {
        rate.setAddAuditFields();
        Rate savedRate = rateRepository.save(rate);
        RateVM rateVM = new RateVM(savedRate);
        return rateVM;
    }

    @Override
    public RateVM updateRate(Rate rate, Long id) {
        try {
            Rate ra = rateRepository.findOne(id);
            ra.setEditAuditFields();
            ra.setMachineType(rate.getMachineType());
            ra.setRate(rate.getRate());
            ra.setStitches(rate.getStitches());
            Rate updatedRate = rateRepository.save(ra);
            return new RateVM(updatedRate);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.RATE_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int deleteRate(Long id) {
        Rate rate = rateRepository.findOne(id);
        if (rate == null) {
            throw new DataIntegrityViolationException(CONSTANTS.RATE_DELETE_ERROR);
        }
        int status = rateRepository.delete(id, false);
        //int status = customerRepository.deleteById(id);
        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.RATE_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<RateVM> getAllRates() {
        List<Rate> rateList = rateRepository.findAll(sortByIdDesc());
        List<RateVM> rateVMList = new ArrayList<>();
        for (Rate rate : rateList) {
            rateVMList.add(new RateVM(rate));
        }
        return rateVMList;
    }

    @Override
    public RateVM getRatesById(Long id) {
        try {
            Rate rate = rateRepository.findOne(id);
            return new RateVM(rate);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.RATE_DOES_NOT_EXISTS);
        }
    }
}
