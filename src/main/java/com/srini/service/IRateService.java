package com.srini.service;

import com.srini.model.Rate;
import com.srini.viewmodel.RateVM;

import java.util.List;

public interface IRateService {

    RateVM addRate(Rate rate);
    RateVM updateRate(Rate rate, Long id);
    int deleteRate(Long id);
    List<RateVM> getAllRates();
    RateVM getRatesById(Long id);

}
