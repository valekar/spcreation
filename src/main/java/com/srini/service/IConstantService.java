package com.srini.service;

import com.srini.model.Constant;
import com.srini.viewmodel.ConstantVM;

import java.util.List;

public interface IConstantService {

    ConstantVM add(Constant fromBodyModel);
    ConstantVM update(Constant fromBodyModel, Long id);
    int delete(Long id);
    List<ConstantVM> getAll();
    ConstantVM getById(Long id);
}
