package com.srini.service;

import com.srini.model.Production;
import com.srini.viewmodel.InwardVM;
import com.srini.viewmodel.PlanningForm;
import com.srini.viewmodel.ProductionVM;

import java.text.ParseException;
import java.util.List;

public interface IProductionService {

    //List<InwardVM> getInward();
    InwardVM confirmOrderWithInward(Long inwardId);
    List<InwardVM> orderApprovedOrConfirmedInwards();
    List<InwardVM> confirmedOrderInwards();

    Production setPlanner(PlanningForm planningForm) throws ParseException;

    ProductionVM getById(Long id);

    ProductionVM getProdWithInProductionTags(Long id);

    List<ProductionVM> getAll();
}
