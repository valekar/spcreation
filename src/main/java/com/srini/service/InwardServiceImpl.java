package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Inward;
import com.srini.model.Order;
import com.srini.repository.InwardRepository;
import com.srini.repository.OrderRepository;
import com.srini.util.Utility;
import com.srini.viewmodel.InwardVM;
import com.srini.viewmodel.OrderVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class InwardServiceImpl extends BaseServiceImpl implements IInwardService{

    @Autowired
    private InwardRepository repository;

    @Autowired
    private  OrderRepository orderRepository;

    @Autowired
    private ImageService imageService;

    @PersistenceContext
    EntityManager entityManager;
    
    @Override
    public InwardVM add(Inward fromBodyModel) throws ParseException{
        fromBodyModel.setAddAuditFields();
        fromBodyModel.setLay(Utility.getTimestampFromTimeZoneString(fromBodyModel.getLayStr()));
        Inward savedModel = repository.save(fromBodyModel);
        InwardVM VM = new InwardVM(savedModel);
        return VM;
    }

    @Override
    public InwardVM update(Inward fromBodyModel, Long id) throws NullPointerException,ParseException{
        try {
            Inward retrievedModel = repository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setOrder(fromBodyModel.getOrder());
            retrievedModel.setInwardNumber(fromBodyModel.getInwardNumber());
            retrievedModel.setLay(Utility.getTimestampFromTimeZoneString(fromBodyModel.getLayStr()));
            Inward updatedModel = repository.save(retrievedModel);
            return new InwardVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.INWARD_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Inward retrievedModel = repository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.INWARD_DELETE_ERROR);
        }
        int status = repository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.INWARD_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<InwardVM> getAll() {
        List<Inward> modelList = repository.findAll(sortByIdDesc());
        List<InwardVM> VMList = new ArrayList<>();
        for (Inward model : modelList) {
            VMList.add(new InwardVM(model));
        }
        return VMList;
    }



    @Override
    public InwardVM getById(Long id) throws NullPointerException{
        try {
            Inward retrievedModel = repository.findOne(id);
            return new InwardVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.INWARD_DOES_NOT_EXISTS);
        }
    }

    @Override
    public List<OrderVM> getOnlyApprovedOrders(Long page,Long size) throws IOException{
        long count = size;
        if(size==0) {
            count = repository.count();
        }
        if(count == 0){
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
        Page<Order> orders = orderRepository.findAllByStatus(new PageRequest(0,(int) count),CONSTANTS.ORDER_APPROVE);
        List<Order> orderList = new ArrayList<Order>();
        for(Order order : orders){
            orderList.add(order);
        }
        List<InwardVM> inwardVMList = getAll();
        List<Order> orderListFromInward = new ArrayList<>();
        for(InwardVM inwardVM : inwardVMList){
            orderListFromInward.add(inwardVM.getOrder());
        }
        List<Order> ordersToBeRetained = new ArrayList<>();
        List<OrderVM> vmList = new ArrayList<>();

        orderList.removeAll(orderListFromInward);

        for(Order model : orderList){
            OrderVM newOrderVM = new OrderVM(model);
            Resource resource = imageService.findOneImage(model.getImage().getName());
            newOrderVM.setStatus(newOrderVM.getStatus().toUpperCase());
            newOrderVM.setImageResource(imageService.findOneImage(model.getImage().getName()));
            vmList.add(newOrderVM);
        }

        return vmList;

    }


}
