package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Department;
import com.srini.repository.DepartmentRepository;
import com.srini.viewmodel.DepartmentVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl extends BaseServiceImpl implements IDepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    @Transactional
    @Override
    public DepartmentVM add(Department fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        Department savedModel = departmentRepository.save(fromBodyModel);
        DepartmentVM VM = new DepartmentVM(savedModel);
        return VM;
    }

    @Transactional
    @Override
    public DepartmentVM update(Department fromBodyModel, Long id) {
        try {
            Department retrievedModel = departmentRepository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setName(fromBodyModel.getName());
            Department updatedModel = departmentRepository.save(retrievedModel);
            return new DepartmentVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.DEPARTMENT_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Department retrievedModel = departmentRepository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.DEPARTMENT_DELETE_ERROR);
        }
        int status = departmentRepository.delete(id, false);

        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.DEPARTMENT_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<DepartmentVM> getAll() {
        List<Department> modelList = departmentRepository.findAll(sortByIdDesc());
        List<DepartmentVM> VMList = new ArrayList<>();
        for (Department model : modelList) {
            VMList.add(new DepartmentVM(model));
        }
        return VMList;
    }

    @Override
    public DepartmentVM getById(Long id) {
        try {
            Department retrievedModel = departmentRepository.findOne(id);
            return new DepartmentVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.DEPARTMENT_DOES_NOT_EXISTS);
        }
    }
}
