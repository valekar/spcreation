package com.srini.service;

import com.srini.model.Inward;
import com.srini.viewmodel.InwardVM;
import com.srini.viewmodel.OrderVM;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

public interface IInwardService {

    InwardVM add(Inward fromBodyModel) throws ParseException;
    InwardVM update(Inward fromBodyModel, Long id) throws ParseException;
    int delete(Long id);
    List<InwardVM> getAll();
    InwardVM getById(Long id);
    List<OrderVM> getOnlyApprovedOrders(Long page,Long size) throws IOException;
}
