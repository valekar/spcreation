package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.SAM_SPC_counter;
import com.srini.repository.SAMSPCCounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class SAMSPCServiceImpl extends  BaseServiceImpl implements ISAMSPCCounterService {

    @Autowired
    SAMSPCCounterRepository samspcCounterRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public SAM_SPC_counter save() {
        SAM_SPC_counter sam_spc_counter = new SAM_SPC_counter();
        SAM_SPC_counter sam_spc_counter1 = samspcCounterRepository.save(sam_spc_counter);
        return sam_spc_counter1;
    }

    @Override
    public int resetTable() throws DataIntegrityViolationException{
        int status = samspcCounterRepository.resetTable();
        if(status==0){
            throw new DataIntegrityViolationException(CONSTANTS.SAM_SPC_DELETE_TABLE_ERROR);
        }
        return status;
    }

    @Override
    public int resetSequence() throws DataIntegrityViolationException{

        try{
            entityManager
                    .createNativeQuery("ALTER SEQUENCE SAM_SPC_counter_id_seq RESTART WITH 1")
                    .executeUpdate();
        }
        catch (Exception ex){
            throw new DataIntegrityViolationException(CONSTANTS.SAM_SPC_DELETE_SEQ_ERROR);
        }

        return 1;
    }
}
