package com.srini.service;

import com.srini.model.Department;
import com.srini.viewmodel.DepartmentVM;

import java.util.List;

public interface IDepartmentService {

    DepartmentVM add(Department fromBodyModel);
    DepartmentVM update(Department fromBodyModel, Long id);
    int delete(Long id);
    List<DepartmentVM> getAll();
    DepartmentVM getById(Long id);
}
