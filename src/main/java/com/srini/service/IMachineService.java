package com.srini.service;

import com.srini.model.Machine;
import com.srini.viewmodel.MachineVM;

import java.util.List;

public interface IMachineService {

    MachineVM addMachine(Machine machine);
    MachineVM updateMachine(Machine machine, Long id);
    int deleteMachine(Long id);
    List<MachineVM> getAllMachines();
    MachineVM getMachinesById(Long id);
}
