package com.srini.service;

import com.srini.model.Thread;
import com.srini.viewmodel.ThreadVM;

import java.util.List;

public interface IThreadService {
    ThreadVM add(Thread fromBodyModel);
    ThreadVM update(Thread fromBodyModel, Long id);
    int delete(Long id);
    List<ThreadVM> getAll();
    ThreadVM getById(Long id);

}
