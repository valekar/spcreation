package com.srini.service;

import com.srini.model.Order;
import com.srini.viewmodel.OrderVM;
import com.sun.org.apache.xpath.internal.operations.Mult;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IOrderService {

    OrderVM add(Order fromBodyModel, MultipartFile file) throws IOException,MultipartException;

    OrderVM update(Order fromBodyModel, MultipartFile file, Long id) throws IOException,MultipartException;

    int delete(Long id) throws IOException;
    List<OrderVM> getAll(int page,int size) throws IOException;
    OrderVM getById(Long id) throws IOException;

    long getOrderCount();

    OrderVM approveOrder(Long id) throws IOException;
    OrderVM rejectOrder(Long id) throws  IOException;
    OrderVM pendingOrder(Long id) throws IOException;

}
