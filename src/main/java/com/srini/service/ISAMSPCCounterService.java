package com.srini.service;

import com.srini.model.SAM_SPC_counter;

public interface ISAMSPCCounterService {

    SAM_SPC_counter save();

    int resetTable();

    int resetSequence();
}
