package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.*;
import com.srini.repository.*;
import com.srini.util.Utility;
import com.srini.viewmodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ProductionImpl extends BaseServiceImpl implements IProductionService {

    @Autowired
    InwardRepository inwardRepository;
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductionRepository repository;

    @Autowired
    ProdMachineRepository prodMachineRepository;

    @Autowired
    ProdInwardRepository prodInwardRepository;

    @Autowired
    TagRepository tagRepository;

    @Override
    public InwardVM confirmOrderWithInward(Long inwardId) {
        Inward inward = inwardRepository.findOne(inwardId);
        inward.getOrder().setStatus(CONSTANTS.ORDER_CONFIRMED);
        inward.setEditAuditFields();
        inward = inwardRepository.save(inward);
        InwardVM inwardVM = new InwardVM(inward);
        return inwardVM;
    }

    @Override
    public List<InwardVM> orderApprovedOrConfirmedInwards() {
        List<Inward> inwards = inwardRepository.findAll();
        List<InwardVM> vmList = new ArrayList<>();
        for (Inward inward : inwards) {
            boolean isActive = inward.getOrder().isActive();
            if ((inward.getOrder().getStatus().equals(CONSTANTS.ORDER_APPROVE) ||
                    inward.getOrder().getStatus().equals(CONSTANTS.ORDER_CONFIRMED)) && isActive) {
                InwardVM inwardVM = new InwardVM(inward);
                vmList.add(inwardVM);
            }
        }
        //if(vmList.size()<=0){
        //    throw new NullPointerException(CONSTANTS.INWARD_DOES_NOT_EXISTS);
        //}

        return vmList;
    }

    @Override
    public List<InwardVM> confirmedOrderInwards() {
        List<Inward> inwards = inwardRepository.findAll();
        List<InwardVM> vmList = new ArrayList<>();
        for (Inward inward : inwards) {
            String status = inward.getOrder().getStatus();
            boolean isActive = inward.getOrder().isActive();
            if (status.equals(CONSTANTS.ORDER_CONFIRMED) && isActive) {
                InwardVM inwardVM = new InwardVM(inward);
                vmList.add(inwardVM);
            }
        }
        return vmList;
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class})
    @Override
    public Production setPlanner(PlanningForm planningForm) throws ParseException, DataIntegrityViolationException {
        try {
            Production production = new Production();
            production.setPlanningDate(Utility.getTimestampFromString(planningForm.getPlanningDate()));
            production.setShift(planningForm.getShift());
            //production.setMachines(planningForm.getMachines());
            List<ProdMachine> prodMachineList = new ArrayList<>();
            if (!planningForm.getMachines().isEmpty()) {
                for (MachineForm machineForm : planningForm.getMachines()) {
                    ProdMachine prodMachine = new ProdMachine();
                    prodMachine.setHeads(machineForm.getHeadsField());
                    prodMachine.setName(machineForm.getName());
                    prodMachine.setNoOfOrders(machineForm.getNoOfOrders());
                    prodMachine.setStitchCapacity(machineForm.getStitchCapacityField());
                    prodMachine.setType(machineForm.getTypeField());
                    List<ProdInward> prodInwardList = new ArrayList<>();
                    if (!machineForm.getOrders().isEmpty()) {
                        for (OrderForm orderForm : machineForm.getOrders()) {
                            ProdInward prodInward = new ProdInward();
                            prodInward.setTagsClothQuantity(orderForm.getTagsClothesQuantity());
                            prodInward.setInCharge(orderForm.getInchargeField());
                            prodInward.setFramer(orderForm.getFramerField());
                            prodInward.setOperator(orderForm.getOperatorField());

                            if (!orderForm.getTagsToBeProcessed().isEmpty()) { //dragged tags
                                for (TagForm toBetTagForm : orderForm.getTagsToBeProcessed()) {
                                    for (TagForm inwardTagForm : orderForm.getTags()) { // orderForm.getTags() we have values, we are passing all
                                        Tag saveTag = toBetTagForm.getTag();
                                        if (saveTag.getId() == inwardTagForm.getTag().getId()) {
                                            saveTag.setStatus(CONSTANTS.TAG_STATUS_IN_PRODUCTION);
                                            saveTag.setEditAuditFields();
                                            saveTag.setInward(orderForm.getInwardField()); //this needs to be set otherwise we will loose the inward connection
                                            tagRepository.save(saveTag);
                                        }
                                    }
                                }
                                //we are setting toBeprocessed coz we only need to show tags that are related to a particular plan
                                Set<Tag> toBeProcessedTags = new HashSet<>();
                                for (TagForm tagForm : orderForm.getTagsToBeProcessed()) {
                                    toBeProcessedTags.add(tagForm.getTag());
                                }
                                prodInward.setToBeProcessedTags(toBeProcessedTags);
                            }
                            prodInward.setInward(orderForm.getInwardField());
                            Set<Tag> allTags = new HashSet<>();
                            for (TagForm tagForm : orderForm.getTags()) {
                                allTags.add(tagForm.getTag());
                            }
                            prodInward.getInward().setTags(allTags);
                            prodInwardList.add(prodInward);
                        }
                    }
                    Set<ProdInward> savedProdInwardList = new HashSet<>();
                    for (ProdInward order : prodInwardList) {
                        order.setAddAuditFields();
                        savedProdInwardList.add(prodInwardRepository.save(order));
                    }

                    prodMachine.setOrders(savedProdInwardList);
                    prodMachineList.add(prodMachine);
                }
            }
            Set<ProdMachine> savedMachineList = new HashSet<>();
            for (ProdMachine machine : prodMachineList) {
                machine.setAddAuditFields();
                savedMachineList.add(prodMachineRepository.save(machine));
            }
            production.setMachines(savedMachineList);
            production.setAddAuditFields();
            production = repository.save(production);
            //production = repository.findOne(production.getId());
            return production;
        } catch (Exception ex) {
            throw new DataIntegrityViolationException(CONSTANTS.FAILED);
        }

    }

    @Override
    public ProductionVM getById(Long id) {
        try {
            Production retrievedModel = repository.findOne(id);

            return new ProductionVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.PRODUCTION_DOES_NOT_EXISTS);
        }
    }

    @Override
    public ProductionVM getProdWithInProductionTags(Long id) {
        try {
            Production retrievedModel = repository.findOne(id);
            Set<ProdMachine> prodMachines = new HashSet<>();
            for (ProdMachine machine : retrievedModel.getMachines()) {
                Set<ProdInward> prodInwardList = new HashSet<>();
                for (ProdInward inward : machine.getOrders()) {
                    Set<Tag> inProductionTags = new HashSet<>();
                    for (Tag tag : inward.getToBeProcessedTags()) {
                        if (tag.getStatus().equals(CONSTANTS.TAG_STATUS_IN_PRODUCTION)) {
                            inProductionTags.add(tag);
                        }
                    }
                    inward.getInward().setTags(inProductionTags);
                    prodInwardList.add(inward);
                }
                machine.setOrders(prodInwardList);
                prodMachines.add(machine);
            }
            retrievedModel.setMachines(prodMachines);
            return new ProductionVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.PRODUCTION_DOES_NOT_EXISTS);
        }
    }

    @Override
    public List<ProductionVM> getAll() {
        List<Production> modelList = repository.findAll(sortByIdDesc());
        List<ProductionVM> vmList = new ArrayList<>();
        for (Production production : modelList) {
            vmList.add(new ProductionVM(production));
        }
        return vmList;
    }

}
