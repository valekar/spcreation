package com.srini.service;

import com.srini.viewmodel.OutwardReportVM;

import java.util.List;

public interface IReportsService  {

    List<OutwardReportVM> getInwardsByCustomerId(Long id);

}
