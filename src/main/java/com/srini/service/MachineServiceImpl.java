package com.srini.service;

import com.srini.config.LoggedInUserSingleton;
import com.srini.constant.CONSTANTS;
import com.srini.model.Machine;
import com.srini.repository.MachineRepository;
import com.srini.service.IMachineService;
import com.srini.util.Utility;
import com.srini.viewmodel.MachineVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class MachineServiceImpl extends BaseServiceImpl implements IMachineService{

    @Autowired
    MachineRepository machineRepository;

    @Transactional
    @Override
    public MachineVM addMachine(Machine machine) {
        machine.setAddAuditFields();
        Machine savedMachine = machineRepository.save(machine);
        MachineVM machineVM = new MachineVM(savedMachine);
        return machineVM;

    }

    @Transactional
    @Override
    public MachineVM updateMachine(Machine machineFromBody, Long id) {
        try {
            Machine mach = machineRepository.findOne(id);
            mach.setHeads(machineFromBody.getHeads());
            mach.setName(machineFromBody.getName());
            mach.setEditAuditFields();
            mach.setType(machineFromBody.getType());
            mach.setStitchCapacity(machineFromBody.getStitchCapacity());
            Machine updatedCustomer = machineRepository.save(mach);
            return new MachineVM(updatedCustomer);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.MACHINE_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int deleteMachine(Long id) {
        Machine machine = machineRepository.findOne(id);
        if (machine == null) {
            throw new DataIntegrityViolationException(CONSTANTS.MACHINE_DELETE_ERROR);
        }
        int status = machineRepository.delete(id, false);
        //int status = customerRepository.deleteById(id);
        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.MACHINE_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<MachineVM> getAllMachines() {
        List<Machine> machineList = machineRepository.findAll(sortByIdDesc());
        List<MachineVM> machineVMList = new ArrayList<>();
        for (Machine machine : machineList) {
            machineVMList.add(new MachineVM(machine));
        }
        return machineVMList;
    }

    @Override
    public MachineVM getMachinesById(Long id) {
        try {
            Machine machine = machineRepository.findOne(id);
            return new MachineVM(machine);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.MACHINE_DOES_NOT_EXISTS);
        }
    }
}
