package com.srini.service;

import com.srini.model.Employee;
import com.srini.repository.EmployeeRepository;
import com.srini.viewmodel.EmployeeVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EmployeeService {

    EmployeeVM addEmployee(Employee employee);
    EmployeeVM updateEmployee(Employee employee,Long id);
    int deleteEmployee(Long id);
    List<EmployeeVM> getAllEmployees();
    EmployeeVM getEmployeeById(Long id);
    EmployeeVM getEmployeeByName(String name);

    List<EmployeeVM> getEmployeesByDepartmentType(String employeesType);

}
