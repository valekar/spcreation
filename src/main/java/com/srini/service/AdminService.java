package com.srini.service;

import com.srini.component.LoggedInUser;
import com.srini.config.MyUserPrincipal;
import com.srini.constant.CONSTANTS;
import com.srini.exception.CustomException;
import com.srini.exception.UserNotFoundException;
import com.srini.model.Privilege;
import com.srini.model.User;
import com.srini.repository.UserRepository;
import com.srini.util.Mail;
import com.srini.viewmodel.ModifiedUserVM;
import freemarker.template.Configuration;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminService {

    final private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ResetPasswordService resetPasswordService;

    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    JavaMailSender sender;

    @Autowired
    ISAMSPCCounterService isamspcCounterService;

    @PersistenceContext
    private EntityManager entityManager;

    @Value("${client.url}")
    private String client_url;

    @Value("${owner.email}")
    private String owner_email;

    public List<ModifiedUserVM> getAllUsers(){
       // List<User> userList = userRepository.findAllOrderByIdDesc();
        List<User> userList = userRepository.findAll(sortByIdDesc());
        List<ModifiedUserVM> modifiedUserList = new ArrayList<>();
        for(User user: userList){
            ModifiedUserVM mUser = new ModifiedUserVM(user);
            modifiedUserList.add(mUser);
        }
        return modifiedUserList;
    }

    private Sort sortByIdDesc() {
        return new Sort(Sort.Direction.DESC, "id");
    }

    @Transactional
    public int deleteUser(Long id) throws CustomException{
        MyUserPrincipal mUser = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(mUser.getUser().getId() == id){
            throw new CustomException(CONSTANTS.CANNOT_DELETE_USER);
        }
        User user = userRepository.findOne(id);
        if(user == null){
            throw new CustomException(CONSTANTS.CANNOT_DELETE_USER);
        }
        //int status = userRepository.delete(id,false);
        int status = userRepository.deleteById(id);
        return status;
    }

    public ModifiedUserVM updateUser(ModifiedUserVM modifiedUserVM,Long userId) throws NullPointerException{
        try {
            User user = userRepository.findOne(userId);
            if(modifiedUserVM.getInward_tab()!=null) {
                modifiedUserVM.getPrivileges().setInward_tab(modifiedUserVM.getInward_tab());
                //user.setPrivileges(modifiedUserVM.getPrivileges());
            }
            if(modifiedUserVM.getAdmin_tab()!=null) {
                modifiedUserVM.getPrivileges().setAdmin_tab(modifiedUserVM.getAdmin_tab());
            }
            if(modifiedUserVM.getChecking_tab()!=null) {
                modifiedUserVM.getPrivileges().setChecking_tab(modifiedUserVM.getChecking_tab());
            }
            if(modifiedUserVM.getProduction_tab()!=null) {
                modifiedUserVM.getPrivileges().setProduction_tab(modifiedUserVM.getProduction_tab());
            }
            if(modifiedUserVM.getCompany_tab()!=null) {
                modifiedUserVM.getPrivileges().setCompany_tab(modifiedUserVM.getCompany_tab());
            }
            if(modifiedUserVM.getStore_tab()!=null) {
                modifiedUserVM.getPrivileges().setStore_tab(modifiedUserVM.getStore_tab());
            }
            if(modifiedUserVM.getReports_tab()!=null) {
                modifiedUserVM.getPrivileges().setReports_tab(modifiedUserVM.getReports_tab());
            }
            if(modifiedUserVM.getOrder_tab()!=null) {
                modifiedUserVM.getPrivileges().setOrder_tab(modifiedUserVM.getOrder_tab());
            }
            user.setPrivileges(modifiedUserVM.getPrivileges());
            if(modifiedUserVM.getEmail()!=null && modifiedUserVM.getEmail().length()>0)
                user.setEmail(modifiedUserVM.getEmail());

            user.setEnabled(modifiedUserVM.isEnabled());
            user.setEmail(modifiedUserVM.getEmail());
            user.setUsername(modifiedUserVM.getUsername());

            //save user
            User updatedUser = userRepository.save(user);
            ModifiedUserVM userVM = new ModifiedUserVM(updatedUser);
            return userVM;
        }
        catch (NullPointerException ex){
            throw new NullPointerException(CONSTANTS.USER_DOES_NOT_EXIST);
        }
    }

    @Transactional
    public ModifiedUserVM saveMUser(ModifiedUserVM mUser) throws DataIntegrityViolationException,CustomException{
        try {
            User user = new User();
            Privilege privilege = new Privilege();
            privilege.setAdmin_tab(mUser.getAdmin_tab());
            privilege.setInward_tab(mUser.getInward_tab());
            user.setEmail(mUser.getEmail());
            user.setEnabled(mUser.isEnabled());
            user.setPrivileges(privilege);
            user.setUsername(mUser.getUsername());
            //String defaultPassword = "password";
            String characters = CONSTANTS.CHARACTERS;
            String pwd = RandomStringUtils.random(4,characters);
            user.setPassword(passwordEncoder.encode(pwd));
            user = userRepository.save(user);
            LoggedInUser loggedInUserService = new LoggedInUser();
            if(user!=null){
                sendEmail(user.getUsername(),pwd,loggedInUserService.getUserName(),user.getEmail(),loggedInUserService.getUserEmail());
            }

            ModifiedUserVM modifiedUserVM = new ModifiedUserVM(user);
            return modifiedUserVM;
        }
        catch (DataIntegrityViolationException ex){
            throw new DataIntegrityViolationException("User" , ex);
        }
        catch (Exception ex){
            throw new CustomException(CONSTANTS.EMAIL_NOT_SENT);
        }
    }

    @Transactional
    public int resetCounter() throws DataIntegrityViolationException{
        int status = isamspcCounterService.resetTable();
        if(status ==1){
            status = isamspcCounterService.resetSequence();
        }
        if(status == 0){
            throw new DataIntegrityViolationException(CONSTANTS.SAM_SPC_DELETE_TABLE_ERROR);
        }
        return status;
    }

    private void sendEmail(String UserName, String password,String SenderName,String email,String loggedInUserEmail) throws Exception{
        //String recipient = email;//uncomment when mail gun is purchased
        String recipient = this.owner_email; //comment when mail gun is purchased
        String content = "";
        String subject = CONSTANTS.WELCOME_USER;
        String CC = loggedInUserEmail;
        String template_type = CONSTANTS.WELCOME_USER_TEMPLATE;
        Map<String,String> details = new HashMap<>();
        details.put(CONSTANTS.username_key, UserName);
        details.put(CONSTANTS.sender_name_key,SenderName);
        details.put(CONSTANTS.password_key,password);
        details.put(CONSTANTS.login_url_key, this.client_url + CONSTANTS.LOGIN_URL);
        details.put(CONSTANTS.action_url_key,"");
        details.put(CONSTANTS.help_url, this.client_url + CONSTANTS.HELP_URL);
        Mail.sendEmail(freemarkerConfig,this.getClass(),template_type,recipient,content,subject,sender,details,CC);

    }



}
