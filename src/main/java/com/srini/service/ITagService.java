package com.srini.service;

import com.srini.model.Tag;
import com.srini.viewmodel.TagVM;

import java.text.ParseException;
import java.util.List;

public interface ITagService  {

    TagVM add(Tag fromBodyModel);
    TagVM update(Tag fromBodyModel, Long id);
    int delete(Long id);
    List<TagVM> getAll();
    TagVM getById(Long id);
    List<TagVM> getAllByInwardId(Long id);
}
