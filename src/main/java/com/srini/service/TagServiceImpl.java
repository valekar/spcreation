package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Tag;
import com.srini.repository.TagRepository;
import com.srini.viewmodel.TagVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class TagServiceImpl extends BaseServiceImpl implements ITagService {

    @Autowired
    TagRepository repository;

    @Override
    public TagVM add(Tag fromBodyModel) {
        fromBodyModel.setAddAuditFields();
        Tag savedModel = repository.save(fromBodyModel);
        savedModel.setTagId("M"+savedModel.getId());
        savedModel.setStatus(CONSTANTS.ORDER_PENDING.toUpperCase());
        savedModel.setMistakes(0L);
        savedModel.setQuantityCompleted(0L);
        savedModel.setCheckingQuantityCompleted(0L);
        savedModel.setCheckingMistakes(0L);
        Tag savedModel2 = repository.save(fromBodyModel);
        TagVM VM = new TagVM(savedModel2);
        return VM;
    }

    @Override
    public TagVM update(Tag fromBodyModel, Long id) throws NullPointerException{
        try {
            Tag retrievedModel = repository.findOne(id);
            retrievedModel.setEditAuditFields();
            retrievedModel.setBundle(fromBodyModel.getBundle());
            retrievedModel.setComments(fromBodyModel.getComments());
            if(fromBodyModel.getInward() != null){
                retrievedModel.setInward(fromBodyModel.getInward());
            }

            retrievedModel.setQuantity(fromBodyModel.getQuantity());
            retrievedModel.setTagId(fromBodyModel.getTagId());
            retrievedModel.setSize(fromBodyModel.getSize());
            retrievedModel.setQuantityCompleted(fromBodyModel.getQuantityCompleted());
            retrievedModel.setMistakes(fromBodyModel.getMistakes());
            retrievedModel.setStatus(fromBodyModel.getStatus());
            retrievedModel.setCheckingMistakes(fromBodyModel.getCheckingMistakes());
            retrievedModel.setCheckingQuantityCompleted(fromBodyModel.getCheckingQuantityCompleted());
            Tag updatedModel = repository.save(retrievedModel);

            return new TagVM(updatedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.TAG_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) {
        Tag retrievedModel = repository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.TAG_DELETE_ERROR);
        }
        int status = 0;
        if(!retrievedModel.getStatus().equals(CONSTANTS.TAG_STATUS_IN_PRODUCTION)){
            status = repository.delete(id, false);
        }
        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.TAG_DELETE_ERROR);
        } else {
            return status;
        }
    }

    @Override
    public List<TagVM> getAll() {
        List<Tag> modelList = repository.findAll(sortByIdDesc());
        List<TagVM> VMList = new ArrayList<>();
        for (Tag model : modelList) {
            VMList.add(new TagVM(model));
        }
        return VMList;
    }

    @Override
    public TagVM getById(Long id) {
        try {
            Tag retrievedModel = repository.findOne(id);
            return new TagVM(retrievedModel);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.TAG_DOES_NOT_EXISTS);
        }
    }

    @Override
    public List<TagVM> getAllByInwardId(Long inwardId) {
        List<Tag> modelList = repository.findByInwardId(inwardId);
        List<TagVM> VMList = new ArrayList<>();
        for (Tag model : modelList) {
            VMList.add(new TagVM(model));
        }
        return VMList;
    }
}
