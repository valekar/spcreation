package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.exception.UserNotFoundException;
import com.srini.model.PasswordResetToken;
import com.srini.model.User;
import com.srini.repository.ResetPasswordRepository;
import com.srini.repository.UserRepository;
import com.srini.util.Mail;
import freemarker.template.Configuration;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

@Service
public class ResetPasswordService {
    @Autowired
    private ResetPasswordRepository resetPasswordRepository;

    @Autowired
    JavaMailSender sender;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Configuration freemarkerConfig;


    @Value("${client.url}")
    private String client_url;

    @Value("${owner.email}")
    private String owner_email;

    @Transactional
    public PasswordResetToken save(String email) throws UserNotFoundException,Exception{

            User u = userRepository.findOneByEmail(email);
            if(u == null){
                throw new UserNotFoundException();
            }
            String token = generateToken();
            //send email with token in url
            sendEmail(token,email);
            PasswordResetToken p = new PasswordResetToken();
            p.setUser_id(u.getId());
            p.setToken(passwordEncoder.encode(token));
            p.setExpiryDate(getTime());
            return resetPasswordRepository.save(p);
    }

    public List<PasswordResetToken> findPasswordResetTokens(){
        return resetPasswordRepository.findTokensByExpiryDate();
    }

    //@Transactional
    public boolean validateToken(String token){
        PasswordResetToken matchedResetToken = tokenMatch(token);
        if(matchedResetToken!=null){
            if(matchedResetToken.getExpiryDate().before(getTime())){
                return true;
            }
        }
        return false;
    }
    @Transactional
    public PasswordResetToken tokenMatch(String token){
        PasswordResetToken matchedResetToken =null;
        List<PasswordResetToken> list = findPasswordResetTokens();
        for(PasswordResetToken a : list ){
            if(passwordEncoder.matches(token,a.getToken())){
                matchedResetToken = a;
                break;
            }
        }
        return matchedResetToken;
    }

    private String generateToken(){
        try {
            Date d = new Date();
            d.getTime();
            byte[] bytes = new byte[20];
            SecureRandom.getInstanceStrong().nextBytes(bytes);
            return Base64.encodeBase64String(bytes);
        }
       catch (Exception ex){
            byte[] b = new byte[20];
            new Random().nextBytes(b);
            return Base64.encodeBase64String(b);
       }
    }

    private Timestamp getTime(){
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTime(ts);
        cal.add(Calendar.DAY_OF_WEEK, 1);
        ts.setTime(cal.getTime().getTime()); // or
        ts = new Timestamp(cal.getTime().getTime());
        return  ts;
    }



    private void sendEmail(String token,String email) throws Exception{
        //String recipient = email;//uncomment when mail gun is purchased
        String recipient = this.owner_email;
        String content = this.client_url + CONSTANTS.CHANGE_PASSWORD_URL+token;
        String subject = CONSTANTS.EMAIL_RESET_PASSWORD;
        String template_type = CONSTANTS.RESET_PASSWORD_TEMPLATE;
        //String CC = this.owner_email; //uncomment when mail gun is purchased
        String CC = "";
        Map<String,String> details = new HashMap<>();
        details.put("name","");
        details.put("action_url",this.client_url + CONSTANTS.CHANGE_PASSWORD_URL+token);
        Mail.sendEmail(freemarkerConfig,this.getClass(),template_type,recipient,content,subject,sender,details,CC);
    }
}
