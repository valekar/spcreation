package com.srini.service;

import org.springframework.data.domain.Sort;

public abstract class BaseServiceImpl {

    public Sort sortByIdDesc() {
        return new Sort(Sort.Direction.DESC, "id");
    }

}
