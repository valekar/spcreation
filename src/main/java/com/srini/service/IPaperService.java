package com.srini.service;



import com.srini.model.Paper;
import com.srini.viewmodel.PaperVM;

import java.util.List;


public interface IPaperService {
    PaperVM add(Paper fromBodyModel);
    PaperVM update(Paper fromBodyModel, Long id);
    int delete(Long id);
    List<PaperVM> getAll();
    PaperVM getById(Long id);
}
