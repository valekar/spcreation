package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.Image;
import com.srini.repository.ImageRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Service
public class ImageService {

    private final ImageRepository imageRepository;
    private final ResourceLoader resourceLoader;

    private static String UPLOAD_ROOT = "upload.file.path";
    private static String MY_ENVIRONMENT = "my.environment";
    private static String DEV = "development";
    private  static String PROD = "production";

    @Autowired
    public ImageService(ImageRepository imageRepository, ResourceLoader resourceLoader){
        this.imageRepository = imageRepository;
        this.resourceLoader = resourceLoader;
    }

    @Autowired
    Environment env;

    public Resource findOneImage(String filename) throws IOException{
        Image image = imageRepository.findByName(filename);
        if(image!=null){
            try{
                Resource resource =  new UrlResource(Paths.get(env.getProperty(UPLOAD_ROOT)).resolve(filename).toUri());
                return resource;
            }
            catch (IOException ex){
                return null;
            }
        }
        else{
            throw new IOException(CONSTANTS.IMAGE_DOES_NOT_EXISTS);
        }
    }

    public Image createImage(MultipartFile file) throws IOException{
        try {
            String fileNameWithOutExt = FilenameUtils.removeExtension(file.getOriginalFilename());
            String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
            String fileNameWithTimestamp = fileNameWithOutExt + "_" + new Date().getTime() + "." + fileExtension;
            Image image = imageRepository.findByName(fileNameWithTimestamp);
            if (image != null) {
                throw new IOException(CONSTANTS.IMAGE_ALREADY_EXISTS);
            }

            if (!file.isEmpty()) {
                //String fileName = file.getOriginalFilename();
                String directory = env.getProperty(UPLOAD_ROOT);
                //Files.copy(file.getInputStream(),Paths.get("." + File.separator + directory,fileName));
                Files.copy(file.getInputStream(), Paths.get(directory, fileNameWithTimestamp));
                Image newImage = new Image(fileNameWithTimestamp);
                newImage.setAddAuditFields();
                Image image1 = imageRepository.save(newImage);
                return image1;
            } else {
                throw new IOException(CONSTANTS.IMAGE_ALREADY_EXISTS);
            }
        }
        finally{
            file.getInputStream().close();
        }
    }

    public void deleteImage(String filename) throws IOException{
        final Image byName = imageRepository.findByName(filename);
        imageRepository.delete(byName);
        String directory = env.getProperty(UPLOAD_ROOT);
        // i am  getting an error saying "the process cannot access the file because it is being used by another process."
        // so this has been commented, but it is advised to uncomment it layer
        if(env.getProperty(MY_ENVIRONMENT).equals(PROD)) {
            Files.deleteIfExists(Paths.get(directory, filename));
        }
    }


    public  int  inActivateImage(String filename) throws IOException{
        final Image byName = imageRepository.findByName(filename);
        //Files.deleteIfExists(Paths.get(UPLOAD_ROOT,filename));
        return imageRepository.inActivate(byName.getId(),false);
    }



}
