package com.srini.service;

import com.srini.constant.CONSTANTS;
import com.srini.model.*;
import com.srini.model.Thread;
import com.srini.repository.*;
import com.srini.viewmodel.OrderVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Service
public class OrderServiceImpl extends BaseServiceImpl implements IOrderService {

    @Autowired
    OrderRepository repository;

    @Autowired
    ThreadRepository threadRepository;

    @Autowired
    AppliqueRepository appliqueRepository;

    @Autowired
    PaperRepository paperRepository;

    @Autowired
    ISAMSPCCounterService isamspcCounterService;

    @Autowired
    ImageService imageService;

    @Override
    public OrderVM add(Order fromBodyModel, MultipartFile file) throws IOException, MultipartException {
        fromBodyModel.setAddAuditFields();
        //Set<Combo> combos = new Set<Combo>();
        for (Combo combo : fromBodyModel.getCombos()) {
            //ThreadVM thread = combo.getThread();
            Thread thread = threadRepository.findOne(combo.getThread().getId());
            Applique applique = appliqueRepository.findOne(combo.getApplique().getId());
            Paper paper = paperRepository.findOne(combo.getPaper().getId());
            combo.setApplique(applique);
            combo.setPaper(paper);
            combo.setThread(thread);
            combo.setAddAuditFields();
        }

        //set SAM and SPC number from SAM_SPC_counter table
        SAM_SPC_counter sam_spc_counter = isamspcCounterService.save();
        Order updatedFromBodyModel = setSAMSPC(fromBodyModel, sam_spc_counter);

        //save image
        Image image = imageService.createImage(file);
        updatedFromBodyModel.setImage(image);

        updatedFromBodyModel.setAddAuditFields();
        Order savedModel = repository.save(updatedFromBodyModel);
        OrderVM VM = new OrderVM(savedModel);
        //set the image resource
        VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
        return VM;
    }

    @Transactional
    @Override
    public OrderVM update(Order fromBodyModel, MultipartFile file, Long id) throws IOException, MultipartException {
        try {
            Order retrievedModel = repository.findOne(id);
            retrievedModel.setEditAuditFields();
            //retrievedModel.setStatus(fromBodyModel.getStatus());
            retrievedModel.setGrade(fromBodyModel.getGrade());
            //retrievedModel.setCombos(fromBodyModel.getCombos());
            retrievedModel.setDesignName(fromBodyModel.getDesignName());
            retrievedModel.setDuration(fromBodyModel.getDuration());
            retrievedModel.setEmbroideryType(fromBodyModel.getEmbroideryType());
            retrievedModel.setFabric_size(fromBodyModel.getFabric_size());
            retrievedModel.setFabricColor(fromBodyModel.getFabricColor());
            retrievedModel.setNoOfCombos(fromBodyModel.getNoOfCombos());
            retrievedModel.setQuantity(fromBodyModel.getQuantity());
            retrievedModel.setStitches(fromBodyModel.getStitches());
            //added missed fields
            retrievedModel.setPart(fromBodyModel.getPart());
            retrievedModel.setPosition(fromBodyModel.getPosition());
            retrievedModel.setMeasurement(fromBodyModel.getMeasurement());
            //set customer
            retrievedModel.setCustomer(fromBodyModel.getCustomer());
            if (fromBodyModel.getCombos().size() >= retrievedModel.getCombos().size()) {
                for (Combo comboFromBody : fromBodyModel.getCombos()) {
                    Long comboFromBodyId = comboFromBody.getId();
                    //update the existing combo
                    if (comboFromBodyId != null)
                        for (Combo retrievedCombo : retrievedModel.getCombos()) {
                            if (retrievedCombo.getId() == comboFromBodyId) {
                                retrievedCombo.setThread(comboFromBody.getThread());
                                retrievedCombo.setPaper(comboFromBody.getPaper());
                                retrievedCombo.setApplique(comboFromBody.getApplique());
                                retrievedCombo.setBoxes(comboFromBody.getBoxes());
                                retrievedCombo.setCones(comboFromBody.getCones());
                                retrievedCombo.setFrameSize(comboFromBody.getFrameSize());
                                retrievedCombo.setLayers(comboFromBody.getLayers());
                                retrievedCombo.setEditAuditFields();
                            }
                        }
                        //if new combo is added then add that to the database
                    else {
                        Set<Combo> combos = retrievedModel.getCombos();
                        comboFromBody.setAddAuditFields();
                        combos.add(comboFromBody);
                        retrievedModel.setCombos(combos);
                    }
                }
            } else if (fromBodyModel.getCombos().size() < retrievedModel.getCombos().size()) {
                //Set<Combo> retrievedModelCombos = retrievedModel.getCombos();
                Set<Combo> fromBodyCombos = fromBodyModel.getCombos();
                retrievedModel.getCombos().retainAll(fromBodyCombos);
                for (Combo comboFromBody : fromBodyModel.getCombos()) {
                    Long comboFromBodyId = comboFromBody.getId();
                    //update the existing combo
                    for (Combo retrievedCombo : retrievedModel.getCombos()) {
                        if (retrievedCombo.getId() == comboFromBodyId) {
                            retrievedCombo.setThread(comboFromBody.getThread());
                            retrievedCombo.setPaper(comboFromBody.getPaper());
                            retrievedCombo.setApplique(comboFromBody.getApplique());
                            retrievedCombo.setBoxes(comboFromBody.getBoxes());
                            retrievedCombo.setCones(comboFromBody.getCones());
                            retrievedCombo.setFrameSize(comboFromBody.getFrameSize());
                            retrievedCombo.setLayers(comboFromBody.getLayers());
                            retrievedCombo.setEditAuditFields();
                        }
                    }
                }
            }

            String imageName = retrievedModel.getImage().getName();
            OrderVM VM = null;

            if (imageName.equals(file.getOriginalFilename())) {
                Order updatedModel = repository.save(retrievedModel);
                VM = new OrderVM(updatedModel);

            } else {
                //set image
                retrievedModel.setImage(null);
                Order updatedModel = repository.save(retrievedModel);
                imageService.deleteImage(imageName);
                Image image = imageService.createImage(file);
                updatedModel.setImage(image);
                Order updatedModel1 = repository.save(updatedModel);
                VM = new OrderVM(updatedModel1);
            }
            //set the image resource
            VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
            return VM;
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int delete(Long id) throws IOException,DataIntegrityViolationException{
        Order retrievedModel = repository.findOne(id);
        if (retrievedModel == null) {
            throw new DataIntegrityViolationException(CONSTANTS.ORDER_DELETE_ERROR);
        }
        //first inactivate the image but dont delete it
        imageService.inActivateImage(retrievedModel.getImage().getName());
        int status = repository.delete(id, false);
        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.ORDER_DELETE_ERROR);
        } else {

            return status;
        }
    }

    @Override
    public List<OrderVM> getAll(int page, int size) throws IOException,NullPointerException {
        long count = size;
        if(size==0) {
            count = repository.count();
        }
        if(count == 0){
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }

        Page<Order> modelList = repository.findAll(new PageRequest(0, (int) count, sortByIdDesc()));
        List<OrderVM> VMList = new ArrayList<>();
        for (Order model : modelList) {
            OrderVM newOrderVM = new OrderVM(model);
            Resource resource = imageService.findOneImage(model.getImage().getName());
            newOrderVM.setStatus(newOrderVM.getStatus().toUpperCase());
            newOrderVM.setImageResource(imageService.findOneImage(model.getImage().getName()));
            VMList.add(newOrderVM);
        }
        return VMList;
    }

    @Override
    public OrderVM getById(Long id) throws NullPointerException, IOException {
        try {
            Order retrievedModel = repository.findOne(id);
            OrderVM VM = new OrderVM(retrievedModel);
            VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
            return VM;
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
    }

    @Override
    public long getOrderCount() {
        try {
            return repository.count();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public OrderVM approveOrder(Long id) throws IOException, NullPointerException{
        Order order = repository.findOne(id);
        if(order == null){
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
        order.setStatus(CONSTANTS.ORDER_APPROVE);
        order.setSpcNumber(order.getSamNumber());
        order.setSPC("#SPC"+order.getSpcNumber());
        order.setEditAuditFields();
        Order updatedOrder = repository.save(order);
        OrderVM VM = new OrderVM(updatedOrder);
        VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
        return VM;
    }

    @Override
    public OrderVM rejectOrder(Long id) throws IOException, NullPointerException {
        Order order = repository.findOne(id);
        if(order == null){
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
        order.setStatus(CONSTANTS.ORDER_REJECT);
        order.setSpcNumber(0);
        order.setSPC("");
        order.setEditAuditFields();
        Order updatedOrder = repository.save(order);
        OrderVM VM = new OrderVM(updatedOrder);
        VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
        return VM;
    }

    @Override
    public OrderVM pendingOrder(Long id) throws IOException,NullPointerException{
        Order order = repository.findOne(id);
        if(order == null){
            throw new NullPointerException(CONSTANTS.ORDER_DOES_NOT_EXISTS);
        }
        order.setStatus(CONSTANTS.ORDER_PENDING);
        order.setSpcNumber(0);
        order.setSPC("");
        order.setEditAuditFields();
        Order updatedOrder = repository.save(order);
        OrderVM VM = new OrderVM(updatedOrder);
        VM.setImageResource(imageService.findOneImage(VM.getImage().getName()));
        return VM;
    }


    private Order setSAMSPC(Order fromBody, SAM_SPC_counter counter) {
        fromBody.setSamNumber(Integer.valueOf(counter.getId() + ""));
        fromBody.setSpcNumber(0);
        fromBody.setSAM("#SAM" + counter.getId());
        fromBody.setSPC("");
        return fromBody;
    }

}
