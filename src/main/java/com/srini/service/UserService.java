package com.srini.service;


import com.srini.config.MyUserPrincipal;
import com.srini.constant.CONSTANTS;
import com.srini.model.PasswordResetToken;
import com.srini.model.Privilege;
import com.srini.model.TableMetadata;
import com.srini.model.User;
import com.srini.util.Mail;
import com.srini.viewmodel.ModifiedUserVM;
import freemarker.template.Configuration;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.stereotype.Service;

import com.srini.repository.UserRepository;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedArrayType;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("userDetailsService")
public class UserService implements UserDetailsService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
    private PasswordEncoder passwordEncoder;

	@Autowired
    private ResetPasswordService resetPasswordService;

    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    JavaMailSender sender;

	/*@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findOneByUsername(username);
	}*/

	@Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findOneByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        //getPrivilegeColumns();
        return new MyUserPrincipal(user);
    }

	@Transactional
	public User save(User user) {
        try {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(user);
        }
        catch (DataIntegrityViolationException ex){
            throw new DataIntegrityViolationException("User" , ex);
        }
	}


	@Transactional
    public int updatePassword(String password, String token) throws InvalidTokenException{
        PasswordResetToken passwordResetToken = resetPasswordService.tokenMatch(token);
        if(passwordResetToken!=null){
            int u = userRepository.updatePassword(passwordEncoder.encode(password),passwordResetToken.getUser_id());
            //entityUpdate(passwordEncoder.encode(password),passwordResetToken.getUser_id());
            return 0;
        }
        else{
            throw new InvalidTokenException("Invalid Token");
        }
    }

    private void entityUpdate(String password, Long id){
        User user= (User)entityManager.find(User.class , 1);
        entityManager
                .createQuery("update User set password = :password where id= :id")
                .setParameter("password", password)
                .setParameter("id", id)
                .executeUpdate();
    }




}

