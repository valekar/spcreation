package com.srini.service;

import com.srini.model.Checking;
import com.srini.model.Inward;
import com.srini.viewmodel.CheckingVM;
import com.srini.viewmodel.TagVM;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

public interface ICheckingService  {
    
    CheckingVM add(CheckingVM fromBodyModel) throws ParseException;
    CheckingVM update(Checking fromBodyModel, Long id);
    int delete(Long id);
    List<CheckingVM> getAll();
    CheckingVM getById(Long id);

    Set<Inward> getInwardsWithProducedTags();

    Set<TagVM> getTagsWithProducedStatus();
}
