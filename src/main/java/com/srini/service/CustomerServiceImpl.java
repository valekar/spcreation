package com.srini.service;

import com.srini.component.LoggedInUser;
import com.srini.config.LoggedInUserSingleton;
import com.srini.constant.CONSTANTS;
import com.srini.model.Customer;
import com.srini.repository.CustomerRepository;
import com.srini.util.Utility;
import com.srini.viewmodel.CustomerVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl extends BaseServiceImpl implements ICustomerService {

    @Autowired
    CustomerRepository customerRepository;


    @Override
    public CustomerVM addCustomer(Customer customer) {
        customer.setCreated_at(Utility.getTime());
        customer.setUpdated_at(Utility.getTime());
        //LoggedInUser loggedInUser = new LoggedInUser();
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
        customer.setCreated_by(loggedInUser.getUserName());
        customer.setCreated_user_id(loggedInUser.getUserId());
        customer.setUpdated_by(loggedInUser.getUserName());
        customer.setUpdated_user_id(loggedInUser.getUserId());

        Customer savedCustomer = customerRepository.save(customer);
        CustomerVM customerVM = new CustomerVM(savedCustomer);
        return customerVM;

    }

    @Override
    public CustomerVM updateCustomer(Customer customer, Long id) throws NullPointerException {
        try {
            //LoggedInUser loggedInUser = new LoggedInUser();
            LoggedInUserSingleton loggedInUserSingleton = LoggedInUserSingleton.getInstance();
            Customer cust = customerRepository.findById(id);
            cust.setUpdated_user_id(loggedInUserSingleton.getUserId());
            cust.setUpdated_by(loggedInUserSingleton.getUserName());
            cust.setUpdated_at(Utility.getTime());
            cust.setAddress(customer.getAddress());
            cust.setName(customer.getName());
            Customer updatedCustomer = customerRepository.save(cust);
            return new CustomerVM(updatedCustomer);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.CUSTOMER_DOES_NOT_EXISTS);
        }
    }

    @Transactional
    @Override
    public int deleteCustomer(Long id) throws DataIntegrityViolationException {
        Customer customer = customerRepository.findOne(id);
        if (customer == null) {
            throw new DataIntegrityViolationException(CONSTANTS.CUSTOMER_DELETE_ERROR);
        }
        int status = customerRepository.delete(id, false);
        //int status = customerRepository.deleteById(id);
        if (status == 0) {
            throw new DataIntegrityViolationException(CONSTANTS.CUSTOMER_DELETE_ERROR);
        } else {
            return status;
        }
        //return 0;
    }

    @Override
    public List<CustomerVM> getAllCustomers() {
        List<Customer> customerList = customerRepository.findAll(sortByIdDesc());
        List<CustomerVM> customerVMList = new ArrayList<>();
        for (Customer customer : customerList) {
            customerVMList.add(new CustomerVM(customer));
        }
        return customerVMList;
    }

    @Override
    public CustomerVM getCustomerById(Long id) throws NullPointerException {
        try {
            Customer customer = customerRepository.findById(id);
            return new CustomerVM(customer);
        } catch (NullPointerException ex) {
            throw new NullPointerException(CONSTANTS.CUSTOMER_DOES_NOT_EXISTS);
        }

    }
}
