package com.srini.util;

import com.srini.constant.CONSTANTS;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;


public class Mail {



    public static void sendEmail(Configuration freemarkerConfig,Class resourceLoaderClass ,String template_type, String recipient, String content,
                                 String subject,JavaMailSender sender, Map<String,String> details, String CC) throws Exception{

        try{
            MimeMessage message = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,true);
            helper.setTo(recipient);
            if(CC.length()>0){
                helper.setCc(CC);
            }
            helper.setBcc(CONSTANTS.ADMIN_EMAIL);
            Map<String,String> model = details;
            freemarkerConfig.setClassForTemplateLoading(resourceLoaderClass, CONSTANTS.TEMPLATE_RESOURCE_PATH);
            Template t = freemarkerConfig.getTemplate(template_type);
            String text = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
            helper.setSubject(subject);
            helper.setText(text,true);
            sender.send(message);
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
