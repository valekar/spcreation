package com.srini.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utility {

    public static List<String> getClassColumns(Class aClass){
        //Class aClass = myClass;
        Field[] fields = aClass.getDeclaredFields();
        List<String> strList = new ArrayList<>();
        for(Field f : fields){
            if(Modifier.isPrivate(f.getModifiers())){
                //System.out.println(f.getName().toString());
                strList.add(f.getName());
            }
        }
        return strList;

    }

    public static String getSimpleDateFormatter(Timestamp timestamp){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
        return simpleDateFormat.format(timestamp);
    }

    public static Timestamp getTimestampFromString(String timestampStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "MM-dd-yyyy HH:mm:ss");
        Date date = simpleDateFormat.parse(timestampStr);
        Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;
    }

    public static Timestamp getTimestampFromTimeZoneString(String timestampStr) throws ParseException{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date date = simpleDateFormat.parse(timestampStr);
        Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp;

    }

    public static Timestamp getTime(){
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        //Calendar cal = Calendar.getInstance();
        //cal.setTime(ts);
        //cal.add(Calendar.DAY_OF_WEEK, 1);
        //ts.setTime(cal.getTime().getTime()); // or
        //ts = new Timestamp(cal.getTime().getTime());
        return  ts;
    }
}
