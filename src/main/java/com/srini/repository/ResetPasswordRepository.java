package com.srini.repository;

import com.srini.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

public interface ResetPasswordRepository extends JpaRepository<PasswordResetToken, Long>{

    PasswordResetToken save(PasswordResetToken token);
    PasswordResetToken findByToken(String token);

    @Query("FROM PasswordResetToken t WHERE t.expiryDate >= current_timestamp")
    List<PasswordResetToken> findTokensByExpiryDate();
}
