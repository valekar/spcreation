package com.srini.repository;

import com.srini.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


public interface ImageRepository extends JpaRepository<Image,Long> {
     Image findByName(String name);

     @Modifying
     @Query("UPDATE Image SET active = :is_active WHERE  id=:id")
     int inActivate(@Param("id") Long id, @Param("is_active") boolean is_active);
}
