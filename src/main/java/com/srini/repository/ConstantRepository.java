package com.srini.repository;

import com.srini.model.Constant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConstantRepository extends JpaRepository<Constant,Long>{


    @Modifying
    @Query("UPDATE Constant SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);
}
