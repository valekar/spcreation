package com.srini.repository;

import com.srini.model.CheckEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckEmployeeRepository extends JpaRepository<CheckEmployee,Long> {
}
