package com.srini.repository;

import com.srini.model.Customer;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    //get
    @Override
    List<Customer> findAll(Sort sort);

    Customer findById(Long id);
    //save
    @Override
    Customer save(Customer customer);

    //@Override
    int deleteById(Long id);

    @Modifying
    @Query("UPDATE Customer SET active = :is_active WHERE  id=:customer_id")
    int delete(@Param("customer_id") Long id, @Param("is_active") boolean is_active);

}
