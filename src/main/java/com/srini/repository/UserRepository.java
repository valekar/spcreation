package com.srini.repository;


import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.srini.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Access to the user data. JpaRepository grants us convenient access methods here.
 */

public interface UserRepository extends JpaRepository<User, Long> {
    /**
     * Find a user by username
     *
     * @param username the user's username
     * @return user which contains the user with the given username or null.
     */
    User findOneByUsername(String username);

    User save(User user);

    User findOneByEmail(String email);

    @Modifying
    @Query("UPDATE User SET password = :password WHERE id = :user_id")
    int updatePassword(@Param("password") String password, @Param("user_id") Long userId);


    //@Override
    //List<User> findAllOrderByIdDesc();

    @Override
    List<User> findAll(Sort sort);

    //@Modifying
    //@Query("Update User SET active = :is_active WHERE id = :user_id")
    //int delete(@Param("user_id") Long id, @Param("is_active") boolean is_active);

    int deleteById(Long id);

}
