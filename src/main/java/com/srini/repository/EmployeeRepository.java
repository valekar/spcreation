package com.srini.repository;

import com.srini.model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

    @Override
    Employee findOne(Long aLong);

    Employee findByName(String name);

    @Override
    Employee save(Employee employee);

    //@Override
    int deleteById(Long id);

    @Modifying
    @Query("UPDATE Employee SET active = :is_active WHERE emp_id =:emp_id")
    int delete(@Param("emp_id") Long id,@Param("is_active") boolean is_active);

    @Override
    List<Employee> findAll(Sort sort);

}


