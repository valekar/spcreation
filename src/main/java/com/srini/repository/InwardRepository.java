package com.srini.repository;

import com.srini.model.Inward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface InwardRepository extends JpaRepository<Inward,Long> {

    @Modifying
    @Query("UPDATE Inward SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);

    Inward findInwardByOrderId(Long id);
}
