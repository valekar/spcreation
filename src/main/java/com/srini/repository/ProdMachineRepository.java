package com.srini.repository;

import com.srini.model.ProdMachine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdMachineRepository extends JpaRepository<ProdMachine,Long> {

}
