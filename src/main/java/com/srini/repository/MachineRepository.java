package com.srini.repository;

import com.srini.model.Machine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MachineRepository extends JpaRepository<Machine, Long>{

    @Modifying
    @Query("UPDATE Machine SET active = :is_active WHERE  id=:machine_id")
    int delete(@Param("machine_id") Long id, @Param("is_active") boolean is_active);
}
