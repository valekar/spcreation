package com.srini.repository;

import com.srini.model.CheckTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckTagRepository extends JpaRepository<CheckTag,Long> {
}
