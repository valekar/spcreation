package com.srini.repository;

import com.srini.model.Inward;
import com.srini.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface TagRepository extends JpaRepository<Tag,Long> {


    @Modifying
    @Query("UPDATE Tag SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);

    List<Tag> findByInwardId(Long inwardId);

    Set<Tag> getInwardByStatus(String status);
}
