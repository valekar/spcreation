package com.srini.repository;

import com.srini.model.Checking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CheckingRepository extends JpaRepository<Checking,Long> {


    @Modifying
    @Query("UPDATE Checking SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);
}
