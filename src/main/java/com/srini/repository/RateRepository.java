package com.srini.repository;

import com.srini.model.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RateRepository extends JpaRepository<Rate,Long>{


    @Modifying
    @Query("UPDATE Rate SET active = :is_active WHERE  id=:rate_id")
    int delete(@Param("rate_id") Long id, @Param("is_active") boolean is_active);
}
