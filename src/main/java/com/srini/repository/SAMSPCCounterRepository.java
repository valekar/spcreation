package com.srini.repository;

import com.srini.model.SAM_SPC_counter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SAMSPCCounterRepository extends JpaRepository<SAM_SPC_counter,Long>{



    @Query("Delete from SAM_SPC_counter")
    @Modifying
    int resetTable();

    //@Modifying
    //@Query("Alter SEQUENCE SAM_SPC_counter_id_seq RESTART WITH 1")
    //int resetSequence();
}
