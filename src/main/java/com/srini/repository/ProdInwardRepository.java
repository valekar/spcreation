package com.srini.repository;

import com.srini.model.ProdInward;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdInwardRepository extends JpaRepository<ProdInward,Long> {
}
