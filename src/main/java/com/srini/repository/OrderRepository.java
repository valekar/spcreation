package com.srini.repository;

import com.srini.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.OrderBy;
import java.util.List;
import java.util.Set;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order,Long> {

    @Modifying
    @Query("UPDATE Order SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);

    Page<Order> findAllByStatus(Pageable pageable,String status);

    @OrderBy("Updated_at DESC")
    Set<Order> findOrdersByCustomerId(Long id);
}
