package com.srini.repository;

import com.srini.model.Applique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AppliqueRepository extends JpaRepository<Applique,Long>{

    @Modifying
    @Query("UPDATE Applique SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);

}
