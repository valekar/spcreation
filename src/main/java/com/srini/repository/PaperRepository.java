package com.srini.repository;

import com.srini.model.Paper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PaperRepository extends JpaRepository<Paper,Long>{

    @Modifying
    @Query("UPDATE Paper SET active = :is_active WHERE  id=:id")
    int delete(@Param("id") Long id, @Param("is_active") boolean is_active);

}
