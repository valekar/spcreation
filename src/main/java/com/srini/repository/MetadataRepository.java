package com.srini.repository;

import com.srini.model.TableMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MetadataRepository  extends JpaRepository<TableMetadata,Long>{

    //@Query("From TableMetadata WHERE tableSchema = :schema and tableName = :tableName")
    @Query("SELECT columnName From TableMetadata WHERE tableName = :tableName")
    List<String> getColumnNames(@Param("tableName") String tableName);

    List<TableMetadata> findByTableName(String tableName);
   // List<String>
}
