package com.srini.constant;

public class CONSTANTS {


    //config values
    public static String WELCOME_USER_TEMPLATE = "Welcome.ftl";
    public static String LOGIN_URL = "/login";
    public static String HELP_URL = "/help";
    public static String CHANGE_PASSWORD_URL = "/change-password?token=";
    public static String RESET_PASSWORD_TEMPLATE = "Reset_Password.ftl";
    public static String TEMPLATE_RESOURCE_PATH = "/templates";
    public static String ADMIN_EMAIL = "srinivas.valekar@gmail.com";
    public static String IMAGE_UPLOAD_FOLDER_PATH = "images-dir";
    //config values

    public static String ADD = "add";
    public static String EDIT = "edit";
    public static String DELETE = "delete";
    public static String ADDED_MESSAGE = "Added ";
    public static String EDIT_MESSAGE = "Edited ";
    public static String DELETE_MESSAGE = "Deleted ";
    public static String SUCCESSFULLY_MESSAGE = " successfully";
    public static String USER = "user";
    public static String HEADER ="header";
    public static String MESSAGE = "message";
    public static String SUCCESS_HEADER = "Successfully";
    public static String SUCCESS_HEADER_ADD = "added";
    public static String SUCCESS_HEADER_EDIT = "edited";
    public static String SUCCESS_HEADER_DELETE = "deleted";
    public static String SENT_EMAIL = "sent email";
    public static String SENT = "Sent";
    public static String EMAIL_DESCRIPTION = "Email has been sent to your email id, please use the link in the email to reset your password";
    public static String USER_NOT_FOUND = "User not found, Please enter correct email id or create an account if you are new";
    public static String FAILED = "Failed";
    public static String ERROR = "Error!";
    public static String INVALID_TOKEN_DESC = "Token has expired! Please reset your password again";
    public static String VALID_TOKEN_DESC = "Valid token! Token validated successfully";
    public static String VALID_TOKEN = "Valid Token";
    public static String PASSWORD_CHANGED = "Password Changed!";
    public static String PASSWORD_CHANGED_DESC = "Password Changed Successfully, Please login again";

    //data integrity errors
    public static  String USER_ALREADY_EXISTS = "User already exists, Please give another username";
    public static String USER_ERROR_HEADER = "User Creation Error";
    public static String USER_EMAIL_ALREADY_EXISTS = "Email already exists, please use different email id";
    //contact admin
    public static String DATABASE_ERROR_HEADER = "Database Error!";
    public static String SOMETHING_WENT_WRONG = "Something has gone wrong, please contact the admin";


    //ADMIN Adding
    public static String ADMIN_ADDED = "ADMIN_ADD";
    public static String ADMIN_AUTH_CHANGED = "Access Changes!";
    public static String ADMIN_AUTH_CHANGED_DESC = "Access changes done to the user";
    public static String RESOURCE_NOT_FOUND = "No results found";


    //Create User
    public static String CREATE_USER = "create_user";
    public static String NEW_USER_CREATED = "New User Created!";
    public static String NEW_USER_CREATE_DESC = "Successfully Created new user!";
    public static String CREATE_USER_DEFAULT_PASSWORD = "Create User With Default Password";
    //public static String NEW_USER_CREATED = "New User Created!";
    public static String NEW_USER_CREATE_DEFAULT_PASSWORD_DESC =
            "Successfully Created New User with Default Password! An email is sent to the admin and user";
    //Delete USer
    public static String DELETE_USER = "Delete User";
    public static String DELETED_USER = "Delete User!";
    public static String DELETED_USER_DESC = "Successfully deleted User!";
    //user
    public static String USER_DOES_NOT_EXIST = "User does not exist";
    public static String CANNOT_DELETE_USER = "Cannot delete <strong> currently loggedin </strong> user";
    //update user
    public static String USER_UPDATE = "User Update";
    public static String USER_UPDATE_DESC = "User successfully updated";
    //mail
    public static String EMAIL_RESET_PASSWORD = "Reset Password SP Creation";
    public static String WELCOME_USER = "Welcome to SP Creation";
    public static String username_key = "username";
    public static String sender_name_key = "sender_name";
    public static String password_key = "password";
    public static String login_url_key = "login_url";
    public static String action_url_key = "action_url";
    public static String help_url = "help_url";
    public static String CHARACTERS = "ABCDEFGHKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz023456789";
    public static String EMAIL_NOT_SENT = "Email Not Sent";

    //file
    public  static String FILE_UPLOAD_EXCEPTION = "Unable to upload the file, The file might be already existing.Change the name and upload";
    public static  String FILE_UPLOAD_EXCEED_LIMIT = "Image upload exceeds the maximum limit, please reduce the image size";
    public static String PLEASE_UPLOAD_IMAGE = "Please upload an image";
    public static String REQUIRED_FILE_NOT_FOUND= "Required request part 'file' is not present";
    //Employee
    public static String EMPLOYEE_GET = "Retrieved Employee(s)!";
    public static String EMPLOYEE_GET_DESC = "Successfully retrieved Employee(s)";
    public static String EMPLOYEE_UPDATE_ERROR = "Unable to update employee";
    public static String EMPLOYEE_DELETED = "Employee Delete";
    public static String EMPLOYEE_DELETED_DESC = "Employee deleted successfully!";
    public static String EMPLOYEE_DELETED_HEADER = "Employee Delete";
    public static String EMPLOYEE_UPDATED = "Employee Updated!";
    public static String EMPLOYEE_UPDATED_DESC = "Employee Updated Successfully!";
    public static String EMPLOYEE_CREATED = "Employee Created!";
    public static String EMPLOYEE_CREATED_DESC = "Employee Created Successfully";
    public static String EMPLOYEE_DOES_NOT_EXIST = "Employee does not exist";
    public static String EMPLOYEE_DELETE_ERROR = "Unable to delete employee";

    //customer
    public static String CUSTOMER_DOES_NOT_EXISTS = "Customer does not exist";
    public static String CUSTOMER_DELETE_ERROR  = "Customer delete error!";
    public static String CUSTOMER_GET = "Retrieved Customer(s)!";
    public static String CUSTOMER_GET_DESC = "Successfully retrieved Customer(s)";
    public static String CUSTOMER_UPDATED = "Customer Updated!";
    public static String CUSTOMER_UPDATED_DESC = "Customer updated successfully!";
    public static String CUSTOMER_CREATED = "Customer Created!";
    public static String CUSTOMER_CREATED_DESC = "Customer Created Successfully!";
    public static String CUSTOMER_DELETED = "Customer Deleted!";
    public static String CUSTOMER_DELETED_DESC = "Customer Deleted Successfully";

    //machine
    public static String MACHINE_DOES_NOT_EXISTS = "Machine does not exist";
    public static String MACHINE_DELETE_ERROR  = "Machine delete error!";
    public static String MACHINE_GET = "Retrieved Machine(s)!";
    public static String MACHINE_GET_DESC = "Successfully retrieved Machine(s)";
    public static String MACHINE_UPDATED = "Machine Updated!";
    public static String MACHINE_UPDATED_DESC = "Machine updated successfully!";
    public static String MACHINE_CREATED = "Machine Created!";
    public static String MACHINE_CREATED_DESC = "Machine Created Successfully!";
    public static String MACHINE_DELETED = "Machine Deleted!";
    public static String MACHINE_DELETED_DESC = "Machine Deleted Successfully";

    //rate
    public static String RATE_DOES_NOT_EXISTS = "Rate does not exist";
    public static String RATE_DELETE_ERROR  = "Rate delete error!";
    public static String RATE_GET = "Retrieved Rate(s)!";
    public static String RATE_GET_DESC = "Successfully retrieved Rate(s)";
    public static String RATE_UPDATED = "Rate Updated!";
    public static String RATE_UPDATED_DESC = "Rate updated successfully!";
    public static String RATE_CREATED = "Rate Created!";
    public static String RATE_CREATED_DESC = "Rate Created Successfully!";
    public static String RATE_DELETED = "Rate Deleted!";
    public static String RATE_DELETED_DESC = "Rate Deleted Successfully";

    //paper
    public static String PAPER_DOES_NOT_EXISTS = "Paper does not exist";
    public static String PAPER_DELETE_ERROR  = "Paper delete error!";
    public static String PAPER_GET = "Retrieved Paper(s)!";
    public static String PAPER_GET_DESC = "Successfully retrieved Paper(s)";
    public static String PAPER_UPDATED = "Paper Updated!";
    public static String PAPER_UPDATED_DESC = "Paper updated successfully!";
    public static String PAPER_CREATED = "Paper Created!";
    public static String PAPER_CREATED_DESC = "Paper Created Successfully!";
    public static String PAPER_DELETED = "Paper Deleted!";
    public static String PAPER_DELETED_DESC = "Paper Deleted Successfully";

    //thread
    public static String THREAD_DOES_NOT_EXISTS = "Thread does not exist";
    public static String THREAD_DELETE_ERROR  = "Thread delete error!";
    public static String THREAD_GET = "Retrieved Thread(s)!";
    public static String THREAD_GET_DESC = "Successfully retrieved Thread(s)";
    public static String THREAD_UPDATED = "Thread Updated!";
    public static String THREAD_UPDATED_DESC = "Thread updated successfully!";
    public static String THREAD_CREATED = "Thread Created!";
    public static String THREAD_CREATED_DESC = "Thread Created Successfully!";
    public static String THREAD_DELETED = "Thread Deleted!";
    public static String THREAD_DELETED_DESC = "Thread Deleted Successfully";

    //department
    public static String DEPARTMENT_DOES_NOT_EXISTS = "Department does not exist";
    public static String DEPARTMENT_DELETE_ERROR  = "Department delete error!";
    public static String DEPARTMENT_GET = "Retrieved Department(s)!";
    public static String DEPARTMENT_GET_DESC = "Successfully retrieved Department(s)";
    public static String DEPARTMENT_UPDATED = "Department Updated!";
    public static String DEPARTMENT_UPDATED_DESC = "Department updated successfully!";
    public static String DEPARTMENT_CREATED = "Department Created!";
    public static String DEPARTMENT_CREATED_DESC = "Department Created Successfully!";
    public static String DEPARTMENT_DELETED = "Department Deleted!";
    public static String DEPARTMENT_DELETED_DESC = "Department Deleted Successfully";

    //applique
    public static String APPLIQUE_DOES_NOT_EXISTS = "Applique does not exist";
    public static String APPLIQUE_DELETE_ERROR  = "Applique delete error!";
    public static String APPLIQUE_GET = "Retrieved Applique(s)!";
    public static String APPLIQUE_GET_DESC = "Successfully retrieved Applique(s)";
    public static String APPLIQUE_UPDATED = "Applique Updated!";
    public static String APPLIQUE_UPDATED_DESC = "Applique updated successfully!";
    public static String APPLIQUE_CREATED = "Applique Created!";
    public static String APPLIQUE_CREATED_DESC = "Applique Created Successfully!";
    public static String APPLIQUE_DELETED = "Applique Deleted!";
    public static String APPLIQUE_DELETED_DESC = "Applique Deleted Successfully";

    //order
    public static String ORDER_DOES_NOT_EXISTS = "Order does not exist";
    public static String ORDER_DELETE_ERROR  = "Order delete error!";
    public static String ORDER_GET = "Retrieved Order(s)!";
    public static String ORDER_GET_DESC = "Successfully retrieved Order(s)";
    public static String ORDER_UPDATED = "Order Updated!";
    public static String ORDER_UPDATED_DESC = "Order updated successfully!";
    public static String ORDER_CREATED = "Order Created!";
    public static String ORDER_CREATED_DESC = "Order Created Successfully!";
    public static String ORDER_DELETED = "Order Deleted!";
    public static String ORDER_DELETED_DESC = "Order Deleted Successfully";
    public static String ORDER_COUNT = "Order Count";
    public static String ORDER_COUNT_DESC = "Successfully returned order count";
    //approvals
    public static String ORDER_APPROVE="APPROVED";
    public static String ORDER_PENDING = "PENDING";
    public static String ORDER_REJECT="REJECTED";
    public static String ORDER_CONFIRMED="CONFIRMED";
    public static String TAG_STATUS_IN_PRODUCTION = "IN PRODUCTION";
    public static String TAG_STATUS_IN_PRODUCED = "PRODUCED";
    public static String TAG_STATUS_IN_CHECKING = "IN CHECKING";

    public static String ORDER_APPROVE_DESC = "The order has been approved!";
    public static String ORDER_REJECT_DESC = "The order has been rejected";
    public static String ORDER_PENDING_DESC = "The order has been reverted back to \"Pending\"";
    public static String ORDER_CONFIRMED_DESC = "The order has been confirmed and ready for production";


    //Image
    public static String IMAGE_ALREADY_EXISTS = "Image already exists!";
    //SAM_SPC_counter
    public static String SAM_SPC_DELETE_TABLE_ERROR = "Unable to delete the table";
    public static String SAM_SPC_DELETE_SEQ_ERROR = "Unable to delete the sequence";
    public static String SAM_SPC_RESET_COUNTER_SUCCESS = "Successfully resetted the counter!";
    public static String IMAGE_DOES_NOT_EXISTS = "Image does not exists";


    //common
    public static String DELETE_ERROR_HEADER = "Delete Error!";


    //inward
    public static String INWARD_DOES_NOT_EXISTS = "Inward does not exist";
    public static String INWARD_DELETE_ERROR  = "Inward delete error!";
    public static String INWARD_GET = "Retrieved Inward(s)!";
    public static String INWARD_GET_DESC = "Successfully retrieved Inward(s)";
    public static String INWARD_UPDATED = "Inward Updated!";
    public static String INWARD_UPDATED_DESC = "Inward updated successfully!";
    public static String INWARD_CREATED = "Inward Created!";
    public static String INWARD_CREATED_DESC = "Inward Created Successfully!";
    public static String INWARD_DELETED = "Inward Deleted!";
    public static String INWARD_DELETED_DESC = "Inward Deleted Successfully";
    public static String INWARD_COUNT = "Inward Count";
    public static String INWARD_COUNT_DESC = "Successfully returned Inward count";

    //tags
    public static String TAG_DOES_NOT_EXISTS = "Tag does not exist";
    public static String TAG_DELETE_ERROR  = "Tag delete error!";
    public static String TAG_GET = "Retrieved Tag(s)!";
    public static String TAG_GET_DESC = "Successfully retrieved Tag(s)";
    public static String TAG_UPDATED = "Tag Updated!";
    public static String TAG_UPDATED_DESC = "Tag updated successfully!";
    public static String TAG_CREATED = "Tag Created!";
    public static String TAG_CREATED_DESC = "Tag Created Successfully!";
    public static String TAG_DELETED = "Tag Deleted!";
    public static String TAG_DELETED_DESC = "Tag Deleted Successfully";

    //constants
    //tags
    public static String CONSTANT_DOES_NOT_EXISTS = "Constant does not exist";
    public static String CONSTANT_DELETE_ERROR  = "Constant delete error!";
    public static String CONSTANT_GET = "Retrieved Constant(s)!";
    public static String CONSTANT_GET_DESC = "Successfully retrieved Constant(s)";
    public static String CONSTANT_UPDATED = "Constant Updated!";
    public static String CONSTANT_UPDATED_DESC = "Constant updated successfully!";
    public static String CONSTANT_CREATED = "Constant Created!";
    public static String CONSTANT_CREATED_DESC = "Constant Created Successfully!";
    public static String CONSTANT_DELETED = "Constant Deleted!";
    public static String CONSTANT_DELETED_DESC = "Constant Deleted Successfully";

    //production planning
    public static String PRODUCTION_PLANNING = "Production Planning";
    public static String PRODUCTION_PLANNING_SET = "Production Planning";
    public static String PRODUCTION_PLANNING_SET_DESC = "Successfully set the production planning";
    public static String PRODUCTION_DOES_NOT_EXISTS = "Production does not exists";

    //checker

    public static String CHECKER_INWARD_GET = "Retrieved Inwards(s)!";
    public static String CHECKER_INWARD_GET_DESC = "Successfully retrieved Inwards(s) with produced tags";
    public static String CHECKER_TAG_GET = "Retrieved produced Tag(s)!";
    public static String CHECKER_TAG_GET_DESC = "Successfully retrieved tags with produced status";
    public static String CHECKING_SAVE_ERROR = "Could not save the checking plan";
    public static String CHECKING_CREATED = "Checking Created!";
    public static String CHECKING_CREATED_DESC = "Checking Created Successfully!";
    public static String CHECKING_DELETED = "Checking Deleted!";
    public static String CHECKING_DELETED_DESC = "Checking Deleted Successfully";
    public static String CHECKING_GET = "Retrieved Checking Plan(s)!";
    public static String CHECKING_GET_DESC = "Successfully retrieved Checking Plan(s)";

}

