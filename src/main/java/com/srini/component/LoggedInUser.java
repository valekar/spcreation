package com.srini.component;

import com.srini.config.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;



public class LoggedInUser {

    private MyUserPrincipal myUserPrincipal;

    //final static LoggedInUser loggedInUser = new LoggedInUser();

    //@Autowired
    public LoggedInUser(){
        this.myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();;
    }



    public MyUserPrincipal getMyUserPrincipal(){
        return  this.myUserPrincipal;
    }


    public String getUserName(){
        return this.myUserPrincipal.getUser().getUsername();
    }
    public String getUserEmail(){
        return this.myUserPrincipal.getUser().getEmail();
    }

    public Long getUserId(){
        return this.myUserPrincipal.getUser().getId();
    }
}


