package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "papers")
@Where(clause = "is_active=true")
public class Paper extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "papers_id_seq")
    @SequenceGenerator(name = "papers_id_seq", sequenceName = "papers_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name="type")
    private String type;

    @Column(name="quality")
    private String quality;

    @Column(name="size")
    private Integer size;

    public Long getId() {
        return id;
    }

    /*public void setId(Long id) {
        this.id = id;
    }*/

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
