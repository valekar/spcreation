package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "appliques")
@Where(clause = "is_active=true")
public class Applique extends  BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "app_id_seq")
    @SequenceGenerator(name = "app_id_seq", sequenceName = "appliques_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "number")
    private Integer number;
    @Column(name = "type")
    private String type;
    @Column(name = "color")
    private String color;

    public Long getId() {
        return id;
    }

   /* public void setId(Long id) {
        this.id = id;
    }*/

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
