package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Table(name = "departments")
@Entity
@Where(clause = "is_active = true")
public class Department extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "departments_id_seq")
    @SequenceGenerator(name = "departments_id_seq", sequenceName = "departments_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

   /* public void setId(Long id) {
        this.id = id;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
