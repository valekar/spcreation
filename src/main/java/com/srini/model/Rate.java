package com.srini.model;


import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "rates")
@Where(clause = "is_active=true")
public class Rate extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "rates_id_seq")
    @SequenceGenerator(name = "rates_id_seq", sequenceName = "rates_id_seq", allocationSize = 1)
    @Column(name = "id")
    Long id;

    @Column(name = "stitch_numbers")
    Integer stitches;

    @Column(name="rate")
    Float rate;

    @Column(name="type")
    String machineType;

    public Long getId() {
        return id;
    }

   /* public void setId(Long id) {
        this.id = id;
    }*/

    public Integer getStitches() {
        return stitches;
    }

    public void setStitches(Integer stitches) {
        this.stitches = stitches;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }
}
