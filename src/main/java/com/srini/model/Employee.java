package com.srini.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "employees")
@Where(clause = "is_active = true")
public class Employee extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "employee_id_seq")
    @SequenceGenerator(name ="employee_id_seq" , sequenceName = "employees_emp_id_seq", allocationSize = 1)
    @Column(name = "emp_id")
    private Long id;


    @Column(name = "name")
    private String name;

    @Column(name="age")
    private Long age;

    @Column(name = "address")
    private String address;

    @Column(name = "sex")
    private String sex;

    @Column(name = "blood_group")
    private String blood_group;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JoinTable(name = "employees_departments",
    joinColumns = @JoinColumn(name = "employee_id",referencedColumnName = "emp_id"),
    inverseJoinColumns = @JoinColumn(name = "department_id",referencedColumnName = "id"))
    private Set<Department> departments;


    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    public Long getId() {
        return id;
    }

    /*public void setId(Long id) {
        this.id = id;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }


}
