package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "machines")
@Where(clause = "is_active = true")
public class Machine extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "machines_id_seq")
    @SequenceGenerator(name = "machines_id_seq", sequenceName = "machines_id_seq", allocationSize = 1)
    @Column(name = "id")
    Long id;

    @Column(name = "name")
    String name;

    @Column(name ="heads")
    Integer heads;

    @Column(name ="type")
    String type;

    @Column(name="stitch_capacity")
    Long stitchCapacity;

    public Long getStitchCapacity() {
        return stitchCapacity;
    }

    public void setStitchCapacity(Long stitchCapacity) {
        this.stitchCapacity = stitchCapacity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeads() {
        return heads;
    }

    public void setHeads(Integer heads) {
        this.heads = heads;
    }


}
