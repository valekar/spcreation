package com.srini.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "constants")
@Setter
@Getter
@Where(clause = "is_active = true")
public class Constant  extends BaseEntity{

    @Id
    @GeneratedValue(generator = "constants_seq_id",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "constants_seq_id",sequenceName = "constants_id_seq",allocationSize = 1)
    private Long id;

    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private String value;
}
