package com.srini.model;


import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "combos")
@Where(clause = "is_active=true")
public class Combo extends BaseEntity{

    @GeneratedValue(strategy = GenerationType.AUTO, generator = "combos_seq_id")
    @SequenceGenerator(name = "combos_seq_id",sequenceName = "combos_id_seq",allocationSize = 1)
    @Id
    @Column(name = "id")
    Long id;

    @Column(name = "cones")
    Float cones;

    @Column(name="boxes")
    Float boxes;

    @Column(name = "frame_size")
    Float frameSize;

    @Column(name="layers")
    Integer layers;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REMOVE})
    @JoinTable(name="combos_threads",
    joinColumns = @JoinColumn(name = "combo_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "thread_id",referencedColumnName = "id"))
    private Thread thread;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "combos_appliques",
    joinColumns = @JoinColumn(name = "combo_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "applique_id",referencedColumnName = "id"))
    private Applique applique;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "combos_papers",
    joinColumns = @JoinColumn(name = "combo_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "paper_id",referencedColumnName = "id"))
    private Paper paper;


    public Long getId() {
        return id;
    }

    /*public void setId(Long id) {
        this.id = id;
    }*/

    public Float getCones() {
        return cones;
    }

    public void setCones(Float cones) {
        this.cones = cones;
    }

    public Float getBoxes() {
        return boxes;
    }

    public void setBoxes(Float boxes) {
        this.boxes = boxes;
    }

    public Float getFrameSize() {
        return frameSize;
    }

    public void setFrameSize(Float frameSize) {
        this.frameSize = frameSize;
    }

    public Integer getLayers() {
        return layers;
    }

    public void setLayers(Integer layers) {
        this.layers = layers;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public Applique getApplique() {
        return applique;
    }

    public void setApplique(Applique applique) {
        this.applique = applique;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public boolean equals(Object o){
        if(this ==o) return true;
        if(o==null ||!(o instanceof Combo)){
            return false;
        }
        Combo otherCombo = (Combo)o;
        if(id == null) return false;
        return id.equals(otherCombo.getId());
    }

    public int hashCode(){
        if(id!=null){
            return id.hashCode();
        }
        else {
            return super.hashCode();
        }
    }
}
