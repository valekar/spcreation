package com.srini.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "checkings_tags")
public class CheckTag extends BaseEntity {


    @Id
    @GeneratedValue(generator = "checkings_tag_id_seq",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "checkings_tag_id_seq",sequenceName = "checkings_tags_id_seq",allocationSize = 1)
    @Column(name = "id")
    private Long id;


    @OneToOne
    @JoinTable(name = "checkings_tags_tags_brid",
            joinColumns = @JoinColumn(name = "checking_tag_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id",referencedColumnName = "id"))
    private Tag tag;
}
