package com.srini.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "checkings_employees")
@Where(clause = "is_active=true")
public class CheckEmployee extends BaseEntity {


    @Id
    @GeneratedValue(generator = "checkings_employees_id_seq", strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "checkings_employees_id_seq", sequenceName = "checkings_employees_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @OneToMany
    @JoinTable(name = "checkings_tags_checkings_employees_brid",
            joinColumns = @JoinColumn(name = "checking_employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "checkings_tag_id", referencedColumnName = "id"))
    private Set<CheckTag> assignedTags;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "employees_checkings_employees_brid",
            joinColumns = @JoinColumn(name = "checking_employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "emp_id"))
    private Employee checker;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "employees_helper_checkings_employees_brid",
            joinColumns = @JoinColumn(name = "checking_employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "emp_id"))
    private Employee helper;


    @Column(name = "assigned_quantity")
    private Long assignedQuantity;
}
