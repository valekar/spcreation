package com.srini.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(TableMetadata.TableMetadataKey.class)
@Table(name = "columns", schema = "information_schema")
public class TableMetadata {
    @Column(name = "column_name")
    @Id
    String columnName;

    @Column(name = "table_name")
    @Id
    String tableName;


    @Column(name = "table_schema")
    String tableSchema;



    public static class TableMetadataKey implements Serializable {
        String columnName;
        String tableName;
    }
}
