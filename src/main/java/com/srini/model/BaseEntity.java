package com.srini.model;

import com.srini.config.LoggedInUserSingleton;
import com.srini.util.Utility;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
public abstract class BaseEntity {

    @Column(name = "created_at")
    private Timestamp created_at;

    @Column(name = "updated_at")
    private Timestamp updated_at;

    @Column(name = "created_by")
    private String created_by;

    @Column(name = "created_user_id")
    private Long created_user_id;

    @Column(name = "updated_by")
    private String updated_by;

    @Column(name = "updated_user_id")
    private Long updated_user_id;

    @Column(name = "is_active")
    private boolean active = true;


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public Long getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(Long updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Long getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(Long created_user_id) {
        this.created_user_id = created_user_id;
    }

    public void setAddAuditFields(){
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
        this.created_at = Utility.getTime();
        this.created_by = loggedInUser.getUserName();
        this.created_user_id = loggedInUser.getUserId();

        this.updated_at = Utility.getTime();
        this.updated_by = loggedInUser.getUserName();
        this.updated_user_id = loggedInUser.getUserId();
    }

    public void setEditAuditFields(){
        LoggedInUserSingleton loggedInUser = LoggedInUserSingleton.getInstance();
        this.updated_at = Utility.getTime();
        this.updated_by = loggedInUser.getUserName();
        this.updated_user_id = loggedInUser.getUserId();
    }


}
