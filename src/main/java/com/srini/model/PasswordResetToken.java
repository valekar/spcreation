package com.srini.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name="password_reset_tokens")
public class PasswordResetToken {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator= "password_reset_seq_id")
    @SequenceGenerator(name="password_reset_seq_id", sequenceName = "reset_password_token_token_id_seq",allocationSize = 1)
    @Column(name = "token_id",nullable = false,updatable = false)
    private Long id;

    @Column(name="token")
    private String token;

    @Column(name = "user_id")
    private Long user_id;

    @Column(name = "expiry_date")
    private Timestamp expiryDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Timestamp getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Timestamp expiryDate) {
        this.expiryDate = expiryDate;
    }
}
