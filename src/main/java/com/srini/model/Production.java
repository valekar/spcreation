package com.srini.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "productions")
///PlanningForm
public class Production extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "productions_id_seq")
    @SequenceGenerator(name = "productions_id_seq", sequenceName = "productions_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "shift")
    private String shift;

    @Column(name = "planningdate")
    private Timestamp planningDate;


    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "machines_prod_brid",
            joinColumns = @JoinColumn(name = "production_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "machine_production_id",referencedColumnName = "id"))
    private Set<ProdMachine> machines;

}
