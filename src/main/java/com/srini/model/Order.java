package com.srini.model;

import com.srini.constant.CONSTANTS;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "orders")
@Where(clause = "is_active=true")
public class Order extends BaseEntity {

    @GeneratedValue(strategy = GenerationType.AUTO, generator = "orders_id_seq")
    @SequenceGenerator(name = "orders_id_seq" ,sequenceName = "orders_id_seq", allocationSize = 1)
    @Id
    @Column(name = "id")
    Long id;

    @Column(name = "status")
    String status = CONSTANTS.ORDER_PENDING;

    @Column(name="grade")
    String grade;

    @Column(name="design_name")
    String designName;

    @Column(name = "embroidery_type")
    String embroideryType;

    @Column(name = "duration")
    Integer duration;

    @Column(name = "quantity")
    Integer quantity;

    @Column(name="fabric_color")
    String fabricColor;

    @Column(name = "fabric_size")
    private String fabric_size;

    @Column(name="measurement")
    private String measurement;

    @Column(name="part")
    private String part;

    @Column(name="position")
    private String position;

    @Column(name = "no_combos")
    private Integer noOfCombos;

    @Column(name = "stitches")
    private Long stitches;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "orders_combos",
    joinColumns = @JoinColumn(name = "order_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "combo_id",referencedColumnName = "id"))
    private Set<Combo> combos;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JoinTable(name="orders_customers",
    joinColumns = @JoinColumn(name = "order_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name="customer_id",referencedColumnName = "id"))
    private Customer customer;


    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JoinTable(name="orders_images",
    joinColumns = @JoinColumn(name="order_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name="image_id",referencedColumnName = "id"))
    private Image image;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    //this is used to generate SAM and SPC numbers
    @Column(name = "sam")
    private String SAM;

    @Column(name = "spc")
    private String SPC;

    @Column(name = "sam_number")
    private Integer samNumber;

    @Column(name = "spc_number")
    private Integer spcNumber;

    public String getSAM() {
        return SAM;
    }

    public void setSAM(String SAM) {
        this.SAM = SAM;
    }

    public String getSPC() {
        return SPC;
    }

    public void setSPC(String SPC) {
        this.SPC = SPC;
    }

    public Integer getSamNumber() {
        return samNumber;
    }

    public void setSamNumber(Integer samNumber) {
        this.samNumber = samNumber;
    }

    public Integer getSpcNumber() {
        return spcNumber;
    }

    public void setSpcNumber(Integer spcNumber) {
        this.spcNumber = spcNumber;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public String getEmbroideryType() {
        return embroideryType;
    }

    public void setEmbroideryType(String embroideryType) {
        this.embroideryType = embroideryType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }

    public String getFabric_size() {
        return fabric_size;
    }

    public void setFabric_size(String fabric_size) {
        this.fabric_size = fabric_size;
    }

    public Integer getNoOfCombos() {
        return noOfCombos;
    }

    public void setNoOfCombos(Integer noOfCombos) {
        this.noOfCombos = noOfCombos;
    }

    public Long getStitches() {
        return stitches;
    }

    public void setStitches(Long stitches) {
        this.stitches = stitches;
    }

    public Set<Combo> getCombos() {
        return combos;
    }

    public void setCombos(Set<Combo> combos) {
        this.combos = combos;
    }

    //added three new columns

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
