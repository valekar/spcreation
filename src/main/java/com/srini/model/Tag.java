package com.srini.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "tags")
@Where(clause = "is_active=true")
@Setter
@Getter

//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
public class Tag extends BaseEntity implements Comparable<Tag>{

    @Id
    @GeneratedValue(generator = "tags_seq_id",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "tags_seq_id",sequenceName = "tags_id_seq",allocationSize = 1)
    private Long id;

    @Column(name = "tag_id")
    private String tagId;

    @Column(name="size")
    private String size;

    @Column(name = "bundle")
    private Long bundle;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name="comments")
    private String comments;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    //@JsonIgnore
    @JoinTable(name = "inwards_tags",
            joinColumns = @JoinColumn(name = "tag_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name="inward_id",referencedColumnName = "id"))
    private Inward inward;


    @Column(name="status")
    private String status;

    @Column(name = "quantity_complete")
    private Long quantityCompleted;

    @Column(name = "mistakes")
    private Long mistakes = 0L;

    @Column(name = "check_quantity_complete")
    private Long checkingQuantityCompleted;

    @Column(name = "check_mistakes")
    private Long checkingMistakes;

    public int compareTo(Tag tag) {
        Long id = ((Tag) tag).getId();
        //ascending order
        return (int)(this.id - id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        return id != null ? id.equals(tag.id) : tag.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
