package com.srini.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Setter
@Getter
@Table(name = "checkings")
@Entity
@Where(clause = "is_active=true")
public class Checking extends BaseEntity {

    @Id
    @GeneratedValue(generator = "checkings_id_seq",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "checkings_id_seq",sequenceName = "checkings_id_seq",allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "shift")
    private String shift;

    @Column(name = "planning_date")
    private Timestamp planningDate;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "checkings_checkings_employees_brid",
            joinColumns = @JoinColumn(name = "checking_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "checking_employee_id",referencedColumnName = "id"))
    private Set<CheckEmployee> employees;

}
