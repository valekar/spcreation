package com.srini.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Table(name = "inwards_productions")
@Entity
@Getter
@Setter
//InwardForm
public class ProdInward extends BaseEntity{


    @Id
    @GeneratedValue(generator = "inwards_productions_id",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "inwards_productions_id",sequenceName = "inwards_productions_id_seq",allocationSize = 1)
    private Long id;

    @Column(name = "tag_cloth_quantity")
    private Long tagsClothQuantity;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "inwards_prod_inwards_brid",
            joinColumns = @JoinColumn(name = "inward_production_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "inward_id",referencedColumnName = "id"))
    private Inward inward;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "inwards_prod_operator_emp",
            joinColumns = @JoinColumn(name = "inward_production_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id",referencedColumnName = "emp_id"))
    private Employee operator;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "inwards_prod_incharge_emp",
            joinColumns = @JoinColumn(name = "inward_production_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id",referencedColumnName = "emp_id"))
    private Employee inCharge;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "inwards_prod_framer_emp",
            joinColumns = @JoinColumn(name = "inward_production_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id",referencedColumnName = "emp_id"))
    private Employee framer;



    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name="inwards_prod_tags",
            joinColumns = @JoinColumn(name = "inward_production_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id",referencedColumnName = "id"))
    private Set<Tag> toBeProcessedTags;

}
