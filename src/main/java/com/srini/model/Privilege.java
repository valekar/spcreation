package com.srini.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "privileges")
public class Privilege {
    @Column(name ="id")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="order_tab",nullable = false)
    private String order_tab;

    @Column(name = "inward_tab" ,nullable = false,columnDefinition = "none")
    private String inward_tab;

    @Column(name="production_tab",nullable = false)
    private String production_tab;

    @Column(name = "checking_tab",nullable = false)
    private String checking_tab;

    @Column(name="reports_tab",nullable = false)
    private String reports_tab;

    @Column(name = "admin_tab", nullable = false,columnDefinition = "none")
    private String admin_tab;

    @Column(name = "store_tab",nullable = false)
    private String store_tab;

    @Column(name="company_tab",nullable = false)
    private String company_tab;



}
