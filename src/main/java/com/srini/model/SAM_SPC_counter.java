package com.srini.model;

import javax.persistence.*;

@Entity
@Table(name = "SAM_SPC_counter")
public class SAM_SPC_counter {

    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SAM_SPC_counter_id_seq")
    @SequenceGenerator(name = "SAM_SPC_counter_id_seq", sequenceName = "SAM_SPC_counter_id_seq",allocationSize = 1)
    @Id
    @Column(name = "id")
    Long id;

    public Long getId() {
        return id;
    }

    /*public void setId(Long id) {
        this.id = id;
    }*/
}
