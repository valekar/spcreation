package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "images")
@Where(clause = "is_active=true")
public class Image extends BaseEntity{

    @GeneratedValue(strategy = GenerationType.AUTO, generator = "images_seq_id")
    @SequenceGenerator(name = "images_seq_id",sequenceName = "images_id_seq",allocationSize = 1)
    @Id
    Long id;

    @Column(name = "name")
    String name;


    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JoinTable(name = "orders_images",
    joinColumns = @JoinColumn(name = "order_id",referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "image_id",referencedColumnName = "id"))
    private Order order;

    private Image(){}

    public Image(String name){
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    /*public void setId(Long id) {
        this.id = id;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
