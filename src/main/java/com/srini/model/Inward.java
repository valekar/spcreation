package com.srini.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;


@Table(name="inwards")
@Entity
@Where(clause = "is_active=true")
@Setter
@Getter

//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="id")
public class Inward extends BaseEntity {

    @Id
    @GeneratedValue(generator = "inwards_seq_id",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "inwards_seq_id",sequenceName = "inwards_id_seq",allocationSize = 1)
    private Long id;

    @Column(name = "number")
    private Long inwardNumber;

    @Column(name="lay")
    private Timestamp lay;

    @Transient
    //concrete variable
    private String layStr;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinTable(name = "inwards_orders",
            joinColumns = @JoinColumn(name = "inward_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "order_id",referencedColumnName = "id"))
    //@WhereJoinTable(clause = "")
    private Order order;


    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL,mappedBy = "inward")
    //here mappedBy = "inward" is the property defined in Tag class
    //@Transient
    //@JsonBackReference
    private Set<Tag> tags;

}
