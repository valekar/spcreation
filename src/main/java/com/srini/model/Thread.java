package com.srini.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;

//@Embeddable
@Entity
@Table(name = "threads")
@Where(clause = "is_active=true")
public class Thread extends BaseEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "threads_id_seq")
    @SequenceGenerator(name = "threads_id_seq", sequenceName = "threads_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name="color")
    private String color;

    @Column(name="type")
    private String type;

    //@Column(name="shape")
    //private String shape;

    /*public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }
*/
    public Long getId() {
        return id;
    }

   /* public void setId(Long id) {
        this.id = id;
    }*/

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /*public boolean equals(Object o){
        if(this ==o) return true;
        if(o==null ||!(o instanceof Thread)){
            return false;
        }
        Thread other = (Thread)o;
        if(id == null ||brandName == null) return false;
        return id.equals(other.getId()) && brandName.equals(other.getBrandName());
    }

    public int hashCode(){
        if(id!=null){
            return id.hashCode();
        }
        else {
            return super.hashCode();
        }
    }*/
}
