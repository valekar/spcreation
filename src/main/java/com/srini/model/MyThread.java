package com.srini.model;

import javax.persistence.*;

//@Entity
//@Table(name = "threads")
public class MyThread {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "thread_id")
	private Long id;
	
	@Column(name = "thread_name")
	private String name;
	
	
	public String getThreadName() {
		return name;
	}
	
	
	public Long getThreadId() {
		return id;
	}
	
	
}
