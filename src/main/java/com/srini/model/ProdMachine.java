package com.srini.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Table(name = "machines_productions")
@Entity
@Getter
@Setter
//MachineForm
public class ProdMachine extends BaseEntity{

    @Id
    @GeneratedValue(generator = "machines_productions_id_seq",strategy = GenerationType.AUTO)
    @SequenceGenerator(name = "machines_productions_id_seq",sequenceName = "machines_productions_id_seq",allocationSize = 1)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "heads")
    private Long heads;

    @Column(name = "stitch_capacity")
    private Long stitchCapacity;

    @Column(name = "name")
    private String name;

    @Column(name = "no_of_orders")
    private Long noOfOrders;

    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "machines_prod_inwards_prod_brid",
            joinColumns = @JoinColumn(name = "machine_prod_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "inward_prod_id",referencedColumnName = "id"))
    private Set<ProdInward> orders;
}
