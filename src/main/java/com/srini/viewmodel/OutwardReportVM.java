package com.srini.viewmodel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OutwardReportVM  {

    String lay;
    String part;
    String SPC;
    int totalTags ;
    int totalClothes;
    int totalCheckingMistakes;
    int totalProdMistakes;
    int totalProduced;
    int toBeProdBalanceClothes;
    int toBeCheckBalanceClothes;
    int totalBundles;

    int totalChecked;
    String orderStatus;
    String updated_by;
    String formatted_updated_at;
}
