package com.srini.viewmodel;

import com.srini.model.Applique;

public class AppliqueVM extends BaseVM {

    private Long id;
    private Integer number;
    private String color;
    private String type;

    public AppliqueVM(Applique model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.number = model.getNumber();
        this.color = model.getColor();
        this.type = model.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
