package com.srini.viewmodel;

import com.srini.util.Utility;

import java.sql.Time;
import java.sql.Timestamp;

public class BaseVM {

     Timestamp created_at;
     Timestamp updated_at;
     String created_by;
     Long created_user_id;
     String updated_by;
     Long updated_user_id;
     String formatted_updated_at;
     String formatted_created_at;


    BaseVM(){}

    BaseVM(Timestamp created_at, Timestamp updated_at,String created_by,
           Long created_user_id,String updated_by,Long updated_user_id){
        this.created_at = created_at;
        this.updated_at = updated_at;
        if(updated_at!=null)
            this.formatted_updated_at = Utility.getSimpleDateFormatter(updated_at);
        if(created_at!=null)
            this.formatted_created_at = Utility.getSimpleDateFormatter(created_at);

        this.created_by = created_by;
        this.created_user_id = created_user_id;
        this.updated_by = updated_by;
        this.updated_user_id = updated_user_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Long getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(Long created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public Long getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(Long updated_user_id) {
        this.updated_user_id = updated_user_id;
    }


    public String getFormatted_updated_at() {
        return formatted_updated_at;
    }

    public void setFormatted_updated_at(String formatted_updated_at) {
        this.formatted_updated_at = formatted_updated_at;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        this.formatted_created_at = formatted_created_at;
    }
}
