package com.srini.viewmodel;

import com.srini.model.Rate;

public class RateVM extends BaseVM{
    private Long id;
    private Integer stitches;
    private Float rate;
    private String machineType;

    public RateVM(Rate rate){
        super(rate.getCreated_at(),rate.getUpdated_at(),rate.getCreated_by(),
                rate.getCreated_user_id(),rate.getUpdated_by(),rate.getUpdated_user_id());
        this.id = rate.getId();
        this.machineType = rate.getMachineType();
        this.stitches = rate.getStitches();
        this.rate = rate.getRate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStitches() {
        return stitches;
    }

    public void setStitches(Integer stitches) {
        this.stitches = stitches;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }
}
