package com.srini.viewmodel;

import com.srini.model.CheckTag;
import com.srini.model.Tag;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CheckTagVM extends BaseVM{

    private Long id;
    private Tag tag;

    public CheckTagVM(CheckTag model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.tag = model.getTag();
    }

    public CheckTagVM(){}

}
