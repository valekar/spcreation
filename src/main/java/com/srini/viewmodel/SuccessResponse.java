package com.srini.viewmodel;

import org.springframework.http.HttpStatus;

public class SuccessResponse<T> {

    private int code;
    private String description;
    //private String message;
    private HttpStatus status;
    private String header;
    private T obj;

    public SuccessResponse(int code, String header,String description, HttpStatus status,T obj){
        this.code = code;
        this.description = description;
        this.header = header;
        this.status = status;
        this.obj = obj;
    }

    public SuccessResponse(int code, String header,String description, HttpStatus status){
        this.code = code;
        this.description = description;
        this.header = header;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }
}
