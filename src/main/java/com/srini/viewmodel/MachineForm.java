package com.srini.viewmodel;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MachineForm{
    private Long idField;
    private String typeField;
    private Long headsField;
    private Long stitchCapacityField;
    private Long noOfOrders;
    private String name;
    private List<OrderForm> orders;
}

