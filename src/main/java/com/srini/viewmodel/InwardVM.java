package com.srini.viewmodel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.srini.model.Inward;
import com.srini.model.Order;
import com.srini.model.Tag;
import com.srini.util.Utility;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Setter
@Getter
public class InwardVM extends BaseVM {

    private Long id;
    private Timestamp lay;
    private Long inwardNumber;
    private String layStr;
    private Order order;
    @JsonManagedReference
    private List<Tag> tags;

    public InwardVM(Inward inward){
        super(inward.getCreated_at(),inward.getUpdated_at(),inward.getCreated_by(),
                inward.getCreated_user_id(),inward.getUpdated_by(),inward.getUpdated_user_id());

        this.id = inward.getId();
        if(inward.getLay()!=null){
            this.layStr = Utility.getSimpleDateFormatter(inward.getLay());
        }

        this.inwardNumber = inward.getInwardNumber();
        this.order = inward.getOrder();
        List<Tag> tags = new ArrayList<>(inward.getTags());
        Collections.sort(tags);
        this.tags = tags;
    }

}
