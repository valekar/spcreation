package com.srini.viewmodel;

import com.srini.model.CheckEmployee;
import com.srini.model.CheckTag;
import com.srini.model.Employee;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class CheckEmployeeVM  extends BaseVM{

    private Long id;
    private Employee checker;
    private Employee helper;
    private Set<CheckTagVM> assignedTags;
    private Long assignedQuantity;

    public CheckEmployeeVM(CheckEmployee model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.checker = model.getChecker();
        Set<CheckTagVM> checkTagVMS = new HashSet<>();
        for(CheckTag checkTag:model.getAssignedTags()){
            checkTagVMS.add(new CheckTagVM(checkTag));
        }
        this.assignedTags = checkTagVMS;
        this.helper = model.getHelper();
        this.assignedQuantity = model.getAssignedQuantity();
    }

    public CheckEmployeeVM(){}

}
