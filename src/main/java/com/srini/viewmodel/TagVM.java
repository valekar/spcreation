package com.srini.viewmodel;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.srini.model.Inward;
import com.srini.model.Tag;
import com.srini.util.Utility;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonBackReference;

@Getter
@Setter
public class TagVM extends BaseVM{
    private Long id;
    private String tagId;
    private String size;
    private Long bundle;
    private Long quantity;
    private String comments;
    @JsonBackReference
    private Inward inward;
    private String status;
    private Long quantityCompleted;
    private Long mistakes;
    private Long checkingQuantityCompleted;
    private Long checkingMistakes;


    public TagVM(Tag tag){
        super(tag.getCreated_at(),tag.getUpdated_at(),tag.getCreated_by(),
                tag.getCreated_user_id(),tag.getUpdated_by(),tag.getUpdated_user_id());

        this.id = tag.getId();
        this.tagId = tag.getTagId();
        this.size = tag.getSize();
        this.bundle = tag.getBundle();
        this.quantity = tag.getQuantity();
        this.comments = tag.getComments();
        this.inward = tag.getInward();
        this.status = tag.getStatus();
        this.quantityCompleted = tag.getQuantityCompleted();
        this.mistakes = tag.getMistakes();
        this.checkingQuantityCompleted =tag.getCheckingQuantityCompleted();
        this.checkingMistakes = tag.getCheckingMistakes();
    }
    
}
