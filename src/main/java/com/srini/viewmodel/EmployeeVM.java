package com.srini.viewmodel;

import com.srini.model.Department;
import com.srini.model.Employee;
import com.srini.util.Utility;
//import org.aspectj.apache.bcel.classfile.Utility;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class EmployeeVM {

    private String name;
    private Long id;
    private String sex;
    private String blood_group;
    private String address;
    private Timestamp updated_at;
    private Timestamp created_at;
    private Long age;
    private String formatted_updated_at;
    private String formatted_created_at;
    private String created_by;
    private Long created_user_id;
    private String updated_by;
    private Long updated_user_id;
    private Set<Department> departments;
    //private SimpleDateFormat simpleDateFormat;

    public EmployeeVM(Employee employee){
        this.address = employee.getAddress();
        this.blood_group = employee.getBlood_group();
        this.id = employee.getId();
        this.age = employee.getAge();
        this.sex = employee.getSex();
        this.created_at = employee.getCreated_at();
        this.updated_at = employee.getUpdated_at();
        this.name = employee.getName();
        //simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm:ss:S");
        if(this.updated_at!=null)
            this.formatted_updated_at = Utility.getSimpleDateFormatter(this.updated_at);
        if(this.created_at!=null)
            this.formatted_created_at = Utility.getSimpleDateFormatter(this.created_at);

        this.created_by = employee.getCreated_by();
        this.created_user_id = employee.getCreated_user_id();
        this.updated_by = employee.getUpdated_by();
        this.updated_user_id = employee.getCreated_user_id();

        //set the depart
        this.departments = employee.getDepartments();

    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public Long getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(Long updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public Long getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(Long created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }


    public String getFormatted_updated_at() {
        return formatted_updated_at;
    }

    public void setFormatted_updated_at(String formatted_updated_at) {
        //this.formatted_updated_at =  simpleDateFormat.format(this.updated_at);
        //this.formatted_updated_at = formatted_updated_at;
    }

    public String getFormatted_created_at() {
        return formatted_created_at;
    }

    public void setFormatted_created_at(String formatted_created_at) {
        //this.formatted_created_at = simpleDateFormat.format(this.created_at);
        //this.formatted_created_at = formatted_created_at;
    }


    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }
}
