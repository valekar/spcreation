package com.srini.viewmodel;

import com.srini.model.Thread;

public class ThreadVM extends BaseVM {
    private Long id;
    private String brandName;
    private String color;
    private String type;
    private String shape;

    public ThreadVM(Thread model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());

        this.id = model.getId();
        this.brandName = model.getBrandName();
        this.color = model.getColor();
        this.type = model.getType();
    }


    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
