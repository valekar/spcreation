package com.srini.viewmodel;

import com.srini.model.CheckEmployee;
import com.srini.model.Checking;
import com.srini.util.Utility;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class CheckingVM extends BaseVM {

    private Long id;
    private String shift;
    private String planningDate;
    private Set<CheckEmployeeVM> employees;

    public CheckingVM(Checking model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.shift = model.getShift();
        this.planningDate = Utility.getSimpleDateFormatter(model.getPlanningDate());
        Set<CheckEmployeeVM> checkEmployeeVMS = new HashSet<>();
        for(CheckEmployee checkEmployee:model.getEmployees()){
            checkEmployeeVMS.add(new CheckEmployeeVM(checkEmployee));
        }
        this.employees = checkEmployeeVMS;
    }

    public CheckingVM(){}
}
