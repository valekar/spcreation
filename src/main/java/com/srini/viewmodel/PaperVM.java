package com.srini.viewmodel;

import com.srini.model.Paper;

public class PaperVM extends BaseVM {

    private Long id;
    private String type;
    private String quality;
    private Integer size;

    public PaperVM(Paper model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.type = model.getType();
        this.quality = model.getQuality();
        this.size = model.getSize();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
