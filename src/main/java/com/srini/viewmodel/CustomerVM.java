package com.srini.viewmodel;

import com.srini.model.Customer;
import com.srini.util.Utility;

import java.sql.Timestamp;

public class CustomerVM extends BaseVM{

    private Long id;
    private String name;
    private String address;

    public CustomerVM(Customer customer){
        super(customer.getCreated_at(),customer.getUpdated_at(),customer.getCreated_by(),
                customer.getCreated_user_id(),customer.getUpdated_by(),customer.getUpdated_user_id());

        this.id = customer.getId();
        this.name = customer.getName();
        this.address = customer.getAddress();


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
