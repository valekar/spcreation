package com.srini.viewmodel;


import com.srini.model.Tag;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TagForm{
    private Tag tag;
}

