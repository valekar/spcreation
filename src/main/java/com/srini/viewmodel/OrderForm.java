package com.srini.viewmodel;

import com.srini.model.Employee;
import com.srini.model.Inward;
import com.srini.model.Tag;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class OrderForm{
    private Inward inwardField;
    private int orderStitchesField;
    private List<TagForm> tags;
    private Set<TagForm> tagsToBeProcessed;
    private Long tagsClothesQuantity;
    private Employee operatorField;
    private Employee framerField;
    private Employee inchargeField;
}