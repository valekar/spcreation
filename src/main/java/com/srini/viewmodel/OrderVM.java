package com.srini.viewmodel;

import com.srini.model.Combo;
import com.srini.model.Customer;
import com.srini.model.Image;
import com.srini.model.Order;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Set;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class OrderVM extends BaseVM{

    private Long id;
    private String status;
    private String grade;
    String designName;
    String embroideryType;
    Integer duration;
    Integer quantity;
    String fabricColor;
    private String fabric_size;
    private Integer noOfCombos;
    private Long stitches;
    private String SAM;
    private String SPC;
    private Set<Combo> combos;
    //private Integer samNumber;
    //private Integer spcNumber;
    private Customer customer;
    private Image image;
    private Resource imageResource;
    private byte[] imageBytes;
    //adding missed fields
    private String measurement;
    private String position;
    private String part;


    public OrderVM(Order model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());

        this.id = model.getId();
        this.status = model.getStatus();
        this.grade = model.getGrade();
        this.designName = model.getDesignName();
        this.embroideryType = model.getEmbroideryType();
        this.duration = model.getDuration();
        this.quantity = model.getQuantity();
        this.fabricColor = model.getFabricColor();
        this.fabric_size = model.getFabric_size();
        this.noOfCombos = model.getNoOfCombos();
        this.stitches = model.getStitches();
        this.SAM = model.getSAM();
        this.SPC = model.getSPC();

        //combos
        this.combos = model.getCombos();
        this.customer = model.getCustomer();
        this.image = model.getImage();

        //adding missed fields
        this.measurement = model.getMeasurement();
        this.position = model.getPosition();
        this.part = model.getPart();
    }



    public void setImageResource(Resource imageResource) {
        this.imageResource = imageResource;
    }


    public byte[] getImageBytes() throws IOException{
        byte[] bytess = IOUtils.toByteArray(this.imageResource.getInputStream());
        return bytess;
    }



    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Set<Combo> getCombos() {
        return combos;
    }

    public void setCombos(Set<Combo> combos) {
        this.combos = combos;
    }

    public String getFabric_size() {
        return fabric_size;
    }

    public void setFabric_size(String fabric_size) {
        this.fabric_size = fabric_size;
    }

    public String getSAM() {
        return SAM;
    }

    public void setSAM(String SAM) {
        this.SAM = SAM;
    }

    public String getSPC() {
        return SPC;
    }

    public void setSPC(String SPC) {
        this.SPC = SPC;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public String getEmbroideryType() {
        return embroideryType;
    }

    public void setEmbroideryType(String embroideryType) {
        this.embroideryType = embroideryType;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getFabricColor() {
        return fabricColor;
    }

    public void setFabricColor(String fabricColor) {
        this.fabricColor = fabricColor;
    }


    public Integer getNoOfCombos() {
        return noOfCombos;
    }

    public void setNoOfCombos(Integer noOfCombos) {
        this.noOfCombos = noOfCombos;
    }

    public Long getStitches() {
        return stitches;
    }

    public void setStitches(Long stitches) {
        this.stitches = stitches;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status.toUpperCase();
    }

    public void setStatus(String status) {
        this.status = status.toUpperCase();
    }

    //added missing fields

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }
}
