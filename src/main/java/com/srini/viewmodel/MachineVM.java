package com.srini.viewmodel;

import com.srini.model.Machine;
import com.srini.util.Utility;

public class MachineVM extends  BaseVM{

    private Long id;
    private String name;
    private Integer heads;
    private String type;
    public Long stitchCapacity;

    public MachineVM(Machine machine){
        super(machine.getCreated_at(),machine.getUpdated_at(),machine.getCreated_by(),
                machine.getCreated_user_id(),machine.getUpdated_by(),machine.getUpdated_user_id());

        this.id = machine.getId();
        this.name = machine.getName();
        this.heads = machine.getHeads();
        this.type = machine.getType();
        this.stitchCapacity = machine.getStitchCapacity();

    }

    public Long getStitchCapacity() {
        return stitchCapacity;
    }

    public void setStitchCapacity(Long stitchCapacity) {
        this.stitchCapacity = stitchCapacity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeads() {
        return heads;
    }

    public void setHeads(Integer heads) {
        this.heads = heads;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
