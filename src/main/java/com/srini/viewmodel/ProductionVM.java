package com.srini.viewmodel;

import com.srini.model.ProdMachine;
import com.srini.model.Production;
import com.srini.util.Utility;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Setter
@Getter
public class ProductionVM extends BaseVM{

    private Long id;
    private String shift;
    private String planningDate;
    private Set<ProdMachine> machines;

    public ProductionVM(Production model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.shift = model.getShift();
        this.planningDate = Utility.getSimpleDateFormatter(model.getPlanningDate());
        this.machines = model.getMachines();
    }
}
