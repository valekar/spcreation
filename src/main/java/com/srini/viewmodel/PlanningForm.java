package com.srini.viewmodel;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class PlanningForm {
    private  String shift;
    private String planningDate;
    private ArrayList<MachineForm> machines;
}


