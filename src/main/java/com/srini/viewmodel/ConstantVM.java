package com.srini.viewmodel;

import com.srini.model.Constant;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConstantVM  extends BaseVM{

    private Long id;
    private String key;
    private String value;

    public ConstantVM(Constant model){
        super(model.getCreated_at(),model.getUpdated_at(),model.getCreated_by(),
                model.getCreated_user_id(),model.getUpdated_by(),model.getUpdated_user_id());
        this.id = model.getId();
        this.key = model.getKey();
        this.value  = model.getValue();

    }

}
