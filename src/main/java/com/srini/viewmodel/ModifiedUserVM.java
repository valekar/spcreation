package com.srini.viewmodel;

import com.srini.model.Privilege;

public class ModifiedUserVM {


    private String username;
    private boolean enabled;
    private String email;
    private Privilege privileges;
    private  Long id;
    private String inward_tab;
    private String admin_tab;
    private String production_tab;
    private String order_tab;
    private String checking_tab;
    private String reports_tab;
    private String store_tab;
    private String company_tab;

    public ModifiedUserVM(){

    }

    public ModifiedUserVM(com.srini.model.User user){
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.enabled = user.isEnabled();
        this.privileges = user.getPrivileges();
        if(this.privileges!=null){
            this.admin_tab = this.privileges.getAdmin_tab();
            this.inward_tab = this.privileges.getInward_tab();
            this.production_tab = this.privileges.getProduction_tab();
            this.order_tab = this.privileges.getOrder_tab();
            this.checking_tab = this.privileges.getChecking_tab();
            this.reports_tab = this.privileges.getReports_tab();
            this.store_tab = this.privileges.getStore_tab();
            this.company_tab = this.privileges.getCompany_tab();
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Privilege getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Privilege privileges) {
        this.privileges = privileges;
    }

    public String getInward_tab() {
        return inward_tab;
    }

    public void setInward_tab(String inward_tab) {
        this.inward_tab = inward_tab;
    }

    public String getAdmin_tab() {
        return admin_tab;
    }

    public void setAdmin_tab(String admin_tab) {
        this.admin_tab = admin_tab;
    }


    public String getProduction_tab() {
        return production_tab;
    }

    public void setProduction_tab(String production_tab) {
        this.production_tab = production_tab;
    }

    public String getOrder_tab() {
        return order_tab;
    }

    public void setOrder_tab(String order_tab) {
        this.order_tab = order_tab;
    }

    public String getChecking_tab() {
        return checking_tab;
    }

    public void setChecking_tab(String checking_tab) {
        this.checking_tab = checking_tab;
    }

    public String getReports_tab() {
        return reports_tab;
    }

    public void setReports_tab(String reports_tab) {
        this.reports_tab = reports_tab;
    }

    public String getStore_tab() {
        return store_tab;
    }

    public void setStore_tab(String store_tab) {
        this.store_tab = store_tab;
    }

    public String getCompany_tab() {
        return company_tab;
    }

    public void setCompany_tab(String company_tab) {
        this.company_tab = company_tab;
    }
}
