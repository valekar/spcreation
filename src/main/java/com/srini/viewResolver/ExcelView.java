package com.srini.viewResolver;

import com.srini.model.ProdInward;
import com.srini.model.ProdMachine;
import com.srini.model.Tag;
import com.srini.viewmodel.CheckEmployeeVM;
import com.srini.viewmodel.CheckTagVM;
import com.srini.viewmodel.CheckingVM;
import com.srini.viewmodel.ProductionVM;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


public class ExcelView extends AbstractXlsView {


    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        response.addHeader("Content-disposition", "attachment; filename=People.xlsx");
        response.setContentType("application/vnd.ms-excel");

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");

        style = workbook.createCellStyle();
        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.DARK_GREEN.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        font.setBold(true);
        font.setColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
        style.setFont(font);

        //CellStyle style2 = workbook.createCellStyle();
        //style2.setBorderBottom(BorderStyle.THICK);

        if (model.get("productionVM") != null) {
            ProductionVM productionVM = (ProductionVM) model.get("productionVM");
            Sheet sheet = workbook.createSheet("Production Planning");
            setProductionPlan(sheet, style, productionVM);
        } else if (model.get("checkingVM") != null) {
            CheckingVM checkingVM = (CheckingVM) model.get("checkingVM");
            Sheet sheet = workbook.createSheet("Checking Planning");
            setCheckingPlan(sheet, style, checkingVM);
        }

    }

    private void setCheckingPlan(Sheet sheet, CellStyle style, CheckingVM checkingVM) {
        Row planningRow = sheet.createRow(0);
        planningRow.createCell(0).setCellValue("Planning Date");
        planningRow.getCell(0).setCellStyle(style);
        sheet.autoSizeColumn(0);
        planningRow.createCell(1).setCellValue(checkingVM.getPlanningDate());
        //set shift
        Row shiftRow = sheet.createRow(1);
        shiftRow.createCell(0).setCellValue("Shift");
        shiftRow.getCell(0).setCellStyle(style);
        shiftRow.createCell(1).setCellValue(checkingVM.getShift());

        Row header = sheet.createRow(3);


        header.createCell(0).setCellValue("Sl No");
        header.getCell(0).setCellStyle(style);

        header.createCell(1).setCellValue("Checker");
        header.getCell(1).setCellStyle(style);

        header.createCell(2).setCellValue("Helper");
        header.getCell(2).setCellStyle(style);

        header.createCell(3).setCellValue("Assigned Quantity");
        header.getCell(3).setCellStyle(style);
        sheet.autoSizeColumn(3);

        header.createCell(4).setCellValue("Tag Id");
        header.getCell(4).setCellStyle(style);

        header.createCell(5).setCellValue("Size");
        header.getCell(5).setCellStyle(style);

        header.createCell(6).setCellValue("Quantity");
        header.getCell(6).setCellStyle(style);

        header.createCell(7).setCellValue("Production Completed");
        header.getCell(7).setCellStyle(style);
        sheet.autoSizeColumn(7);

        header.createCell(8).setCellValue("Production Mistakes");
        header.getCell(8).setCellStyle(style);
        sheet.autoSizeColumn(8);

        header.createCell(9).setCellValue("Checking Completed");
        header.getCell(9).setCellStyle(style);
        sheet.autoSizeColumn(9);

        header.createCell(10).setCellValue("Checking Mistakes");
        header.getCell(10).setCellStyle(style);
        sheet.autoSizeColumn(10);

        header.createCell(11).setCellValue("Status");
        header.getCell(11).setCellStyle(style);

        header.createCell(12).setCellValue("Comments");
        header.getCell(12).setCellStyle(style);
        sheet.autoSizeColumn(12);

        int rowCheckEmpCount = 4;
        for(CheckEmployeeVM checkEmployeeVM : checkingVM.getEmployees()){
            Row checkEmpRow = sheet.createRow(rowCheckEmpCount);
            checkEmpRow.createCell(0).setCellValue(checkEmployeeVM.getId());
            checkEmpRow.createCell(1).setCellValue(checkEmployeeVM.getChecker().getName());
            checkEmpRow.createCell(2).setCellValue(checkEmployeeVM.getHelper().getName());
            checkEmpRow.createCell(3).setCellValue(checkEmployeeVM.getAssignedQuantity());
            int rowCheckTagCount = rowCheckEmpCount;
            for(CheckTagVM checkTagVM : checkEmployeeVM.getAssignedTags()){
                if(rowCheckEmpCount == rowCheckTagCount){
                    Row currentRow = sheet.getRow(rowCheckTagCount);
                    currentRow.createCell(4).setCellValue(checkTagVM.getTag().getId());
                    currentRow.createCell(5).setCellValue(checkTagVM.getTag().getSize());
                    currentRow.createCell(6).setCellValue(checkTagVM.getTag().getQuantity());
                    currentRow.createCell(7).setCellValue(checkTagVM.getTag().getQuantityCompleted());
                    currentRow.createCell(8).setCellValue(checkTagVM.getTag().getMistakes());
                    currentRow.createCell(9).setCellValue(checkTagVM.getTag().getCheckingQuantityCompleted());
                    currentRow.createCell(10).setCellValue(checkTagVM.getTag().getCheckingMistakes());
                    currentRow.createCell(11).setCellValue(checkTagVM.getTag().getStatus());
                    sheet.autoSizeColumn(11);
                    currentRow.createCell(12).setCellValue(checkTagVM.getTag().getComments());
                }
                else{
                    Row newRow = sheet.createRow(rowCheckTagCount);
                    newRow.createCell(4).setCellValue(checkTagVM.getTag().getId());
                    newRow.createCell(5).setCellValue(checkTagVM.getTag().getSize());
                    newRow.createCell(6).setCellValue(checkTagVM.getTag().getQuantity());
                    newRow.createCell(7).setCellValue(checkTagVM.getTag().getQuantityCompleted());
                    newRow.createCell(8).setCellValue(checkTagVM.getTag().getMistakes());
                    newRow.createCell(9).setCellValue(checkTagVM.getTag().getCheckingQuantityCompleted());
                    newRow.createCell(10).setCellValue(checkTagVM.getTag().getCheckingMistakes());
                    newRow.createCell(11).setCellValue(checkTagVM.getTag().getStatus());
                    newRow.createCell(12).setCellValue(checkTagVM.getTag().getComments());
                }
                rowCheckTagCount++;
            }

            if(checkEmployeeVM.getAssignedTags().size()>0){
                rowCheckEmpCount = rowCheckTagCount;
            }
            else{
                rowCheckEmpCount++;
            }
        }

    }

    private void setProductionPlan(Sheet sheet, CellStyle style, ProductionVM productionVM) {

        // create header row
        //set planning date
        Row planningRow = sheet.createRow(0);
        planningRow.createCell(0).setCellValue("Planning Date");
        planningRow.getCell(0).setCellStyle(style);
        sheet.autoSizeColumn(0);
        planningRow.createCell(1).setCellValue(productionVM.getPlanningDate());
        //set shift
        Row shiftRow = sheet.createRow(1);
        shiftRow.createCell(0).setCellValue("Shift");
        shiftRow.getCell(0).setCellStyle(style);
        shiftRow.createCell(1).setCellValue(productionVM.getShift());


        Row header = sheet.createRow(3);
        header.createCell(0).setCellValue("Machines");
        header.getCell(0).setCellStyle(style);

        header.createCell(1).setCellValue("Orders");
        header.getCell(1).setCellStyle(style);


        header.createCell(2).setCellValue("Customer Name");
        header.getCell(2).setCellStyle(style);
        sheet.autoSizeColumn(2);

        header.createCell(3).setCellValue("Operator");
        header.getCell(3).setCellStyle(style);

        header.createCell(4).setCellValue("Framer");
        header.getCell(4).setCellStyle(style);

        header.createCell(5).setCellValue("In-Charge");
        header.getCell(5).setCellStyle(style);
        sheet.autoSizeColumn(5);

        header.createCell(6).setCellValue("Tags");
        header.getCell(6).setCellStyle(style);

        header.createCell(7).setCellValue("Size");
        header.getCell(7).setCellStyle(style);

        header.createCell(8).setCellValue("Quantity");
        header.getCell(8).setCellStyle(style);

        header.createCell(9).setCellValue("Completed");
        header.getCell(9).setCellStyle(style);

        header.createCell(10).setCellValue("Mistakes");
        header.getCell(10).setCellStyle(style);

        header.createCell(11).setCellValue("Status");
        header.getCell(11).setCellStyle(style);

        header.createCell(12).setCellValue("Comments");
        header.getCell(12).setCellStyle(style);
        sheet.autoSizeColumn(12);

        int rowMachineCount = 4;
        for (ProdMachine prodMachine : productionVM.getMachines()) {
            Row prodRow = sheet.createRow(rowMachineCount);
            prodRow.createCell(0).setCellValue(prodMachine.getId());
            int rowInwardCount = rowMachineCount;
            for (ProdInward prodInward : prodMachine.getOrders()) {
                if (rowInwardCount == rowMachineCount) {
                    Row currentRow = sheet.getRow(rowMachineCount);
                    currentRow.createCell(1).setCellValue(prodInward.getInward().getOrder().getSPC());
                    currentRow.createCell(2).setCellValue(prodInward.getInward().getOrder().getCustomer().getName());
                    currentRow.createCell(3).setCellValue(prodInward.getOperator().getName());
                    currentRow.createCell(4).setCellValue(prodInward.getFramer().getName());
                    currentRow.createCell(5).setCellValue(prodInward.getInCharge().getName());
                } else {
                    Row row1 = sheet.createRow(rowInwardCount);
                    row1.createCell(1).setCellValue(prodInward.getInward().getOrder().getSPC());
                    row1.createCell(2).setCellValue(prodInward.getInward().getOrder().getCustomer().getName());
                    row1.createCell(3).setCellValue(prodInward.getOperator().getName());
                    row1.createCell(4).setCellValue(prodInward.getFramer().getName());
                    row1.createCell(5).setCellValue(prodInward.getInCharge().getName());
                }
                int rowTagCount = rowInwardCount;
                for (Tag tag : prodInward.getToBeProcessedTags()) {
                    if (rowTagCount == rowInwardCount) {
                        Row currentRow = sheet.getRow(rowInwardCount);
                        currentRow.createCell(6).setCellValue(tag.getTagId());
                        currentRow.createCell(7).setCellValue(tag.getSize());
                        currentRow.createCell(8).setCellValue(tag.getQuantity());
                        currentRow.createCell(9).setCellValue(tag.getQuantityCompleted());
                        currentRow.createCell(10).setCellValue(tag.getMistakes());
                        currentRow.createCell(11).setCellValue(tag.getStatus());
                        sheet.autoSizeColumn(11);
                        currentRow.createCell(12).setCellValue(tag.getComments());
                    } else {
                        Row row1 = sheet.createRow(rowTagCount);
                        row1.createCell(6).setCellValue(tag.getTagId());
                        row1.createCell(7).setCellValue(tag.getSize());
                        row1.createCell(8).setCellValue(tag.getQuantity());
                        row1.createCell(9).setCellValue(tag.getQuantityCompleted());
                        row1.createCell(10).setCellValue(tag.getMistakes());
                        row1.createCell(11).setCellValue(tag.getStatus());
                        row1.createCell(12).setCellValue(tag.getComments());
                    }
                    rowTagCount++;
                }

                if (prodInward.getToBeProcessedTags().size() > 0) {
                    rowInwardCount = rowTagCount;
                } else {
                    rowInwardCount++;
                }
            }
            if (prodMachine.getOrders().size() > 0) {
                rowMachineCount = rowInwardCount;
            } else {
                rowMachineCount += 1;
            }
        }
    }
}
