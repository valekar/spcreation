package com.srini.exception;

import com.srini.constant.CONSTANTS;
import com.srini.constant.DATA_INTEGRITY_CONSTANTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;

import javax.persistence.RollbackException;
import javax.validation.constraints.Null;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ErrorHandlingController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(CustomDataIntegrityException.class)
    public ResponseEntity<ExceptionResponse> myDataException(CustomDataIntegrityException e) throws RuntimeException{
        log.error("+++++" + e.getCorrectMessage() );
        log.error("In SQLLL excpetion");
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(e.getCorrectMessage());
        eR.setStatus(HttpStatus.BAD_REQUEST);
        //log = LoggerFactory.getLogger();
        //log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionResponse> generalException(Exception e){
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(e.getMessage());
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        if(e.getMessage().equals(CONSTANTS.REQUIRED_FILE_NOT_FOUND)){
            eR.setDetailedDescription(CONSTANTS.PLEASE_UPLOAD_IMAGE);
        }
        else {
            eR.setDetailedDescription(CONSTANTS.RESOURCE_NOT_FOUND);
        }
        eR.setHeader(CONSTANTS.ERROR);
        log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<ExceptionResponse> IOException(Exception e){
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(e.getMessage());
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        eR.setDetailedDescription(CONSTANTS.FILE_UPLOAD_EXCEPTION);
        eR.setHeader(CONSTANTS.ERROR);
        log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<ExceptionResponse> multipartException(Exception e){
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(e.getMessage());
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        eR.setDetailedDescription(CONSTANTS.FILE_UPLOAD_EXCEED_LIMIT);
        eR.setHeader(CONSTANTS.ERROR);
        log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ExceptionResponse> customException(CustomException e){
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(e.getMessage());
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        eR.setHeader(CONSTANTS.ERROR);
        log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ExceptionResponse> userNotFoundException(UserNotFoundException e) {
        log.error("In Usernot found excpetion");
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription(CONSTANTS.USER_NOT_FOUND);
        eR.setHeader(CONSTANTS.FAILED);
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        //log = LoggerFactory.getLogger(clazz);
        //log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);

    }

  /*  @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionResponse> runtimeException(RuntimeException e) throws RuntimeException{
        System.out.println("+++++" );
        System.out.println("In Usernot found excpetion");
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setDescription("In Usernot found excpetion");
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        //log = LoggerFactory.getLogger();
        //log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);

    }*/

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ExceptionResponse> sqlException(DataIntegrityViolationException e) throws DataIntegrityViolationException{
        //System.out.println("+++++" + e.getMessage() );
        //System.out.println("In SQLLL excpetion");
        Map map = getCorrectMessage(e);
        String header = map.get(CONSTANTS.HEADER).toString();
        String message = map.get(CONSTANTS.MESSAGE).toString();
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        eR.setHeader(header);
        eR.setDescription(message);
        eR.setDetailedDescription(e.getLocalizedMessage());
        eR.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        //log = LoggerFactory.getLogger();
        //log.error("{} ({}): {}", e.getMessage(), e.getCause(), e.getClass(), e);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity<ExceptionResponse> invalidTokenException(InvalidTokenException i) throws InvalidTokenException{
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.UNAUTHORIZED.value());
        eR.setHeader(CONSTANTS.ERROR);
        eR.setDescription(CONSTANTS.INVALID_TOKEN_DESC);
        eR.setStatus(HttpStatus.UNAUTHORIZED);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.UNAUTHORIZED);
    }


    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<ExceptionResponse> nullPointerException(NullPointerException i) throws NullPointerException{

        log.error(i.getLocalizedMessage());
        ExceptionResponse eR = new ExceptionResponse();
        eR.setCode(HttpStatus.NOT_FOUND.value());
        eR.setHeader(CONSTANTS.ERROR);
        eR.setDescription(CONSTANTS.RESOURCE_NOT_FOUND);
        eR.setDetailedDescription(i.getLocalizedMessage());
        eR.setStatus(HttpStatus.NOT_FOUND);
        return new ResponseEntity<ExceptionResponse>(eR,HttpStatus.NOT_FOUND);
    }

    private Map<String,String> getCorrectMessage(Throwable cause){
        Map<String,String> map = new HashMap<>();
        if(cause.getMessage().contains(DATA_INTEGRITY_CONSTANTS.USERS_USERNAME_KEY)){
            map.put(CONSTANTS.HEADER, CONSTANTS.USER_ERROR_HEADER);
            map.put(CONSTANTS.MESSAGE,CONSTANTS.USER_ALREADY_EXISTS);
            return map;
        }
        else if(cause.getMessage().contains(DATA_INTEGRITY_CONSTANTS.USERS_EMAIL_KEY)){
            map.put(CONSTANTS.HEADER, CONSTANTS.USER_ERROR_HEADER);
            map.put(CONSTANTS.MESSAGE,CONSTANTS.USER_EMAIL_ALREADY_EXISTS);
            return map;
        }

        else if(cause.getMessage().contains(CONSTANTS.EMPLOYEE_DELETE_ERROR)){
            map.put(CONSTANTS.HEADER, CONSTANTS.DELETE_ERROR_HEADER);
            map.put(CONSTANTS.MESSAGE,CONSTANTS.EMPLOYEE_DELETE_ERROR);
            return map;
        }
        else {
            map.put(CONSTANTS.HEADER, CONSTANTS.DATABASE_ERROR_HEADER);
            map.put(CONSTANTS.MESSAGE,CONSTANTS.SOMETHING_WENT_WRONG);
        }
        return map;
    }


}
