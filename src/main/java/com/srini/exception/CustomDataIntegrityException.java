package com.srini.exception;

import com.srini.constant.CONSTANTS;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
public class CustomDataIntegrityException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    private String message;
    private Throwable cause;
    public CustomDataIntegrityException(String msg, Throwable cause){
        super(msg,cause);
        this.message = msg;
        this.cause = cause;
    }

    public CustomDataIntegrityException(){}

    public CustomDataIntegrityException(String msg) {
        super(msg);
    }

    public String getCorrectMessage(){
        if(cause.getMessage().contains("users_username_key")){
            return CONSTANTS.USER_ALREADY_EXISTS;
        }
        return cause.getMessage();
    }
}
