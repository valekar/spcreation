package com.srini.exception;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class ExceptionResponse {
    private int code;
    private String description;
    private HttpStatus status;
    private String header;
    private String detailedDescription;

    public int getCode(){
        return code;
    }
    public String getDescription(){
        return description;
    }

    public HttpStatus getStatus(){
        return status;
    }

    public void setCode(int code){
        this.code = code;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setStatus(HttpStatus status){
        this.status = status;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setDetailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }
}
