package com.srini.exception;

import org.springframework.stereotype.Component;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(){

    }
    public UserNotFoundException(String message){
        super(message);
    }

}
