
CREATE TABLE users (
    user_id SERIAL  PRIMARY KEY ,
    username VARCHAR(128) UNIQUE,
    password VARCHAR(256),
    enabled BOOL,
	email varchar(255) unique,
    created_at timestamp default now(),
	is_active bool default true not null
);



create table password_reset_tokens(
	token_id Bigserial primary key,
    token varchar(255),
    user_id bigint,
    expiry_date timestamp default CURRENT_TIMESTAMP + INTERVAL '1 day'
);

--INSERT INTO threads (thread_id , thread_name) VALUES ('1','PICAS');

INSERT INTO users (user_id, username, password, enabled,email,is_active) VALUES
	('1', 'srini', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true,'srinivas.valekar@gmail.com',true),
	('2', 'kanishka', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true, 'kanishkasrijith@gmail.com',true),
	('3', 'srijith', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', true, 'spcreation.emb@gmail.com',true);



create table privileges(
	id bigserial primary key,
    order_tab varchar(20) default 'none',
	inward_tab varchar(20) default 'none',
	production_tab varchar(20) default 'none',
	checking_tab varchar(20) default 'none',
	reports_tab varchar(20) default 'none',
	admin_tab varchar(20) default 'none',
	store_tab varchar(20) default 'none',
	company_tab varchar(20) default 'none'


);

create table users_privileges(
	privilege_id bigint references privileges(id),
    user_id bigint references users(user_id)
);


--insert into privileges values(1,'write','write');
--insert into users_privileges values(1,1)

create table employees(
	emp_id bigserial primary key,
    name varchar(50),
    age integer,
    address varchar(255),
    sex varchar(4),
    blood_group varchar(10),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
	created_by varchar(255) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table departments(
	id bigserial primary key,
    name varchar(100),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);

create table employees_departments(
	employee_id bigint references employees(emp_id),
    department_id bigint references departments(id)
);

create table customers(
	id bigserial primary key,
    name varchar(40) not null,
    address varchar(255),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table machines(
	id bigserial primary key,
    name varchar(255),
    heads integer,
	type varchar(50),
	stitch_capacity bigint,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);



create table rates(
	id bigserial primary key,
    stitch_numbers serial,
    rate numeric(10,5),
    type varchar(50),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table papers(
	id bigserial primary key,
    type varchar(100),
    quality varchar(100),
    size serial,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table threads(
	id bigserial primary key,
    brand_name varchar(100),
    color varchar(100),
    type varchar(50),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table appliques(
	id bigserial primary key,
    number integer,
    type varchar(40),
    color varchar(40),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);




create table combos(
	id bigserial primary key,
    cones numeric(18,5),
    boxes numeric(18,5),
    frame_size numeric(18,5),
    layers bigint,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);

create table combos_threads(
	combo_id bigint references combos(id),
    thread_id bigint references threads(id)
);

create table combos_appliques(
	combo_id bigint references combos(id),
    applique_id bigint references appliques(id)
);


create table combos_papers(
	combo_id bigint references combos(id),
    paper_id bigint references papers(id)
);


create table orders(
	id bigserial primary key,
    SAM varchar(140) not null,
    SPC varchar(140) not null,
    SAM_number int not null,
    SPC_number int not null,
    status varchar(140) not null default 'PENDING',
    grade varchar(140),
    design_name varchar(140),
    embroidery_type varchar(140),
    duration bigint, -- storing the time in seconds
    quantity bigint,
    fabric_color varchar(140),
    fabric_size varchar(140),
    position varchar(140),
    measurement varchar(140),
    part varchar(140),
    no_combos bigint,
    stitches bigint,
	created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
	created_user_id bigint not null,
	updated_by varchar(40) not null,
	updated_user_id bigint not null,
	is_active BOOL default true not null
);


create table orders_combos(
	order_id bigint references orders(id),
    combo_id bigint references combos(id)
);




Create table images(
	id bigserial primary key,
    name varchar(255),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);



create table orders_images(
order_id bigint references orders(id),
image_id bigint references images(id)
);


create table orders_customers(
    order_id bigint references orders(id),
    customer_id bigint references customers(id)

);


-- for inwards
create table inwards(
	id bigserial primary key,
    lay timestamp,
    number bigint,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);


create table inwards_orders(
	inward_id bigint references inwards(id),
    order_id bigint references orders(id)
);

create table tags (
	id bigserial primary key,
    tag_id bigint,
    size varchar(140),
    bundle bigint,
    quantity bigint,
    comments varchar(140),
    status varchar(140) default 'PENDING',
    quantity_complete bigint default 0,
    mistakes bigint default 0,
    check_quantity_complete bigint default 0,
    check_mistakes bigint default 0,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);

create table inwards_tags(
	inward_id bigint references inwards(id),
    tag_id bigint references tags(id)
);



create table constants(
	id bigserial primary key,
    key varchar(140),
    value varchar(140),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);


create table productions(
	/*
    	production table keeping track of planned inwards
    */
    id bigserial primary key,
    shift varchar(140),
    planningDate timestamp,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);

create table machines_productions(
    /*
    	this is a copy table of machines with extra fields
    */
	id bigserial primary key,
    type varchar(140),
    heads bigint,
    stitch_capacity bigint,
    no_of_orders bigint,
    name varchar(140),
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null

);


create table machines_prod_brid(
	production_id bigint references productions(id),
    machine_production_id bigint references machines_productions(id)
);



create table inwards_productions(
	id bigserial primary key,
    tag_cloth_quantity bigint,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);

create table machines_prod_inwards_prod_brid (
	machine_prod_id bigint references machines_productions(id),
    inward_prod_id bigint references inwards_productions(id)
 );

 create table inwards_prod_inwards_brid(
 	inward_production_id bigint references inwards_productions(id),
     inward_id bigint references inwards(id)
 );

create table inwards_prod_operator_emp(
	inward_production_id bigint references inwards_productions(id),
    employee_id bigint references employees(emp_id)
);


create table inwards_prod_incharge_emp(
	inward_production_id bigint references inwards_productions(id),
    employee_id bigint references employees(emp_id)
);


create table inwards_prod_framer_emp(
	inward_production_id bigint references inwards_productions(id),
    employee_id bigint references employees(emp_id)
);

--for storing tobeProcessedTags
create table inwards_prod_tags(
	tag_id bigint references tags(id),
    inward_production_id bigint references inwards_productions(id)
);




-- now the checking tables come in
create table checkings(
	id bigserial primary key,
    shift varchar(140),
    planning_date timestamp,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null
);

create table checkings_employees(
    -- contains details of employees
	id bigserial primary key,
	assigned_quantity bigint default 0,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null

);

create table checkings_checkings_employees_brid(
    -- a checking can have many employees so this is the bridge between checkings table and checkings_employees table
	checking_id bigint references checkings(id),
    checking_employee_id bigint references checkings_employees(id)
);


create table employees_helper_checkings_employees_brid(
    -- this is the one on one relationship created just to spearate the concerns of checking employees,
	checking_employee_id bigint references checkings_employees(id),
    employee_id bigint references employees(emp_id)
);




create table employees_checkings_employees_brid(
    -- this is the one on one relationship created just to spearate the concerns of checking employees,
	checking_employee_id bigint references checkings_employees(id),
    employee_id bigint references employees(emp_id)
);



create table checkings_tags(
    -- contains to be checked tags details
	id bigserial primary key,
    created_at timestamp default CURRENT_TIMESTAMP,
    updated_at timestamp default CURRENT_TIMESTAMP,
    created_by varchar(40) not null,
    created_user_id bigint not null,
    updated_by varchar(40) not null,
    updated_user_id bigint not null,
    is_active BOOL default true not null

);


create table checkings_tags_checkings_employees_brid(
    -- this is the brdige between checkings_tags and checkings_employees table // a checking employee can have many tags
	checking_employee_id bigint references checkings_employees(id),
    checkings_tag_id bigint references checkings_tags(id)
);


create table checkings_tags_tags_brid(
	-- this is the bridge between checkings_tags and tags tables
    checking_tag_id bigint references checkings_tags(id),
    tag_id bigint references tags(id)
);


CREATE TABLE sam_spc_counter (
    id bigint NOT NULL
);


ALTER TABLE sam_spc_counter OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 35314)
-- Name: sam_spc_counter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sam_spc_counter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sam_spc_counter_id_seq OWNER TO postgres;

--
-- TOC entry 2768 (class 0 OID 0)
-- Dependencies: 241
-- Name: sam_spc_counter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sam_spc_counter_id_seq OWNED BY sam_spc_counter.id;